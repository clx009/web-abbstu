package test.hnykl.bp.web.position.controller;

import static org.junit.Assert.*; 

import org.junit.Test;

import test.BaseTest;

public class PositionControllerTest extends BaseTest{

	@Test
	public void testUploadPosition() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindMemberPositions() { 
		
		String requestJsonStr = "";
		requestJsonStr = "{ ";
		requestJsonStr += " 'Data': { ";
		requestJsonStr += "  'Head': {";
		requestJsonStr += "       'User': { "; 
		requestJsonStr += "       } ";
		requestJsonStr += "    }, ";
		requestJsonStr += "   'Request': {   ";
		requestJsonStr += "            'userId': '402847ea54ec986d0154eca6c7240003',"; 
		requestJsonStr += "            'startTime': '2016-06-06 00:00:00',"; 
		requestJsonStr += "            'endTime': '2016-06-06 23:59:59'"; 
		requestJsonStr += "    } ";
		requestJsonStr += " } ";
		requestJsonStr += "}  ";
		this.doExecute("positionController.do?findMemberPositions",
				requestJsonStr);
	}
	@Test
	public void testUploadPositionWithoutFile() { 
		
		String requestJsonStr = "";
		requestJsonStr = "{ ";
		requestJsonStr += " 'Data': { ";
		requestJsonStr += "  'Head': {";
		requestJsonStr += "       'User': { "; 
		requestJsonStr += "       } ";
		requestJsonStr += "    }, ";
		requestJsonStr += "   'Request': {   ";
		requestJsonStr += "            'userId': '4028813746a920bd0146a92a2c1e001b',"; 
		requestJsonStr += "            'longitude': '112.965667',"; 
		requestJsonStr += "            'latitude': '28.204215',"; 
		requestJsonStr += "            'describe': '位置',"; 
		requestJsonStr += "            'createTime': '2016-07-25 00:43:59',"; 
		requestJsonStr += "            'timeZone': 'GMT+00:00'"; 
		requestJsonStr += "    } ";
		requestJsonStr += " } ";
		requestJsonStr += "}  ";
		this.doExecute("positionController.do?uploadPositionWithoutFile",
				requestJsonStr);
	}
	@Test
	public void testAnalysePosition() { 
		String requestJsonStr = "";
		requestJsonStr = "{ ";
		requestJsonStr += " 'Data': { ";
		requestJsonStr += "  'Head': {";
		requestJsonStr += "       'User': { "; 
		requestJsonStr += "       } ";
		requestJsonStr += "    }, ";
		requestJsonStr += "   'Request': {   ";
		requestJsonStr += "            'userId': '402847ea552ec66201552ef5ac37001b',"; 
		requestJsonStr += "            'startTime': '2016-08-14',"; 
		requestJsonStr += "            'endTime': '2016-08-15' "; 
		requestJsonStr += "    } ";
		requestJsonStr += " } ";
		requestJsonStr += "}  ";
		this.doExecute("positionController.do?analysePosition",
				requestJsonStr);
	}

}
