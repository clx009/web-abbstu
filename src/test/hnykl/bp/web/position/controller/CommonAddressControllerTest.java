package test.hnykl.bp.web.position.controller; 

import org.junit.Test;

import test.BaseTest;

public class CommonAddressControllerTest extends BaseTest {

	@Test
	public void testFindMemberCommonAddress() {
		String requestJsonStr = "";
		requestJsonStr = "{ ";
		requestJsonStr += " 'Data': { ";
		requestJsonStr += "  'Head': {";
		requestJsonStr += "       'User': { "; 
		requestJsonStr += "       } ";
		requestJsonStr += "    }, ";
		requestJsonStr += "   'Request': {   ";
		requestJsonStr += "            'familyUserId': '402847ea54ec986d0154eca6c7240003'"; 
		requestJsonStr += "    } ";
		requestJsonStr += " } ";
		requestJsonStr += "}  ";
		this.doExecute("commonAddressController.do?findMemberCommonAddress",
				requestJsonStr);
	}

	@Test
	public void testCreateCommonAddress() {
		String requestJsonStr = "";
		requestJsonStr = "{ ";
		requestJsonStr += " 'Data': { ";
		requestJsonStr += "  'Head': {";
		requestJsonStr += "       'User': { ";
		requestJsonStr += "       } ";
		requestJsonStr += "    }, ";
		requestJsonStr += "   'Request': {   ";
		requestJsonStr += "            'userId': '4028813746a920bd0146a92a2c1e001b',";
		requestJsonStr += "            'familyUserId': '4028813746a920bd0146a92a2c1e001b',";
		requestJsonStr += "            'address': 'changsha hunan china',";
		requestJsonStr += "            'longitude': '112.862586',";
		requestJsonStr += "            'latitude': '28.217249',";
		requestJsonStr += "            'name': '学校',";
		requestJsonStr += "            'radius': '10',";
		requestJsonStr += "            'type': '01'";
		requestJsonStr += "    } ";
		requestJsonStr += " } ";
		requestJsonStr += "}  ";
		this.doExecute("commonAddressController.do?createCommonAddress",
				requestJsonStr);
	}

	@Test
	public void testModifyCommonAddress() {
		String requestJsonStr = "";
		requestJsonStr = "{ ";
		requestJsonStr += " 'Data': { ";
		requestJsonStr += "  'Head': {";
		requestJsonStr += "       'User': { ";
		requestJsonStr += "       } ";
		requestJsonStr += "    }, ";
		requestJsonStr += "   'Request': {   ";
		requestJsonStr += "            'userId': '4028813746a920bd0146a92a2c1e001b',";
		requestJsonStr += "            'commAddrId': '8a21319055aa028d0155b9c06bb50002',";
		requestJsonStr += "            'familyUserId': '402847ea54ec986d0154eca6c7240003',";
		requestJsonStr += "            'address': 'changsha hunan china',";
		requestJsonStr += "            'longitude': '112.862586',";
		requestJsonStr += "            'latitude': '28.217249',";
		requestJsonStr += "            'name': 'zs',";
		requestJsonStr += "            'radius': '30',";
		requestJsonStr += "            'type': '01'";
		requestJsonStr += "    } ";
		requestJsonStr += " } ";
		requestJsonStr += "}  ";
		this.doExecute("commonAddressController.do?modifyCommonAddress",
				requestJsonStr);
	}

	@Test
	public void testDeleteCommonAddress() {
		String requestJsonStr = "";
		requestJsonStr = "{ ";
		requestJsonStr += " 'Data': { ";
		requestJsonStr += "  'Head': {";
		requestJsonStr += "       'User': { "; 
		requestJsonStr += "       } ";
		requestJsonStr += "    }, ";
		requestJsonStr += "   'Request': {   ";
		requestJsonStr += "            'commAddrId': '402847ea55101a840155106b23cd0011'"; 
		requestJsonStr += "    } ";
		requestJsonStr += " } ";
		requestJsonStr += "}  ";
		this.doExecute("commonAddressController.do?deleteCommonAddress",
				requestJsonStr);
	}

}
