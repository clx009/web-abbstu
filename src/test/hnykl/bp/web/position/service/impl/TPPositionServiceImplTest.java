package test.hnykl.bp.web.position.service.impl; 

import java.sql.Timestamp;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.hnykl.bp.web.position.entity.TPPositionEntity;
import com.hnykl.bp.web.position.service.TPPositionServiceI;

@RunWith(SpringJUnit4ClassRunner.class)
//使用junit4进行测试
@ContextConfiguration({ "classpath:/config/spring/applicationContext.xml" })
public class TPPositionServiceImplTest {
	@Autowired
	TPPositionServiceI tPPositionService;
	@Test
	public void testFindMemberPositions() {
		Timestamp start = Timestamp.valueOf("2016-06-06 00:00:00");
		Timestamp end = Timestamp.valueOf("2016-06-06 24:59:59");
		List<TPPositionEntity> findMemberPositions = tPPositionService.findMemberPositions("1212", start, end);
		for(TPPositionEntity e : findMemberPositions){
			System.out.println(e.getId());
		}
	}

}
