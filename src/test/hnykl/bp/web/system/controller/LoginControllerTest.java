package test.hnykl.bp.web.system.controller;
 
import org.junit.Test;

import test.BaseTest;

public class LoginControllerTest extends BaseTest{

	@Test
	public void testLogin() {
		String sbStr = "";
		sbStr = "{ ";
		sbStr += " 'Data': { ";
		sbStr += "  'Head': {";
		sbStr += "       'User': { ";
		sbStr += "       } ";
		sbStr += "    }, ";
		sbStr += "   'Request': {   ";
		sbStr += "            'userName': '15700758231', ";
		sbStr += "            'password': '123456', ";
		sbStr += "            'userType': '' ";
		sbStr += "    } ";
		sbStr += " } ";
		sbStr += "}  "; 
		this.doExecute("loginController.do?login", sbStr);
	}

	@Test
	public void testThirdpartyLogin() {
		String sbStr = "";
		sbStr = "{ ";
		sbStr += " 'Data': { ";
		sbStr += "  'Head': {";
		sbStr += "       'User': { ";
		sbStr += "       } ";
		sbStr += "    }, ";
		sbStr += "   'Request': {   ";
		sbStr += "            'openId': '1111111', ";
		sbStr += "            'thirdpartyLoginType': '1' ";
		sbStr += "    } ";
		sbStr += " } ";
		sbStr += "}  "; 
		this.doExecute("loginController.do?thirdpartLogin", sbStr);
	}

	@Test
	public void testModifyPasswordForForget() {
		String sbStr = "";
		sbStr = "{ ";
		sbStr += " 'Data': { ";
		sbStr += "  'Head': {";
		sbStr += "       'User': { ";
		sbStr += "       } ";
		sbStr += "    }, ";
		sbStr += "   'Request': {   ";
		sbStr += "            'userId': '402847ea55298de90155298ec7440001', ";
		sbStr += "            'loginName': '18692219194', ";
		sbStr += "            'password': '1234567', ";
		sbStr += "            'telValidateCode': '4405' ";
		sbStr += "    } ";
		sbStr += " } ";
		sbStr += "}  "; 
		this.doExecute("loginController.do?modifyPasswordForForget", sbStr);
	}

}
