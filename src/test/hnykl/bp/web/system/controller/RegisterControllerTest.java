package test.hnykl.bp.web.system.controller;

import static org.junit.Assert.fail;

import org.junit.Test;

import test.BaseTest;

import com.hnykl.bp.base.core.util.PasswordUtil;
 

public class RegisterControllerTest extends BaseTest{  

	@Test
	public void testRegister() {   
		String sbStr = "";
		sbStr = "{ ";
		sbStr += " 'Data': { ";
		sbStr += "  'Head': {";
		sbStr += "       'User': { ";
		sbStr += "       } ";
		sbStr += "    }, ";
		sbStr += "   'Request': {   ";
		sbStr += "            'userName': '13533333333', ";
		sbStr += "            'password': '123456', ";
		//sbStr += "            'phoneValidateCode': '5512', ";
		sbStr += "            'type': '12' ";
		sbStr += "    } ";
		sbStr += " } ";
		sbStr += "}  ";
		this.doExecute("registerController.do?register", sbStr);
	}

	@Test
	public void testIsExsitsLoginName() {
		fail("Not yet implemented");
	}

/*	String thirdpartyLoginType = StringUtils.getMapDataStr(reqBodyMap,"thirdpartyLoginType");
	String phoneNumber = StringUtils.getMapDataStr(reqBodyMap,"phoneNumber");
	String password = StringUtils.getMapDataStr(reqBodyMap, "password");*/
	
	
	@Test
	public void testThirdpartyRegister() {
		String sbStr = "";
		sbStr = "{ ";
		sbStr += " 'Data': { ";
		sbStr += "  'Head': {";
		sbStr += "       'User': { ";
		sbStr += "       } ";
		sbStr += "    }, ";
		sbStr += "   'Request': {   ";
		sbStr += "            'openId': 'aaaa11111', ";
		sbStr += "            'thirdpartyLoginType': '1', ";
		sbStr += "            'phoneNumber': '15700758231', ";
		sbStr += "            'password': '123456' ";
		sbStr += "    } ";
		sbStr += " } ";
		sbStr += "}  ";
		this.doExecute("registerController.do?thirdpartRegister", sbStr);
	}
	
	public static void main(String[] args) { 
		String ss = PasswordUtil.encrypt("123456","13458560689",PasswordUtil.getStaticSalt());
		System.out.println(ss);
	}

}
