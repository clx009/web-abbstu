package test.hnykl.bp.web.system.controller;

import static org.junit.Assert.*;

import org.junit.Test;

import com.hnykl.bp.base.tool.character.StringUtils;

import test.BaseTest;

public class UserControllerTest extends BaseTest{
	/*String username = StringUtils.getMapDataStr(reqBodyMap, "username");
	String timeZone = StringUtils.getMapDataStr(reqBodyMap, "timeZone");
	String nickname = StringUtils.getMapDataStr(reqBodyMap, "nickname");
	String messageAlert = StringUtils.getMapDataStr(reqBodyMap,
			"messageAlert");
	String realName = StringUtils.getMapDataStr(reqBodyMap, "realName");*/
	
	
	@Test
	public void testChangeProfile() {
		String sbStr = "";
		sbStr = "{ ";
		sbStr += " 'Data': { ";
		sbStr += "  'Head': {";
		sbStr += "       'User': { ";
		sbStr += "       } ";
		sbStr += "    }, ";
		sbStr += "   'Request': {   ";
		sbStr += "            'username': '15700758231', ";
		sbStr += "            'timeZone': '111', ";
		sbStr += "            'nickname': '222',"; 
		sbStr += "            'messageAlert': '1',"; 
		sbStr += "            'realName': '444'"; 
		sbStr += "    } ";
		sbStr += " } ";
		sbStr += "}  ";
		this.doExecute("userController.do?changeProfile", sbStr);
	}


	@Test
	public void testChangePhoneNumber() {
		fail("Not yet implemented");
	}

	@Test
	public void testChangeEmail() {
		String sbStr = "";
		sbStr = "{ ";
		sbStr += " 'Data': { ";
		sbStr += "  'Head': {";
		sbStr += "       'User': { ";
		sbStr += "            'username': '', ";
		sbStr += "       } ";
		sbStr += "    }, ";
		sbStr += "   'Request': {   ";
		sbStr += "            'email': '', ";
		sbStr += "            'phoneValidateCode': ''"; 
		sbStr += "    } ";
		sbStr += " } ";
		sbStr += "}  ";
		this.doExecute("userController.do?changeEmail", sbStr);
	}

	@Test
	public void testChangePassword() {
		String sbStr = "";
		sbStr = "{ ";
		sbStr += " 'Data': { ";
		sbStr += "  'Head': {";
		sbStr += "       'User': { ";
		sbStr += "            'username': '', ";
		sbStr += "       } ";
		sbStr += "    }, ";
		sbStr += "   'Request': {   ";  
		sbStr += "            'userId': '8a21319055e3836f0155f38530320e03', ";
		sbStr += "            'username': '15700758231',"; 
		sbStr += "            'currentPassword': '123123',"; 
		sbStr += "            'newPassword': '123456'"; 
		sbStr += "    } ";
		sbStr += " } ";
		sbStr += "}  ";
		this.doExecute("userController.do?changePassword", sbStr);
	}
	
	
	
	@Test
	public void testChangeHeadPortrait() {
		String sbStr = "";
		sbStr = "{ ";
		sbStr += " 'Data': { ";
		sbStr += "  'Head': {";
		sbStr += "       'User': { ";
		sbStr += "            'username': '', ";
		sbStr += "       } ";
		sbStr += "    }, ";
		sbStr += "   'Request': {   ";  
		sbStr += "            'userId': '8a21319055e3836f0155f38530320e03', ";
		sbStr += "            'username': '15700758231',"; 
		sbStr += "            'currentPassword': '123123',"; 
		sbStr += "            'newPassword': '123456'"; 
		sbStr += "    } ";
		sbStr += " } ";
		sbStr += "}  ";
		this.doExecute("userController.do?changePassword", sbStr);
	}
	
	@Test
	public void testFindStudentListByUser() {
		String sbStr = "";
		sbStr = "{ ";
		sbStr += " 'Data': { ";
		sbStr += "  'Head': {";
		sbStr += "       'User': { ";
		sbStr += "            'username': '', ";
		sbStr += "       } ";
		sbStr += "    }, ";
		sbStr += "   'Request': {   ";  
		sbStr += "            'userId': '402847ea552ec66201552ef5ac37001b', ";
		sbStr += "    } ";
		sbStr += " } ";
		sbStr += "}  ";
		this.doExecute("userController.do?findStudentListByUser", sbStr);
	}
	
	@Test
	public void testFindUserRealTimeInfo() {
		String sbStr = "";
		sbStr = "{ ";
		sbStr += " 'Data': { ";
		sbStr += "  'Head': {";
		sbStr += "       'User': { ";
		sbStr += "            'username': '', ";
		sbStr += "       } ";
		sbStr += "    }, ";
		sbStr += "   'Request': {   ";  
		sbStr += "            'userId': '8a213190560879ed01561778dcb40088', ";
		sbStr += "    } ";
		sbStr += " } ";
		sbStr += "}  ";
		this.doExecute("userController.do?findUserRealTimeInfo", sbStr);
	}

}
