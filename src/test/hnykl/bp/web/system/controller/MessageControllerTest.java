package test.hnykl.bp.web.system.controller;
 
import org.junit.Test;

import test.BaseTest;

public class MessageControllerTest extends BaseTest{

	@Test
	public void testFindMsgList() {
		String sbStr = "";
		sbStr = "{ ";
		sbStr += " 'Data': { ";
		sbStr += "  'Head': {";
		sbStr += "       'User': { ";
		sbStr += "       } ";
		sbStr += "    }, ";
		sbStr += "   'Request': {   ";
		sbStr += "            'userId': '8af4a8c456648cdc015667ae92ef007c' ";
		sbStr += "    } ";
		sbStr += " } ";
		sbStr += "}  "; 
		this.doExecute("messageController.do?findMsgList", sbStr);
	}

	@Test
	public void testFetchMsgById() {
		String sbStr = "";
		sbStr = "{ ";
		sbStr += " 'Data': { ";
		sbStr += "  'Head': {";
		sbStr += "       'User': { ";
		sbStr += "       } ";
		sbStr += "    }, ";
		sbStr += "   'Request': {   ";
		sbStr += "            'msgId': '402847eb5575c494015575f52ae6000f' ";
		sbStr += "    } ";
		sbStr += " } ";
		sbStr += "}  "; 
		this.doExecute("messageController.do?fetchMsgById", sbStr);
	}
}
