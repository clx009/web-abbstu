package test.hnykl.bp.web.system.controller;

import static org.junit.Assert.*;

import org.junit.Test;

import test.BaseTest;

public class PhoneValidateCodeControllerTest extends BaseTest {

	@Test
	public void testSendTelValidateCode() {
		String sbStr = "";
		sbStr = "{ ";
		sbStr += " 'Data': { ";
		sbStr += "  'Head': {";
		sbStr += "       'User': { ";
		sbStr += "       } ";
		sbStr += "    }, ";
		sbStr += "   'Request': {   ";
		sbStr += "            'phoneNumber': '18692219194',"; 
		sbStr += "            'type': 'register'"; 
		sbStr += "    } ";
		sbStr += " } ";
		sbStr += "}  ";
		this.doExecute("phoneValidateCodeController.do?sendTelValidateCode", sbStr);
	}

}
