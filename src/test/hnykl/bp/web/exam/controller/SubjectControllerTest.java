package test.hnykl.bp.web.exam.controller;
import org.junit.Test;
import test.BaseTest;
public class SubjectControllerTest extends BaseTest{ 
	@Test
	public void testFindSubject() {
		String requestJsonStr = "";
		requestJsonStr = "{ ";
		requestJsonStr += " 'Data': { ";
		requestJsonStr += "  'Head': {";
		requestJsonStr += "       'User': { ";
		requestJsonStr += "       } ";
		requestJsonStr += "    }, ";
		requestJsonStr += "   'Request': {   ";
		requestJsonStr += "    } ";
		requestJsonStr += " } ";
		requestJsonStr += "}  "; 
		this.doExecute("subjectController.do?findSubject", requestJsonStr);
	} 
}
