package test.hnykl.bp.web.exam.controller;
import org.junit.Test;
import test.BaseTest;
public class ExamResultControllerTest extends BaseTest{ 
	@Test
	public void testFindExamResult() {
		String requestJsonStr = "";
		requestJsonStr = "{ ";
		requestJsonStr += " 'Data': { ";
		requestJsonStr += "  'Head': {";
		requestJsonStr += "       'User': { ";
		requestJsonStr += "       } ";
		requestJsonStr += "    }, ";
		requestJsonStr += "   'Request': {   ";
		requestJsonStr += "            'userId': '8a21319055aa028d0155bb2c9f310007',"; 
		requestJsonStr += "            'schoolYear': '2016',"; 
		requestJsonStr += "            'term': '3'"; 
		requestJsonStr += "    } ";
		requestJsonStr += " } ";
		requestJsonStr += "}  "; 
		this.doExecute("examResultController.do?findExamResult", requestJsonStr);
	} 
	
	@Test
	public void testAnalyseExamResult() {
		String requestJsonStr = "";
		requestJsonStr = "{ ";
		requestJsonStr += " 'Data': { ";
		requestJsonStr += "  'Head': {";
		requestJsonStr += "       'User': { ";
		requestJsonStr += "       } ";
		requestJsonStr += "    }, ";
//		requestJsonStr += "   'Request': {   ";
//		requestJsonStr += "            'userId': '402847ea54ff55080154ff5be60e0001',"; 
//		requestJsonStr += "            'subjectId': '402847eb5590af00015590b33e980004,402847eb5590af00015590b3625b0006,4028470f5640d1df015640d8d3d90002',"; 
//		requestJsonStr += "            'schoolYear': '2015,2015,2016,2016',"; 
//		requestJsonStr += "            'term': '1,2,1,3'"; 
//		requestJsonStr += "    } ";
		requestJsonStr += "   'Request': {   ";
		requestJsonStr += "            'userId': '8a213190560879ed01561778dcb40088',"; 
		requestJsonStr += "            'subjectId': '402847eb5590af00015590b33e980004,402847eb5590af00015590b3625b0006,8a21319056438a2a0156538f50bf009f',"; 
		requestJsonStr += "            'schoolYear': '2016,2016,2016,2016',"; 
		requestJsonStr += "            'term': '1,2,3,4'"; 
		requestJsonStr += "    } ";
		requestJsonStr += " } ";
		requestJsonStr += "}  "; 
		this.doExecute("examResultController.do?analyseExamResult", requestJsonStr);
	} 
	
	@Test
	public void testAnalyseExamResult4GPA() {
		String requestJsonStr = "";
		requestJsonStr = "{ ";
		requestJsonStr += " 'Data': { ";
		requestJsonStr += "  'Head': {";
		requestJsonStr += "       'User': { ";
		requestJsonStr += "       } ";
		requestJsonStr += "    }, ";
		requestJsonStr += "   'Request': {   ";
		requestJsonStr += "            'userId': '8a213190560879ed01561778dcb40088',"; 
		//requestJsonStr += "            'subjectId': '402847eb5590af00015590b33e980004,402847eb5590af00015590b3625b0006',"; 
		requestJsonStr += "            'schoolYear': '2015,2016',"; 
		requestJsonStr += "            'term': '1,4'"; 
		requestJsonStr += "    } ";
		requestJsonStr += " } ";
		requestJsonStr += "}  "; 
		this.doExecute("examResultController.do?analyseExamResult4GPA", requestJsonStr);
	} 
}
