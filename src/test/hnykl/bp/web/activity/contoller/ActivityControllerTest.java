package test.hnykl.bp.web.activity.contoller;

import org.junit.Test;

import test.BaseTest;

public class ActivityControllerTest extends BaseTest {

	@Test
	public void testCreateActivities() {
		String requestJsonStr = "";
		requestJsonStr = "{ ";
		requestJsonStr += " 'Data': { ";
		requestJsonStr += "  'Head': {";
		requestJsonStr += "       'User': { ";
		requestJsonStr += "            		'loginName': '18692219194',";
		requestJsonStr += "            		'password': '123',";
		requestJsonStr += "            		'userType': '1'";
		requestJsonStr += "       } ";
		requestJsonStr += "    }, ";
		requestJsonStr += "   'Request': {   ";
		requestJsonStr += "            'ActivityItems':[";
		requestJsonStr += "            		{'userId': '402847ea55298de90155298ec7440001',";
		requestJsonStr += "            		'name': 'aaaaa1',";
		requestJsonStr += "            		'time': '2016-06-13 12:11:23',";
		requestJsonStr += "            		'cnTime': '2016-06-14 12:11:23',";
		requestJsonStr += "            		'remindTime': '30',";
		requestJsonStr += "            		'remindTimeUnit': 'm',";
		requestJsonStr += "            		'type': '1',";
		requestJsonStr += "            		'address': 'addr',";
		requestJsonStr += "            		'longitude': '11011',";
		requestJsonStr += "            		'latitude': '114',";
		requestJsonStr += "            		'remindSwitch': 'C',";
		requestJsonStr += "            		'arriveRemindSwitch': 'C'";
		requestJsonStr += "            		},";
		requestJsonStr += "            		{'userId': '402847ea55298de90155298ec7440001',";
		requestJsonStr += "            		'name': 'aaaaa2',";
		requestJsonStr += "            		'time': '2016-06-15 12:11:23',";
		requestJsonStr += "            		'cnTime': '2016-06-16 12:11:23',";
		requestJsonStr += "            		'remindTime': '30',";
		requestJsonStr += "            		'remindTimeUnit': 'm',";
		requestJsonStr += "            		'type': '1',";
		requestJsonStr += "            		'address': 'ddd',";
		requestJsonStr += "            		'longitude': '11011',";
		requestJsonStr += "            		'latitude': '114',";
		requestJsonStr += "            		'remindSwitch': 'O',";
		requestJsonStr += "            		'arriveRemindSwitch': 'O'";
		requestJsonStr += "            		}";
		requestJsonStr += "  			]";
		requestJsonStr += "    } ";
		requestJsonStr += " } ";
		requestJsonStr += "}  ";
		this.doExecute("activityController.do?createActivities", requestJsonStr);
	}

	@Test
	public void testModifyActivities() {
		String requestJsonStr = "";
		requestJsonStr = "{ ";
		requestJsonStr += " 'Data': { ";
		requestJsonStr += "  'Head': {";
		requestJsonStr += "       'User': { ";
		requestJsonStr += "            		'loginName': '18692219194',";
		requestJsonStr += "            		'password': '123',";
		requestJsonStr += "            		'userType': '1'";
		requestJsonStr += "       } ";
		requestJsonStr += "    }, ";
		requestJsonStr += "   'Request': {   ";
		requestJsonStr += "            'ActivityItems':[";
		requestJsonStr += "            		{";
		requestJsonStr += "            		'id': '402847ea55488f7d0155495899d30015',";
		requestJsonStr += "            		'userId': '402847ea55298de90155298ec7440001',";
		requestJsonStr += "            		'name': 'xxxxx',";
		requestJsonStr += "            		'time': '2016-06-10 12:11:23',";
		requestJsonStr += "            		'remindTime': '30',";
		requestJsonStr += "            		'remindTimeUnit': 'm',";
		requestJsonStr += "            		'type': '1',";
		requestJsonStr += "            		'address': 'xxxx',";
		requestJsonStr += "            		'longitude': '11011',";
		requestJsonStr += "            		'latitude': '114',";
		requestJsonStr += "            		'remindSwitch': 'O',";
		requestJsonStr += "            		'arriveRemindSwitch': 'O'";
		requestJsonStr += "            		},";
		requestJsonStr += "            		{";
		requestJsonStr += "            		'id': '402847ea55488f7d0155495899ee0016',";
		requestJsonStr += "            		'userId': '402847ea55298de90155298ec7440001',";
		requestJsonStr += "            		'name': 'xx',";
		requestJsonStr += "            		'time': '2016-06-09 12:11:23',";
		requestJsonStr += "            		'remindTime': '30',";
		requestJsonStr += "            		'remindTimeUnit': 'm',";
		requestJsonStr += "            		'type': '1',";
		requestJsonStr += "            		'address': 'xx',";
		requestJsonStr += "            		'longitude': '11011',";
		requestJsonStr += "            		'latitude': '114',";
		requestJsonStr += "            		'remindSwitch': 'O',";
		requestJsonStr += "            		'arriveRemindSwitch': 'O'";
		requestJsonStr += "            		}";
		requestJsonStr += "  			]";
		requestJsonStr += "    } ";
		requestJsonStr += " } ";
		requestJsonStr += "}  ";

		this.doExecute("activityController.do?modifyActivities", requestJsonStr);
	}

	@Test
	public void testFindActivity() {
		String requestJsonStr = "";
		requestJsonStr = "{ ";
		requestJsonStr += " 'Data': { ";
		requestJsonStr += "  'Head': {";
		requestJsonStr += "       'User': { ";
		requestJsonStr += "       } ";
		requestJsonStr += "    }, ";
		requestJsonStr += "   'Request': {   ";
		requestJsonStr += "            		'userId': '8a213190560879ed01561778dcb40088',";
		requestJsonStr += "            		'startTime': '2015-01-06 00:00:00',";
		requestJsonStr += "            		'endTime': '2016-08-06 00:00:00'";
		requestJsonStr += "    } ";
		requestJsonStr += " } ";
		requestJsonStr += "}  ";
		this.doExecute("activityController.do?findActivity", requestJsonStr);
	}

	@Test
	public void testDeleteActivity() {
		String requestJsonStr = "";
		requestJsonStr = "{ ";
		requestJsonStr += " 'Data': { ";
		requestJsonStr += "  'Head': {";
		requestJsonStr += "       'User': { ";
		requestJsonStr += "       } ";
		requestJsonStr += "    }, ";
		requestJsonStr += "   'Request': {   ";
		requestJsonStr += "            		'id': '402847ea55488f7d0155495899d30015',";
		requestJsonStr += "    } ";
		requestJsonStr += " } ";
		requestJsonStr += "}  ";
		this.doExecute("activityController.do?deleteActivity", requestJsonStr);
	}

	@Test
	public void testDeleteActivities() {
		String requestJsonStr = "";
		requestJsonStr = "{ ";
		requestJsonStr += " 'Data': { ";
		requestJsonStr += "  'Head': {";
		requestJsonStr += "       'User': { ";
		requestJsonStr += "       } ";
		requestJsonStr += "    }, ";
		requestJsonStr += "   'Request': {   ";
		requestJsonStr += "            		'ids': ['402847ea554cc8e801554cfaf0700007','402847ea554cc8e801554cfaf0610006']";
		requestJsonStr += "    } ";
		requestJsonStr += " } ";
		requestJsonStr += "}  ";
		this.doExecute("activityController.do?deleteActivities", requestJsonStr);
	}

}
