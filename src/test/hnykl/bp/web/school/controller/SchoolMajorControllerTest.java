package test.hnykl.bp.web.school.controller;

import org.junit.Test;

import test.BaseTest;

public class SchoolMajorControllerTest extends BaseTest{

	@Test
	public void testMajorRanking() {
		String sbStr = "";
		sbStr = "{ ";
		sbStr += " 'Data': { ";
		sbStr += "  'Head': {";
		sbStr += "       'User': { ";
		sbStr += "       } ";
		sbStr += "    }, ";
		sbStr += "   'Request': {   ";
		sbStr += "            'userId': '8a213190560879ed01561778dcb40088', ";
		sbStr += "            'firstResult': '0', ";
		sbStr += "            'maxResult': '10', ";
		sbStr += "            'orderType': 'asc', ";
		sbStr += "            'majorId': '', ";
		sbStr += "            'schoolType': ''"; 
		sbStr += "    } ";
		sbStr += " } ";
		sbStr += "}  "; 
		this.doExecute("schoolMajorController.do?majorRanking", sbStr); 
	}
	
	@Test
	public void testChooseSchool() {
		StringBuffer sbStr = new StringBuffer();
		sbStr.append(" {                                                             ");
		sbStr.append("     \"Data\": {                                                 ");
		sbStr.append("         \"Head\": {                                             ");
		sbStr.append("             \"User\": {                                         ");
		sbStr.append("                 \"loginName\": \"\",                              ");
		sbStr.append("                 \"password\": \"\",                               ");
		sbStr.append("                 \"userType\": \"\"   ");
		sbStr.append("             }                                                 ");
		sbStr.append("         },                                                    ");
		/*sbStr.append("         \"Request\": {                                          ");
		sbStr.append("             \"satRequirementMin\": \"1800,2100\",                      ");
		sbStr.append("             \"tuitionMin\": \"2500\",                            ");
		sbStr.append("             \"tuitionMax\": \"4000\",                            ");
		sbStr.append("             \"userId\": \"402847ea552ec66201552ef58cf00018\",     ");
		sbStr.append("             \"maxResult\": \"50\",                                ");
		sbStr.append("             \"majorId\": \"402847eb55a563910155a5715d47000b\",    ");
		sbStr.append("             \"orderType\": \"asc\",                               ");
		sbStr.append("             \"satRequirementMax\": \"2000,2500\",                      ");
		sbStr.append("             \"firstResult\": \"0\",                               ");
		sbStr.append("             \"cityId\": \"8a2131905634028b0156348ff3de000b\",     ");
		sbStr.append("             \"schoolType\": \"0\",     ");
		sbStr.append("             \"toeflRequirementMin\": \"90,100\",                      ");
		sbStr.append("             \"toeflRequirementMax\": \"100,110\"                      ");*/
		sbStr.append("         \"Request\": {                                          ");
		sbStr.append("             \"satRequirementMin\": \"\",                      ");
		sbStr.append("             \"tuitionMin\": \"\",                            ");
		sbStr.append("             \"tuitionMax\": \"\",                            ");
		sbStr.append("             \"userId\": \"8a213190560879ed01561778dcb40088\",     ");
		sbStr.append("             \"maxResult\": \"0\",                                ");
		sbStr.append("             \"majorId\": \"\",    ");
		sbStr.append("             \"orderType\": \"asc\",                               ");
		sbStr.append("             \"satRequirementMax\": \"\",                      ");
		sbStr.append("             \"firstResult\": \"0\",                               ");
		sbStr.append("             \"cityId\": \"\",     ");
		sbStr.append("             \"schoolType\": \"\",     ");
		sbStr.append("             \"toeflRequirementMin\": \"\",                      ");
		sbStr.append("             \"toeflRequirementMax\": \"\"                      ");
		sbStr.append("         }                                                     ");
		sbStr.append("     }                                                         ");
		sbStr.append(" }                                                             ");
		
		/*String sbStr = "";
		sbStr = "{ ";
		sbStr += " 'Data': { ";
		sbStr += "  'Head': {";
		sbStr += "       'User': { ";
		sbStr += "       } ";
		sbStr += "    }, ";
		sbStr += "   'Request': {   ";
		sbStr += "            'userId': '402847eb55966dc4015596783aad0001', ";
		sbStr += "            'firstResult': '0', ";
		sbStr += "            'maxResult': '10', ";
		sbStr += "            'schoolType': '', "; 
		sbStr += "            'orderType': 'asc', "; 
		sbStr += "            'cityId': '402847eb558060070155807976f10005', "; 
		sbStr += "            'tuitionMin': '10000', "; 
		sbStr += "            'tuitionMax': '60000', "; 
		sbStr += "            'majorId': '402847eb55a563910155a5715d47000b', "; 
		sbStr += "            'toeflRequirementMin': '90', "; 
		sbStr += "            'toeflRequirementMax': '120', "; 
		sbStr += "            'satRequirementMin': '2200', "; 
		sbStr += "            'satRequirementMax': '2222' "; 
		sbStr += "    } ";
		sbStr += " } ";
		sbStr += "}  "; */
		this.doExecute("schoolMajorController.do?chooseSchool", sbStr.toString()); 
	}

}
