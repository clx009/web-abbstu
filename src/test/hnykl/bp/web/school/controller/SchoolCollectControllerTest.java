package test.hnykl.bp.web.school.controller;
 

import org.junit.Test;

import test.BaseTest;

public class SchoolCollectControllerTest extends BaseTest{

	@Test
	public void testCollectSchool() {
		String sbStr = "";
		sbStr = "{ ";
		sbStr += " 'Data': { ";
		sbStr += "  'Head': {";
		sbStr += "       'User': { ";
		sbStr += "       } ";
		sbStr += "    }, ";
		sbStr += "   'Request': {   "; 
		sbStr += "            'schoolId': '402847ea5571b4ec015571c0c9850001', "; 
		sbStr += "            'userId': '402847eb55966dc4015596783aad0001' "; 
		sbStr += "    } ";
		sbStr += " } ";
		sbStr += "}  "; 
		this.doExecute("schoolCollectController.do?collectSchool", sbStr); 
	}

	@Test
	public void testCancelCollectSchool() {
		String sbStr = "";
		sbStr = "{ ";
		sbStr += " 'Data': { ";
		sbStr += "  'Head': {";
		sbStr += "       'User': { ";
		sbStr += "       } ";
		sbStr += "    }, ";
		sbStr += "   'Request': {   ";
		sbStr += "            'userId': 'userId',";  
		sbStr += "            'schoolId': '402847ea5571b4ec015571c0c9850001'";  
		sbStr += "    } ";
		sbStr += " } ";
		sbStr += "}  "; 
		this.doExecute("schoolCollectController.do?cancelCollectSchool", sbStr); 
	}
	
	@Test
	public void testCollectSchoolList() {
		String sbStr = "";
		sbStr = "{ ";
		sbStr += " 'Data': { ";
		sbStr += "  'Head': {";
		sbStr += "       'User': { ";
		sbStr += "       } ";
		sbStr += "    }, ";
		sbStr += "   'Request': {   ";
		sbStr += "            'familyId': '402847ea552ec66201552ef5ae71001c'";  
		sbStr += "    } ";
		sbStr += " } ";
		sbStr += "}  "; 
		this.doExecute("schoolCollectController.do?collectSchoolList", sbStr); 
	}
	

}
