package test.hnykl.bp.web.school.controller;
 

import org.junit.Test;

import test.BaseTest;

public class SchoolControllerTest extends BaseTest{

	@Test
	public void testSchoolRanking() {
		String sbStr = "";
		sbStr = "{ ";
		sbStr += " 'Data': { ";
		sbStr += "  'Head': {";
		sbStr += "       'User': { ";
		sbStr += "       } ";
		sbStr += "    }, ";
		sbStr += "   'Request': {   ";
		sbStr += "            'userId': '402847eb55966dc4015596783aad0001', ";
		sbStr += "            'firstResult': '0', ";
		sbStr += "            'maxResult': '10', ";
		sbStr += "            'schoolType': '', "; 
		sbStr += "            'orderType': 'asc', "; 
		sbStr += "    } ";
		sbStr += " } ";
		sbStr += "}  "; 
		this.doExecute("schoolController.do?schoolRanking", sbStr); 
	}
 
	@Test
	public void testSchoolCitys() {
		String sbStr = "";
		sbStr = "{ ";
		sbStr += " 'Data': { ";
		sbStr += "  'Head': {";
		sbStr += "       'User': { ";
		sbStr += "       } ";
		sbStr += "    }, ";
		sbStr += "   'Request': {   ";
		sbStr += "            'pid': '0' "; 
		sbStr += "    } ";
		sbStr += " } ";
		sbStr += "}  "; 
		this.doExecute("schoolController.do?schoolCitys", sbStr); 
	} 

	@Test
	public void testSchool() {
		String sbStr = "";
		sbStr = "{ ";
		sbStr += " 'Data': { ";
		sbStr += "  'Head': {";
		sbStr += "       'User': { ";
		sbStr += "       } ";
		sbStr += "    }, ";
		sbStr += "   'Request': {   ";
		sbStr += "            'schoolId': '402847ea556d6fb401556d7a49ea0004' "; 
		sbStr += "    } ";
		sbStr += " } ";
		sbStr += "}  "; 
		this.doExecute("schoolController.do?school", sbStr); 
	} 

}
