package test.hnykl.bp.web.school.controller;
 

import org.junit.Test;

import test.BaseTest;

public class MajorControllerTest extends BaseTest{

	@Test
	public void testMajorList() {
		String sbStr = "";
		sbStr = "{ ";
		sbStr += " 'Data': { ";
		sbStr += "  'Head': {";
		sbStr += "       'User': { ";
		sbStr += "       } ";
		sbStr += "    }, ";
		sbStr += "   'Request': {   ";
		sbStr += "            'pid': '', ";
		sbStr += "            'isHot': '' ";
		sbStr += "    } ";
		sbStr += " } ";
		sbStr += "}  ";
		this.doExecute("majorController.do?majorList", sbStr);
	}

}
