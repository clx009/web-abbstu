package test.hnykl.bp.web.ecm.controller;
 

import org.junit.Test;

import test.BaseTest;

public class EcmContentControllerTest extends BaseTest{ 
	@Test
	public void testFindContentList() {
		String requestJsonStr = "";
		requestJsonStr = "{ ";
		requestJsonStr += " 'Data': { ";
		requestJsonStr += "  'Head': {";
		requestJsonStr += "       'User': { ";
		requestJsonStr += "       } ";
		requestJsonStr += "    }, ";
		requestJsonStr += "   'Request': {   ";
		requestJsonStr += "            'categoryCode': '001001001001001'"; 
		requestJsonStr += "    } ";
		requestJsonStr += " } ";
		requestJsonStr += "}  "; 
		this.doExecute("contentController.do?findContentList", requestJsonStr);
	} 
	@Test
	public void testFindContentDetail() {
		String requestJsonStr = "";
		requestJsonStr = "{ ";
		requestJsonStr += " 'Data': { ";
		requestJsonStr += "  'Head': {";
		requestJsonStr += "       'User': { ";
		requestJsonStr += "       } ";
		requestJsonStr += "    }, ";
		requestJsonStr += "   'Request': {   ";
		requestJsonStr += "            'contentId': '4028811149e0e8a30149e0f2fb880060'"; 
		requestJsonStr += "    } ";
		requestJsonStr += " } ";
		requestJsonStr += "}  "; 
		this.doExecute("contentController.do?findContentDetail", requestJsonStr);
	} 
	@Test
	public void testSaveContent() {
		String requestJsonStr = "";
		requestJsonStr = "{ ";
		requestJsonStr += " 'Data': { ";
		requestJsonStr += "  'Head': {";
		requestJsonStr += "       'User': { ";
		requestJsonStr += "       } ";
		requestJsonStr += "    }, ";
		requestJsonStr += "   'Request': {   ";
		requestJsonStr += "            'categoryCode': '001001002', "; 
		requestJsonStr += "            'content': '111', "; 
		requestJsonStr += "            'mobilephone': '13707410987', "; 
		requestJsonStr += "            'userId': '4028813746a920bd0146a92a2c1e001b' "; 
		requestJsonStr += "    } ";
		requestJsonStr += " } ";
		requestJsonStr += "}  "; 
		this.doExecute("contentController.do?saveContent", requestJsonStr);
	} 
}
