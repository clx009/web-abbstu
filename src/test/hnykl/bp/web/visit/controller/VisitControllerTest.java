package test.hnykl.bp.web.visit.controller;

import org.junit.Test;

import test.BaseTest;

public class VisitControllerTest extends BaseTest{

	@Test
	public void testCreateVisit() {
		String requestJsonStr = "";
		requestJsonStr = "{ ";
		requestJsonStr += " 'Data': { ";
		requestJsonStr += "  'Head': {";
		requestJsonStr += "       'User': { "; 
		requestJsonStr += "       } ";
		requestJsonStr += "    }, ";
		requestJsonStr += "   'Request': {   ";  
		requestJsonStr += "            'visit':{'addenda': 'plus content',"; 
		requestJsonStr += "            'expectTime': '2016-07-06',"; 
		requestJsonStr += "            'creatorId':'40288105450658b0014506595c390044',"; 
		requestJsonStr += "            'status': '0',"; 
		requestJsonStr += "            'type':'1',"; 
		requestJsonStr += "            'visitOptions':";  
		requestJsonStr += "            		[";  
		requestJsonStr += "            			{";  
		requestJsonStr += "            				'option':{'id': '402847ea5561207501556120f0960001'}"; 
		requestJsonStr += "            			},";  
		requestJsonStr += "            			{";  
		requestJsonStr += "            				'option':{'id': '402847ea556120750155612118940003'}"; 
		requestJsonStr += "            			}";  
		requestJsonStr += "            		]"; 
		requestJsonStr += "            	}"; 
		requestJsonStr += "    } ";
		requestJsonStr += " } ";
		requestJsonStr += "}  ";
		this.doExecute("visitController.do?createVisit",
				requestJsonStr);
	}

	@Test
	public void testVisitList() {
		String requestJsonStr = "";
		requestJsonStr = "{ ";
		requestJsonStr += " 'Data': { ";
		requestJsonStr += "  'Head': {";
		requestJsonStr += "       'User': { "; 
		requestJsonStr += "       } ";
		requestJsonStr += "    }, ";
		requestJsonStr += "   'Request': {   ";
		requestJsonStr += "            'userId': '40288105450658b0014506595c390044',";  
		requestJsonStr += "            'type': '1,2'";  
		requestJsonStr += "    } ";
		requestJsonStr += " } ";
		requestJsonStr += "}  ";
		this.doExecute("visitController.do?visitList",
				requestJsonStr);
	}
	@Test
	public void testFindVisitOptionList() {
		String requestJsonStr = "";
		requestJsonStr = "{ ";
		requestJsonStr += " 'Data': { ";
		requestJsonStr += "  'Head': {";
		requestJsonStr += "       'User': { "; 
		requestJsonStr += "       } ";
		requestJsonStr += "    }, ";
		requestJsonStr += "   'Request': {   ";
		requestJsonStr += "     'type': '1'";
		requestJsonStr += "    } ";
		requestJsonStr += " } ";
		requestJsonStr += "}  ";
		this.doExecute("visitController.do?findVisitOptionList",
				requestJsonStr);
	}

	@Test
	public void testVisitDetail() {
		String requestJsonStr = "";
		requestJsonStr = "{ ";
		requestJsonStr += " 'Data': { ";
		requestJsonStr += "  'Head': {";
		requestJsonStr += "       'User': { "; 
		requestJsonStr += "       } ";
		requestJsonStr += "    }, ";
		requestJsonStr += "   'Request': {   ";
		requestJsonStr += "            'id': '402847ea55625f9301556266353a0000'";  
		requestJsonStr += "    } ";
		requestJsonStr += " } ";
		requestJsonStr += "}  ";
		this.doExecute("visitController.do?visitDetail",
				requestJsonStr);
	}

}
