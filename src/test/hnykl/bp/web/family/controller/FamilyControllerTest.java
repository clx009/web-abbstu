package test.hnykl.bp.web.family.controller;
 

import org.junit.Test;

import test.BaseTest;

public class FamilyControllerTest extends BaseTest{ 
	@Test
	public void testSendFamilyInviteCode() {
		String requestJsonStr = "";
		requestJsonStr = "{ ";
		requestJsonStr += " 'Data': { ";
		requestJsonStr += "  'Head': {";
		requestJsonStr += "       'User': { ";
		requestJsonStr += "            'userId': '402847ea54eb36020154eb3a57060006'";
		requestJsonStr += "       } ";
		requestJsonStr += "    }, ";
		requestJsonStr += "   'Request': {   ";
		requestJsonStr += "            'familyId': '402847ea54ec986d0154eca6c7240003'"; 
		requestJsonStr += "    } ";
		requestJsonStr += " } ";
		requestJsonStr += "}  "; 
		this.doExecute("familyController.do?sendFamilyInviteCode", requestJsonStr);
	} 

}
