package test.hnykl.bp.web.family.controller;
 

import org.junit.Test;

import test.BaseTest;

public class MemberControllerTest extends BaseTest{

	@Test
	public void testJoinFamily() {
		String sbStr = "";
		sbStr = "{ ";
		sbStr += " 'Data': { ";
		sbStr += "  'Head': {";
		sbStr += "       'User': { ";
		sbStr += "            'userId': '402847ea54cd9e680154cda7c0220001'";
		sbStr += "       } ";
		sbStr += "    }, ";
		sbStr += "   'Request': {   "; 
		sbStr += "            'inviteCode': '402847ea54efc9f90154efca43600000' ";
		sbStr += "    } ";
		sbStr += " } ";
		sbStr += "}  ";
		this.doExecute("memberController.do?joinFamily", sbStr); 
	}

	@Test
	public void testDropOutFamily() {
		String sbStr = "";
		sbStr = "{ ";
		sbStr += " 'Data': { ";
		sbStr += "  'Head': {";
		sbStr += "       'User': { ";
		sbStr += "       } ";
		sbStr += "    }, ";
		sbStr += "   'Request': {   "; 
		sbStr += "            'memberId': '402847ea552ec66201552ef6540d001f', ";
		sbStr += "            'groupId': '205328774223364524', ";
		sbStr += "            'userId': '402847ea552ec66201552ef58cf00018' ";
		sbStr += "    } ";
		sbStr += " } ";
		sbStr += "}  ";
		this.doExecute("memberController.do?dropOutFamily", sbStr); 
	}
	
	@Test
	public void testFindFamilyMembers() {
		String sbStr = "";
		sbStr = "{ ";
		sbStr += " 'Data': { ";
		sbStr += "  'Head': {";
		sbStr += "       'User': { ";
		sbStr += "       } ";
		sbStr += "    }, ";
		sbStr += "   'Request': {   "; 
		sbStr += "            'familyId': '402847ea552ec66201552ef5ae71001c' "; 
		sbStr += "    } ";
		sbStr += " } ";
		sbStr += "}  ";
		this.doExecute("memberController.do?findFamilyMembers", sbStr); 
	}

}
