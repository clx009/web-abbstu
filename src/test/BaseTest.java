package test;

import java.io.IOException;
import java.net.URLDecoder;
import java.net.URLEncoder;

import com.hnykl.bp.android.common.AndroidDataTranDefine;
import com.hnykl.bp.android.des.DESHelper;
import com.hnykl.bp.base.tool.http.HttpInvoker;

public class BaseTest {
	
	private String sKey = "72fif5zmyrPao8TvrsoabtEXKHiN1PAjjmZU20Vn";// "72fif5zmyrPao8TvrsoabtEXKHiN1PAjjmZU20Vn";

    private String url = "http://localhost:8082/web-abbstu/";
	//private String url = "http://112.74.72.83:8080/abbstu/";
	 
	public void doExecute(String actionName,String requestJsonStr){
		
		try {
			DESHelper desHelper = new DESHelper(sKey);
			requestJsonStr = desHelper.encrypt(requestJsonStr);
			requestJsonStr = URLEncoder.encode(requestJsonStr, "UTF-8");
			String paraStr = "requestJsonStr=" + requestJsonStr;
			String retStr = HttpInvoker.postData(url + actionName, paraStr);
			retStr = desHelper.decrypt(retStr.trim());
			retStr = URLDecoder.decode(retStr, "UTF-8");
			System.out.println(retStr);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
