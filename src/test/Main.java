package test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import com.hnykl.bp.easemob.server.api.*;
import com.hnykl.bp.easemob.server.comm.ClientContext;
import com.hnykl.bp.easemob.server.comm.EasemobRestAPIFactory;
import com.hnykl.bp.easemob.server.comm.body.ChatGroupBody;
import com.hnykl.bp.easemob.server.comm.body.IMUserBody;
import com.hnykl.bp.easemob.server.comm.body.IMUsersBody;
import com.hnykl.bp.easemob.server.comm.body.TextMessageBody;
import com.hnykl.bp.easemob.server.comm.wrapper.BodyWrapper;
import com.hnykl.bp.easemob.server.comm.wrapper.ResponseWrapper; 

public class Main {

	public static void main(String[] args) throws Exception {
		EasemobRestAPIFactory factory = ClientContext.getInstance().init(ClientContext.INIT_FROM_PROPERTIES).getAPIFactory();
		
		IMUserAPI user = (IMUserAPI)factory.newInstance(EasemobRestAPIFactory.USER_CLASS);
		ChatMessageAPI chat = (ChatMessageAPI)factory.newInstance(EasemobRestAPIFactory.MESSAGE_CLASS);
		FileAPI file = (FileAPI)factory.newInstance(EasemobRestAPIFactory.FILE_CLASS);
		SendMessageAPI message = (SendMessageAPI)factory.newInstance(EasemobRestAPIFactory.SEND_MESSAGE_CLASS);
		ChatGroupAPI chatgroup = (ChatGroupAPI)factory.newInstance(EasemobRestAPIFactory.CHATGROUP_CLASS);
		ChatRoomAPI chatroom = (ChatRoomAPI)factory.newInstance(EasemobRestAPIFactory.CHATROOM_CLASS);

//        ResponseWrapper fileResponse = (ResponseWrapper) file.uploadFile(new File("d:/logo.png"));
//        String uuid = ((ObjectNode) fileResponse.getResponseBody()).get("entities").get(0).get("uuid").asText();
//        String shareSecret = ((ObjectNode) fileResponse.getResponseBody()).get("entities").get(0).get("share-secret").asText();
//        InputStream in = (InputStream) ((ResponseWrapper) file.downloadFile(uuid, shareSecret, false)).getResponseBody();
//        FileOutputStream fos = new FileOutputStream("d:/logo1.png");
//        byte[] buffer = new byte[1024];
//        int len1 = 0;
//        while ((len1 = in.read(buffer)) != -1) {
//            fos.write(buffer, 0, len1);
//        }
//        fos.close();
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("x", "xxx");
		JSONObject msg = new JSONObject();
		msg.put("m", "mmm");
		BodyWrapper payload=new TextMessageBody("chatgroups", new String[]{"205328774223364524"}, "18073193108",null,"hello from rest"); 
		ResponseWrapper sendMessage = (ResponseWrapper)message.sendMessage(payload); 
		System.out.println(sendMessage.getResponseStatus());
//		// Create a IM user
//		BodyWrapper userBody = new IMUserBody("User105", "123456", "HelloWorld");
//		ResponseWrapper responseWrapper= (ResponseWrapper)user.createNewIMUserSingle(userBody);
//		user.deleteIMUserByUserName("User102");
//		user.deleteIMUserByUserName("User102");
//		ResponseWrapper responseWrapper= (ResponseWrapper)user.deleteIMUserByUserName("User102");
//		System.out.println(responseWrapper.getResponseStatus().toString());
		 /*
		// Create some IM users
		List<IMUserBody> users = new ArrayList<IMUserBody>();
		users.add(new IMUserBody("User002", "123456", null));
		users.add(new IMUserBody("User003", "123456", null));
		BodyWrapper usersBody = new IMUsersBody(users);
		user.createNewIMUserBatch(usersBody);
		
		// Get a IM user
		user.getIMUsersByUserName("User001");
		
		// Get a fake user
		user.getIMUsersByUserName("FakeUser001");
		
		// Get 12 users
		user.getIMUsersBatch(null, null);
		*/ 
		
		
		/*ChatGroupBody chatGroupBody = new ChatGroupBody("test0002","test0002", false, 5L, false, "User101",new String[] { "User101" });
		ResponseWrapper responseWraper2 = (ResponseWrapper) chatgroup.createChatGroup(chatGroupBody);
		Map<String,Map<String,String>> map = (Map<String, Map<String, String>>) responseWraper2.getResponseBody(); 
		System.out.println(map.get("data").get("groupId"));*/
		
		
	}

}
