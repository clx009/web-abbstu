package test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/*
 @RunWith(SpringJUnit4ClassRunner.class)
 // 使用junit4进行测试
 @ContextConfiguration({ "classpath:/config/spring/applicationContext.xml" })*/
public class MyTest {
	private Map result;
	Map<String,Object> jsonMap = new HashMap<String, Object>();
	@Test
	public void test() {
		String requestJsonStr = "";
		requestJsonStr = "{ ";
		requestJsonStr += " 'Data': { ";
		requestJsonStr += "  'Head': {";
		requestJsonStr += "       'User': { ";
		requestJsonStr += "            		'loginName': '18692219194',";
		requestJsonStr += "            		'password': '123',";
		requestJsonStr += "            		'userType': '1'";
		requestJsonStr += "       } ";
		requestJsonStr += "    }, ";
		requestJsonStr += "   'Request': {   ";
		requestJsonStr += "            'other': 'xxxx'";
		requestJsonStr += "    } ";
		requestJsonStr += " }, ";
		requestJsonStr += " 'Data2': { ";
		requestJsonStr += "  'Head2': {";
		requestJsonStr += "       'User2': { ";
		requestJsonStr += "            		'loginName2': '18692219194',";
		requestJsonStr += "            		'password2': '123',";
		requestJsonStr += "            		'userType2': '1'";
		requestJsonStr += "       } ";
		requestJsonStr += "    }, ";
		requestJsonStr += "   'Request': {   ";
		requestJsonStr += "            'other': 'xxxx'";
		requestJsonStr += "    } ";
		requestJsonStr += " } ";
		requestJsonStr += "}  ";
		Map map = new HashMap();
		JsonConfig jc = new JsonConfig();
		jc.setClassMap(map);
		jc.setRootClass(Map.class);
		jc.setArrayMode(JsonConfig.MODE_LIST);
		JSONObject jobj = JSONObject.fromObject(requestJsonStr, jc);
		Map<String, Object> decodeJSONObject = decodeJSONObject(jobj, null);
		Object object = decodeJSONObject.get("Data.Head.User.loginName");
		System.out.println(object);
	}

	public Map<String,Object>  decodeJSONObject(JSONObject json, String parentKey) {
		@SuppressWarnings("unchecked")
		Iterator<String> keys = json.keys();
		JSONObject sonJson = null;
		Object o;
		String key;
		while (keys.hasNext()) {
			key = keys.next();
			o = json.get(key);
			
			if (parentKey != null && !"".equals(parentKey)) {
				jsonMap.put(parentKey+"."+key, o);
			} else{
				jsonMap.put(key, o);
			}
			
			
			
			if (o instanceof JSONObject) {
				sonJson = (JSONObject) o;
				if (sonJson.keySet().size() > 0) {
					if (parentKey != null && !"".equals(parentKey)) {
						decodeJSONObject(sonJson, parentKey + "." + key);
					} else {
						decodeJSONObject(sonJson, key);
					}
				}
			}
		}
		return jsonMap;
	}

}
