package test;

public class TestJsonTemplate {

	public static String login(String userName, String password) {
		String sbStr = "";
		sbStr = "{ ";
		sbStr += " 'Data': { ";
		sbStr += "  'Head': {";
		sbStr += "       'User': { ";
		sbStr += "       } ";
		sbStr += "    }, ";
		sbStr += "   'Request': {   ";
		sbStr += "            'userName': '" + userName + "', ";
		sbStr += "            'password': '" + password + "', ";
		sbStr += "            'userType': '' ";
		sbStr += "    } ";
		sbStr += " } ";
		sbStr += "}  ";
		return sbStr;
	}

	public static String register(String userName, String password, String telValidateCode) {
		String sbStr = "";
		sbStr = "{ ";
		sbStr += " 'Data': { ";
		sbStr += "  'Head': {";
		sbStr += "       'User': { ";
		sbStr += "       } ";
		sbStr += "    }, ";
		sbStr += "   'Request': {   ";
		sbStr += "            'userName': '" + userName + "', ";
		sbStr += "            'password': '" + password + "', ";
		sbStr += "            'phoneValidateCode': '" + telValidateCode + "', ";
		sbStr += "            'userType': '' ";
		sbStr += "    } ";
		sbStr += " } ";
		sbStr += "}  ";
		return sbStr;
	}

	public static String sendTelValidateCode(String phoneNumber) {
		String sbStr = "";
		sbStr = "{ ";
		sbStr += " 'Data': { ";
		sbStr += "  'Head': {";
		sbStr += "       'User': { ";
		sbStr += "       } ";
		sbStr += "    }, ";
		sbStr += "   'Request': {   ";
		sbStr += "            'phoneNumber': '" + phoneNumber + "'"; 
		sbStr += "    } ";
		sbStr += " } ";
		sbStr += "}  ";
		return sbStr;
	}

	public static String isExsitsLoginName(String tPhoneNumber) {
		String sbStr = "";
		sbStr = "{ ";
		sbStr += " 'Data': { ";
		sbStr += "  'Head': {";
		sbStr += "       'User': { ";
		sbStr += "       } ";
		sbStr += "    }, ";
		sbStr += "   'Request': {   ";
		sbStr += "            'userName': '" + tPhoneNumber + "'"; 
		sbStr += "    } ";
		sbStr += " } ";
		sbStr += "}  ";
		return sbStr;
	}
	
	
	public static String thirdpartyLogin(String openId) {
		String sbStr = "";
		sbStr = "{ ";
		sbStr += " 'Data': { ";
		sbStr += "  'Head': {";
		sbStr += "       'User': { ";
		sbStr += "       } ";
		sbStr += "    }, ";
		sbStr += "   'Request': {   ";
		sbStr += "            'openId': '" + openId + "'"; 
		sbStr += "    } ";
		sbStr += " } ";
		sbStr += "}  ";
		return sbStr;
	}
	public static String modifyPasswordForForget(String loginName,String password,String telValidateCode) {
		String sbStr = "";
		sbStr = "{ ";
		sbStr += " 'Data': { ";
		sbStr += "  'Head': {";
		sbStr += "       'User': { ";
		sbStr += "       } ";
		sbStr += "    }, ";
		sbStr += "   'Request': {   ";
		sbStr += "            'loginName': '" + loginName + "', ";
		sbStr += "            'password': '" + password + "', ";
		sbStr += "            'telValidateCode': '" + telValidateCode + "'"; 
		sbStr += "    } ";
		sbStr += " } ";
		sbStr += "}  ";
		return sbStr;
	}
	public static String thirdpartyRegister(String openId,String thirdpartyLoginType,String phoneNumber,String telValidateCode) {
		String sbStr = "";
		sbStr = "{ ";
		sbStr += " 'Data': { ";
		sbStr += "  'Head': {";
		sbStr += "       'User': { ";
		sbStr += "       } ";
		sbStr += "    }, ";
		sbStr += "   'Request': {   ";
		sbStr += "            'openId': '" + openId + "', ";
		sbStr += "            'thirdpartyLoginType': '" + thirdpartyLoginType + "', ";
		sbStr += "            'phoneNumber': '" + phoneNumber + "', ";
		sbStr += "            'telValidateCode': '" + telValidateCode + "'"; 
		sbStr += "    } ";
		sbStr += " } ";
		sbStr += "}  ";
		return sbStr;
	}
	public static String changeProfile(String username,String timeZone,String nickname,String messageAlert,String realName) {
		String sbStr = "";
		sbStr = "{ ";
		sbStr += " 'Data': { ";
		sbStr += "  'Head': {";
		sbStr += "       'User': { ";
		sbStr += "       } ";
		sbStr += "    }, ";
		sbStr += "   'Request': {   ";
		sbStr += "            'username': '" + username + "', ";
		sbStr += "            'timeZone': '" + timeZone + "', ";
		sbStr += "            'nickname': '" + nickname + "', ";
		sbStr += "            'messageAlert': '" + messageAlert + "', ";
		sbStr += "            'realName': '" + realName + "'"; 
		sbStr += "    } ";
		sbStr += " } ";
		sbStr += "}  ";
		return sbStr;
	}
	
	public static String changePassword(String username,String currentPassword,String newPassword) {
		String sbStr = "";
		sbStr = "{ ";
		sbStr += " 'Data': { ";
		sbStr += "  'Head': {";
		sbStr += "       'User': { ";
		sbStr += "            'username': '" + username + "', ";
		sbStr += "       } ";
		sbStr += "    }, ";
		sbStr += "   'Request': {   ";
		sbStr += "            'currentPassword': '" + currentPassword + "', ";
		sbStr += "            'newPassword': '" + newPassword + "', "; 
		sbStr += "    } ";
		sbStr += " } ";
		sbStr += "}  ";
		return sbStr;
	}
	
	public static String changePhoneNumber(String username,String newPhoneNumber,String phoneValidateCode) {
		String sbStr = "";
		sbStr = "{ ";
		sbStr += " 'Data': { ";
		sbStr += "  'Head': {";
		sbStr += "       'User': { ";
		sbStr += "            'username': '" + username + "', ";
		sbStr += "       } ";
		sbStr += "    }, ";
		sbStr += "   'Request': {   ";
		sbStr += "            'newPhoneNumber': '" + newPhoneNumber + "', ";
		sbStr += "            'phoneValidateCode': '" + phoneValidateCode + "', "; 
		sbStr += "    } ";
		sbStr += " } ";
		sbStr += "}  ";
		return sbStr;
	}
	
	public static String changeEmail(String username,String email,String phoneValidateCode) {
		String sbStr = "";
		sbStr = "{ ";
		sbStr += " 'Data': { ";
		sbStr += "  'Head': {";
		sbStr += "       'User': { ";
		sbStr += "            'username': '" + username + "', ";
		sbStr += "       } ";
		sbStr += "    }, ";
		sbStr += "   'Request': {   ";
		sbStr += "            'email': '" + email + "', ";
		sbStr += "            'phoneValidateCode': '" + phoneValidateCode + "', "; 
		sbStr += "    } ";
		sbStr += " } ";
		sbStr += "}  ";
		return sbStr;
	}
}
