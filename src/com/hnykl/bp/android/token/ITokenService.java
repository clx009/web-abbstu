package com.hnykl.bp.android.token;

/**
 * <p>
 *  令牌服务，用于安全管理
 * </p>
 *
 */
public interface ITokenService {
	/**
	 * 产生新令牌
	 * @param loginName
	 * @param permissionCodes
	 * @return
	 */
	public String getToken(String loginName,String password,String [] permissionCodes);
	/**
	 * 
	 * @param enCodetoken 加密的token
	 * @return
	 * @throws SecurityException
	 */
	public TokenEntity verifyToken(String enCodetoken) throws SecurityException;
}
