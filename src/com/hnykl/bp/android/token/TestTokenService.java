package com.hnykl.bp.android.token;
import junit.framework.TestCase;

import com.hnykl.bp.android.token.impl.TokenServiceImpl;

/**
 * <p>
 *  测试
 * </p>
 */
public class TestTokenService extends TestCase {
	public void test(){
		for (int i = 0; i < 10; i++) {
			long t=System.currentTimeMillis();
			TokenServiceImpl service = new TokenServiceImpl();
			String loginName = "650105701230007";
			String password = "password";
			String[] permissionCodes = null;
			String token = service.getToken(loginName, password, permissionCodes);
			System.out.println(token);
			try {
				TokenEntity verifyToken = service.verifyToken(token);
				System.out.println(verifyToken.getLoginName());
				System.out.println(verifyToken.getTimestamp());
				System.out.println(verifyToken.toString());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("offset="+(System.currentTimeMillis()-t));

		}
	}
	
	public void test2(){
		try {
			TokenServiceImpl service = new TokenServiceImpl();
			TokenEntity verifyToken=service.verifyToken("tASh0Z8XPZP5BgynifRP0DYSg0cRufEHingQLtlDiK8y3XkGG8pITC39lUECCx52");
			System.out.println(verifyToken.getLoginName());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void test3(){
		try {
			TokenServiceImpl service = new TokenServiceImpl();
			String token = service.getToken("44123232132", null, null);
			System.out.println(token);
			
			TokenEntity verifyToken=service.verifyToken(token);
			System.out.println(verifyToken.getLoginName());
			token = verifyToken.getNewEncodeToken();
			System.out.println(token);
			verifyToken=service.verifyToken(token);
			System.out.println(verifyToken.getLoginName());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}	
	
}
