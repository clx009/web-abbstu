package com.hnykl.bp.android.token.impl;

import java.util.Date;

import org.apache.log4j.Logger;

import com.hnykl.bp.android.token.AESCoder;
import com.hnykl.bp.android.token.Base64Encoder;
import com.hnykl.bp.android.token.ITokenService;
import com.hnykl.bp.android.token.TokenEntity;
import com.hnykl.bp.web.system.common.AndroidUserConfigMgr;
/**
 * <p>
 * 产生新令牌
 * </p>
 * 
 */
public class TokenServiceImpl implements ITokenService {
	private static final Logger log=Logger.getLogger(TokenServiceImpl.class);
	public final static byte[] key = { 116, -97, -29, -113, 23, -30, 12, 56, -98, -33, 74, -92, 34, -71, 29, 59 };

	public String getToken(String loginName,String password) {
		TokenEntity token = new TokenEntity();
		token.setLoginName(loginName);
		token.setPassword(password);
		String ret = null;
		try {
			byte[] bytes = AESCoder.encrypt(token.getNewToken().getBytes(), key);
			ret =  new String( Base64Encoder.encode(bytes));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}
		
	public String getToken(String loginName,String password, String[] permissionCodes) {
		TokenEntity token = new TokenEntity();
		token.setLoginName(loginName);
		token.setPassword(password);
		token.setPermissionCode(permissionCodes);
		String ret = null;
		try {
			byte[] bytes = AESCoder.encrypt(token.getNewToken().getBytes(), key);
			ret =  new String( Base64Encoder.encode(bytes));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}
	

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.foresee.fstax.adobe.token.ITokenService#verifyToken(java.lang.String)
	 */
	public TokenEntity verifyToken(String encodeToken) throws SecurityException {
		byte[] data = Base64Encoder.decode(encodeToken);
		TokenEntity token = null;
		int timeout = AndroidUserConfigMgr.getAndroidTokenTimeout();
		if (timeout == 0) {
			// 60秒超时时间f
			timeout = 60 * 000;
		}
		try {
			byte[] bytes = AESCoder.decrypt(data, key);
			token = new TokenEntity(new String(bytes));
		} catch (Exception e) {
			log.warn("解析token异常！"+encodeToken,e);
			throw new SecurityException("解析token异常！",e);
		}
		if ((token.getTimestamp() + timeout) < System.currentTimeMillis()) {
			log.warn("token验证时间超时："+new Date(token.getTimestamp()));
			throw new SecurityException("token验证时间超时!");
		}
		return token;
	}
	
	public String getLoginName(String encodeToken) throws SecurityException {
		byte[] data = Base64Encoder.decode(encodeToken);
		TokenEntity token = null;
		try {
			byte[] bytes = AESCoder.decrypt(data, key);
			token = new TokenEntity(new String(bytes));
		} catch (Exception e) {
			log.warn("解析token异常！"+encodeToken,e);
			throw new SecurityException("解析token异常！",e);
		}
		return (token != null)?token.getLoginName():null;
	}

}
