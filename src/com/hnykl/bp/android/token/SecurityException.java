package com.hnykl.bp.android.token;

/**
 * <p>
 *  安全异常
 * </p>
 *
 */
public class SecurityException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7571870421485064830L;

	public SecurityException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SecurityException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public SecurityException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public SecurityException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
