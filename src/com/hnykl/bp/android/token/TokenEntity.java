package com.hnykl.bp.android.token;

import com.hnykl.bp.android.token.impl.TokenServiceImpl;


/**
 * <p>
 * 
 * </p>
 * 
 * @version $LastChangedRevision: 0 $ 
 */
public class TokenEntity {

	/**
	 * 登录名
	 */
	private String loginName;
	/**
	 * 密码
	 */
	private String password;
	/**
	 * 时间戳
	 */
	private long timestamp;
	/**
	 * 权限代码
	 */
	private String[] permissionCode;

	/**
	 * @return the loginName
	 */
	public String getLoginName() {
		return loginName;
	}

	/**
	 * @param loginName
	 *            the loginName to set
	 */
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	/**
	 * @return the timestamp
	 */
	public long getTimestamp() {
		return timestamp;
	}

	/**
	 * @param timestamp
	 *            the timestamp to set
	 */
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * @return the permissionCode
	 */
	public String[] getPermissionCode() {
		return permissionCode;
	}

	/**
	 * @param permissionCode
	 *            the permissionCode to set
	 */
	public void setPermissionCode(String[] permissionCode) {
		this.permissionCode = permissionCode;
	}

	public TokenEntity() {
	}

	/**
	 * 
	 * @param token
	 */
	public TokenEntity(String token) throws SecurityException {
		try {
			String[] strs = token.split(",");
			loginName = strs[0];
			password = strs[1];
			timestamp = Long.parseLong(strs[2]);
			if (strs.length > 3) {
				permissionCode = new String[strs.length - 3];
				for (int i = 0; i < strs.length - 3; i++) {
					permissionCode[i] = strs[2 + i];
				}
			}

		} catch (Exception e) {
			throw new SecurityException("解析token出错！", e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		String ret = loginName+"," + password+"," + timestamp;
		if (permissionCode != null) {
			for (int i = 0; i < permissionCode.length; i++) {
				ret +=","+ permissionCode[i];
			}
		}
		return ret;
	}

	public String getNewToken() {
		String ret = loginName+"," + password +","+ System.currentTimeMillis();
		if (permissionCode != null) {
			for (int i = 0; i < permissionCode.length; i++) {
				ret += ","+permissionCode[i];
			}
		}
		return ret;
	}
	public String getNewEncodeToken() {
		String ret = null;
		try {
			byte[] bytes = AESCoder.encrypt(getNewToken().getBytes(), TokenServiceImpl.key);
			ret = new String( Base64Encoder.encode(bytes));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
}
