package com.hnykl.bp.android.des;

import java.io.IOException;
import java.net.URLDecoder;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

public class DESHelper {

 

    private byte[] desKey;

 

    public DESHelper(String desKey) {

        this.desKey = desKey.getBytes();

    }

 

    public byte[] desEncrypt(byte[] plainText) throws Exception {

        SecureRandom sr = new SecureRandom();

        byte rawKeyData[] = desKey;

        DESKeySpec dks = new DESKeySpec(rawKeyData);

        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");

        SecretKey key = keyFactory.generateSecret(dks);

        Cipher cipher = Cipher.getInstance("DES");

        cipher.init(Cipher.ENCRYPT_MODE, key, sr);

        byte data[] = plainText;

        byte encryptedData[] = cipher.doFinal(data);

        return encryptedData;

    }

 

    public byte[] desDecrypt(byte[] encryptText) throws Exception {

        SecureRandom sr = new SecureRandom();

        byte rawKeyData[] = desKey;

        DESKeySpec dks = new DESKeySpec(rawKeyData);

        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");

        SecretKey key = keyFactory.generateSecret(dks);

        Cipher cipher = Cipher.getInstance("DES");

        cipher.init(Cipher.DECRYPT_MODE, key, sr);

        byte encryptedData[] = encryptText;

        byte decryptedData[] = cipher.doFinal(encryptedData);

        return decryptedData;

    }

 

    public String encrypt(String input) throws Exception {

        return base64Encode(desEncrypt(input.getBytes()));

    }

    public String decrypt(String input) throws Exception {
    	String ss="";
    	try{
	        byte[] result = base64Decode(input);
	        ss= new String(desDecrypt(result));
    	}catch(Exception ex){
    		ex.printStackTrace();
    	}
        return ss;
//        return  parseByte2HexStr(desDecrypt(result));

    }

 
    public static String base64Encode(byte[] s) {

        if (s == null)

            return null;

        BASE64Encoder b = new sun.misc.BASE64Encoder();

        return b.encode(s);

    }

 

    public static byte[] base64Decode(String s) throws IOException {

        if (s == null)

            return null;

        BASE64Decoder decoder = new BASE64Decoder();

        byte[] b = decoder.decodeBuffer(s);

        return b;

    }
    
	/**将二进制转换成16进制 
	 * @param buf 
	 * @return 
	 */  
	public  String parseByte2HexStr(byte buf[]) {  
	        StringBuffer sb = new StringBuffer();  
	        for (int i = 0; i < buf.length; i++) {  
	                String hex = Integer.toHexString(buf[i] & 0xFF);  
	                if (hex.length() == 1) {  
	                        hex = '0' + hex;  
	                }  
	                sb.append(hex.toUpperCase());  
	        }  
	        return sb.toString();  
	}  

	/**将16进制转换为二进制 
	 * @param hexStr 
	 * @return 
	 */  
	public  byte[] parseHexStr2Byte(String hexStr) {  
	        if (hexStr.length() < 1)  
	                return null;  
	        byte[] result = new byte[hexStr.length()/2];  
	        for (int i = 0;i< hexStr.length()/2; i++) {  
	                int high = Integer.parseInt(hexStr.substring(i*2, i*2+1), 16);  
	                int low = Integer.parseInt(hexStr.substring(i*2+1, i*2+2), 16);  
	                result[i] = (byte) (high * 16 + low);  
	        }  
	        return result;  
	} 

    public static void main(String []arg){
    	try{
    		String key = "72fif5zmyrPao8TvrsoabtEXKHiN1PAjjmZU20Vn";
	    	DESHelper dESHelper = new  DESHelper(key);//"ED5wLgc3");
	    	String aftStr = dESHelper.decrypt("KYIfAehKhlEthF6P7l8L7VAt7QD1ZkWi31kCk7zg5ig9BKgg7Fv8Oi1joMUoSPTogYOYOOSid5HOSX3SBD3792WvKq0PX1zpEz1St7/vie0Wl3MbAxnxyBEYFmmeswsfnCdyLiXPZnkS5Dk1NmOpXcE7fFWB3sZD+vZABSXGjIdKfsuUMzAxqCQPDFq8im5HsC8EUfWt70sMM3WeiiwKQH6amhDWf0OYHOOOV/qZZHWAfqBdaEW6gEfRWr2ZN1OLV2Udes1qq/QwvY+BkAhOgq4hZ0pUUB/AEmU5FBBvX/NoQLEeE+zeTtVsScfCDZIALvRaDXMwF/gLnlqVGfnJwPlsdiJE5qoL3sqTF2D4fsi53ob7WTyJwB86LRbIbn1Z17VxBI3Ib8DaFQ9R7Xo8loUbxYIf9Zc9/F9KEIlAFdh8WGAV+O2zmwD6PURWn9jXfSM15Ha9wxWt+uJA7gxSfvBRWKDYuNyvrrXx7P/D8EA400uGRigHwGvq+c2+ILcQ1hUiOjhy2Vnf7izlun7EKTBS0hYzr/WtOvGgfYvU/LPkyQYxAOZkS8fCSGsMWWzSMh3uNzHjf7V8WmybjdvQ9DkelS+2RtJSrEIaPj/GWbTCu08d9kwv5NvYr/QVXEB17w/CzyGGxaUKer0dFHmf8kPNDAViFsMUESdjYRkd3Rv1Qnqpzntocd3vcBgAeruDGOjhy2Vnf7Djx/UUKOvb3LTMTfREoqk9xhZpzRoMnHxtiFoEarAFrpXk7DI0FpFLmEUP+cxYVl2u+jCuEiilvzuW9pfWAYGz5PXvgHM3VJKYUi0rA0H01FRTpcWOy+BabfIPfCKpxPDvXPXebcTA4COIHdX08vLsem/AreCesqKiR+twO8GjnGmalZEPJCS71kZe3PVmEOM9fGFE0hEQ4D54Xv9hSz/ogxBGJwsyBYdP0GYlt/VwXibX1txmWeZmChW+Ea3+avNYpDlz+NAYQx/UKt7HVJHEJ1+HOAmqwQsT1gF+CLWXlhMaFPlJyyyuQsJKJMiH70VMVB0hLmR8d7Mkjzs5KQF0/5x2awSVxG/NZwUffQIv+aylJfszvnJ3uDjAOiaemQE9Mgj2p4jWwgwEi7QA5cOlI9ttD4ZOgFCE/JUnoqJH63A7waP42Q18Ccrw9KFXEbeIPlqh7sNmNDTug/CjvOOTeSrI7e5Etsp7JiRhnoXzwYvj8m7bNkKFjTDtJttREQL5D9HYr/QVXEB17z4hDw6McEMsXm2cRjJ9N0KAwKr6sjJLjiUYBkAzy899lQbkSdspf9JyGPre4sgoNseTjkCNJvCoi8oEoyQDvVseTm26YzeVz4nAKahxENjQoooVj+UoWQWoE6BFo2sYNU8yqulVuvTjbn+oZAjwvye7HmzOYn2KZW0wSa3v3dIXG4cIC/jn4CGTP6hU4659ai4ndWNrbc5t69U3931DVCeqnOe2hx0c+xgi5jCoxXNRntpbz5OJYr/QVXEB17y0LvMLUdcro4a+ZutbuOoy65i2WFrX8xA6dsyPx9rVsQ1L6CHRsQCnySXFjpkHZoI4H7900r0dNAyfRTCn7ohOd0Rn3iHfhDYolRzh9K3y+BJHu1jF+0m5HsC8EUfWsDS03b0WGPWsRU5Tv6rwDvkz+Y/UpfySQ1QMca5vcyprTv6Fw7VLAQ4D54Xv9hSyAvzROVv+YoOqYI5/n4t4X82GxFvQkm9D1l8RCAQX6z1NVHr9XwPFGqc+OPBCUR/vnJ3uDjAOidjlp5EFR9SvxuiY7rTnrkdp6zDJFzknu8ZW3e9fsCy+hU46WrJrk+Eo5jMGkoRAd0O5yIDKS8n+w8/4PI6NkcD5N8HSTw6wyVpmZWqcsOIDajf/dYkpF9pzDJFzknu4pV8w6ulOQz/ge38z++S5zYKxZRcHrqbRFzKN4GQqfx3fgosnrXMMRaJ+W9mrK9NTwco34gk+kf6dj+GfnQIb3cXUvXDJIOw4glnbhBwEXbQ6I5BGion2OyU/ZyuKQYsU6rGN48osFYj56MQhnBIw9EtlPhCI4XcJ+1mVxHjJT6PZ/yhZqzf45Rf8epeNeeC6iIxLO88ihg0fL1lxELZtWWHvV0VZ+zPoE+2C5xdJSreCECyjRhyft2xdBYdQ5uR7AvBFH1rK1TVu+UCaSitZtjbQuV7uPZSq86Sld6ZfM0sApSKah8PNtLfEjcg7xI4aEd3aK4SBe6aNFb+nK2aqFAKefZW4fwH2AyYXWi5AbWZBGB/KHTzQNAdBCTF7ZRX+HWNxL+UwcAFr1kyr5+kGM6cSKGqLvGWvenacUYbP0KXMv0KnYITmoIqrYfEOQcCdrstS++lcGo9fVDT76Vwaj19UNPvpXBqPX1Q0M7aIu1tLbZ1tvQ/BC9uvJmBUmzfSds3o0EU4dLY2IC2aqFAKefZW4fwH2AyYXWi5AbWZBGBo/KHTzQNAdBCTF7ZRX+HWNxL+UwcAFr1kyr5kGM6cSKGqLvGWvenacUYbP0KXMv0KnYITmoIqrY9fEOQcCdrstS++lcGo9fVDT76Vwaj19UNPvpBqPX1Q0ArotkubEApai2suduO/35QGQoHfFOSrUX9jwQEmTnWsnx4o5WfI8n67VbvJU9q4Hv8Fye+km3k0OTzGjXaAv0asa+SBS/ujUJAROEm0VPqLS+6vQyWnVY2wvSdkrfoucCJUCj0lEeoFsmmDxkf/g==");
	    	System.out.println("解密之前:"+aftStr);
	    	System.out.println(URLDecoder.decode(aftStr,"UTF-8"));
    	}catch(Exception ex){
    		ex.printStackTrace();
    	}
    }

}