package com.hnykl.bp.android.cache;

import java.util.Iterator;
import java.util.List;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.io.Resource;

public class EhCacheFactory implements InitializingBean, DisposableBean {
	private Resource configLocation;

	private boolean shared = false;

	private CacheManager cacheManager;
	
	public CacheManager getCacheManager() {
		return cacheManager;
	}

	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}

	private List cacheList=null;
	
	
	public Cache getCacheByName(String name){
		return cacheManager.getCache(name);
	}
	
	public void afterPropertiesSet() throws Exception {

		if (this.shared) {
			if (this.configLocation != null) {
				this.cacheManager = CacheManager.create(this.configLocation
						.getInputStream());
			} else {
				this.cacheManager = CacheManager.create();
			}
		} else {
			if (this.configLocation != null) {
				this.cacheManager = new CacheManager(this.configLocation
						.getInputStream());
			} else {
				this.cacheManager = new CacheManager();
			}
		}
		
		if (cacheList!=null){
			for (Iterator cacheNameI=cacheList.iterator();cacheNameI.hasNext();){
				String cacheName=(String)cacheNameI.next();
				if (!this.cacheManager.cacheExists(cacheName)) {
					this.cacheManager.addCache(cacheName);
				}else{
					
				}
			}
		}
	}
	
	public void setConfigLocation(Resource configLocation) {
		this.configLocation = configLocation;
	}

	public void setShared(boolean shared) {
		this.shared = shared;
	}
	

	public void destroy() {

		this.cacheManager.shutdown();
	}

	public List getCacheList() {
		return cacheList;
	}

	public void setCacheList(List cacheList) {
		this.cacheList = cacheList;
	}

}
