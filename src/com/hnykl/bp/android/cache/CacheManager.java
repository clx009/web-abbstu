package com.hnykl.bp.android.cache;
import net.sf.ehcache.Cache;
import net.sf.ehcache.Element;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
/**
 * cache管理
 */
public class CacheManager implements InitializingBean,DisposableBean {

	private EhCacheFactory cacheFactory=null;
	public static Cache tokenCacheByLoginName = null;
	
	
	public static long timeToIdle = 1000;

	public static long timeToLive = 1000;

	
	public void afterPropertiesSet() throws Exception {
		tokenCacheByLoginName=cacheFactory.getCacheByName("tokenCacheByLoginName");
		/*timeToIdle=tokenCacheByLoginName.getTimeToIdleSeconds();
		timeToLive=tokenCacheByLoginName.getTimeToLiveSeconds();  */ 
		
	}
	/**
	 * 从cache中得到token
	 * @param loginName
	 * @return
	 */
	public static String getTokenByLoginName(String loginName){
		Element elem=tokenCacheByLoginName.get(loginName);
		if (elem!=null) {
			return (String)elem.getObjectValue();
		}
		return "";
	}
	/**
	 * 往cache中设置token值
	 * @param loginName
	 */
	protected static void setTokenByLoginName(String loginName,String token){
		tokenCacheByLoginName.put(new Element(loginName,token));
	}
	/**
	 * 移除Token
	 * @param loginName
	 * @return
	 */
	public static boolean removeTokenByLoginName(String loginName){
		return tokenCacheByLoginName.remove(loginName);
	}
	/**
	 * 先移除再设值
	 * @param loginName
	 */
	public static void setToken(String loginName,String token){
		removeTokenByLoginName(loginName);
		setTokenByLoginName(loginName,token);
	}
	
	public static void clearTokenCacheByLoginName(){
		tokenCacheByLoginName.removeAll();
	}

	public void destroy() {
		tokenCacheByLoginName = null;
		timeToIdle=1000;
		timeToLive=1000;
		cacheFactory.destroy();
	}
	public EhCacheFactory getCacheFactory() {
		return cacheFactory;
	}

	public void setCacheFactory(EhCacheFactory cacheFactory) {
		this.cacheFactory = cacheFactory;
	}
}
