package com.hnykl.bp.android.json;

import org.apache.commons.lang.StringUtils;

/**
 * json字符串模板类
 */
public class JsonTemplate {
	/**
	 * 得到用户信息的json请求方法
	 * @param ticketKey
	 * @return
	 */
	public static String getUserInfoReqJsonStr(String ticketKey,String appId){
		String sbStr =  "";
    	sbStr =	"{ " ;
    	sbStr   += " 'Data': { " ;
    	sbStr   +=  "  'Head': {" ;
    	sbStr   +=  "       'User': { " ;
    	sbStr    +=  "            'LoginName': '', " ;
    	sbStr    +=  "            'Password': '', ";
    	sbStr    +=  "            'UserType': '' ";
    	sbStr    +=  "       } ";
    	sbStr    +=  "    }, ";
    	sbStr    +=  "   'Request': {   ";
    	sbStr    +=  "            'TicketKey': '"+ticketKey+"', " ;
    	sbStr    +=  "            'AppId': '"+appId+"', " ;
    	sbStr    +=  "    } ";
    	sbStr    +=  " } ";
    	sbStr   +=  "}  ";
    	return sbStr;
	}
	/**
	 * 得到注册请求json字符串
	 * @return
	 */
	public static  String getRegReqJsonStr(){
	    	String sbStr =  "";
	    	sbStr =	"{ " ;
	    	sbStr    += " 'Data': { " ;
	    	sbStr    +=  "  'Head': {" ;
	    	sbStr    +=  "       'User': { " ;
	    	sbStr    +=  "            'LoginName': '', " ;
	    	sbStr    +=  "            'Password': '', ";
	    	sbStr    +=  "            'UserType': '' ";
	    	sbStr    +=  "       } ";
	    	sbStr    +=  "    }, ";
	    	sbStr    +=  "   'Request': {   ";
	    	sbStr    +=  "    } ";
	    	sbStr    +=  " } ";
	    	sbStr   +=  "}  ";
	    	return sbStr;
	    }
	/**
	 * 得到登出的json str
	 * @return
	 */
	public static String getLoginOutJsonStr(String loginName,String appId){
		if(StringUtils.isEmpty(loginName) || StringUtils.isEmpty(appId))return null;
		String sbStr = "";
		sbStr =	"{ " ;
    	sbStr    += " 'Data': { " ;
    	sbStr    +=  "  'Head': {" ;
    	sbStr    +=  "       'User': { " ;
    	sbStr    +=  "            'LoginName': '', " ;
    	sbStr    +=  "            'Password': '', ";
    	sbStr    +=  "            'UserType': '' ";
    	sbStr    +=  "       } ";
    	sbStr    +=  "    }, ";
    	sbStr    +=  "   'Request': {   ";
    	sbStr    +=  "    'LoginName': '"+loginName+"',";
    	sbStr    +=  "    'AppId': '"+appId+"'";
    	sbStr    +=  "    } ";
    	sbStr    +=  " } ";
    	sbStr   +=  "}  ";
		
		return sbStr;
	}
	/**
	 * 得到修改用户信息的json字符串
	 * @return
	 */
	public static String getModifyUserJsonStr(String loginName,String appId){
		if(StringUtils.isEmpty(loginName) || StringUtils.isEmpty(appId))return "";
		String sbStr =  "";
    	sbStr =	"{ " ;
    	sbStr   += " 'Data': { " ;
    	sbStr   +=  "  'Head': {" ;
    	sbStr   +=  "       'User': { " ;
    	sbStr    +=  "            'LoginName': '', " ;
    	sbStr    +=  "            'Password': '', ";
    	sbStr    +=  "            'UserType': '' ";
    	sbStr    +=  "       } ";
    	sbStr    +=  "    }, ";
    	sbStr    +=  "   'Request': {   ";
    	sbStr    +=  "            'LoginName': '"+loginName+"', " ;
    	sbStr    +=  "            'AppId': '"+appId+"', ";
    	sbStr    +=  "            'UserItems':''";
    	sbStr    +=  "    }";
    	sbStr    +=  " }";
    	sbStr   +=  "}";
    	return sbStr;
	}
	/**
	 * 得到添加用户json字符串
	 * @param loginName
	 * @param appId
	 * @return
	 */
	public static String getAddUserJsonStr(String loginName,String appId){
		if(StringUtils.isEmpty(loginName) || StringUtils.isEmpty(appId))return "";
		String sbStr =  "";
    	sbStr =	"{ " ;
    	sbStr   += " 'Data': { " ;
    	sbStr   +=  "  'Head': {" ;
    	sbStr   +=  "       'User': { " ;
    	sbStr    +=  "            'LoginName': '', " ;
    	sbStr    +=  "            'Password': '', ";
    	sbStr    +=  "            'UserType': '' ";
    	sbStr    +=  "       } ";
    	sbStr    +=  "    }, ";
    	sbStr    +=  "   'Request': {   ";
    	sbStr    +=  "            'LoginName': '"+loginName+"', " ;
    	sbStr    +=  "            'AppId': '"+appId+"', ";
    	sbStr    +=  "            'UserItems':''";
    	sbStr    +=  "    }";
    	sbStr    +=  " }";
    	sbStr   +=  "}";
    	return sbStr;
	}
	/**
	 * 检查是否存在相同的用户json字符串
	 * @param loginName
	 * @param appId
	 * @return
	 */
	public static String getIsExistUserJsonStr(String loginName,String appId){
		if(StringUtils.isEmpty(loginName) || StringUtils.isEmpty(appId))return "";
		String sbStr =  "";
		sbStr =	"{ " ;
		sbStr   += " 'Data': { " ;
		sbStr   +=  "  'Head': {" ;
		sbStr   +=  "       'User': { " ;
		sbStr    +=  "            'LoginName': '', " ;
		sbStr    +=  "            'Password': '', ";
		sbStr    +=  "            'UserType': '' ";
		sbStr    +=  "       } ";
		sbStr    +=  "    }, ";
		sbStr    +=  "   'Request': {   ";
		sbStr    +=  "            'LoginName': '"+loginName+"', " ;
		sbStr    +=  "            'AppId': '"+appId+"'";
		sbStr    +=  "    }";
		sbStr    +=  " }";
		sbStr   +=  "}";
		return sbStr;
	}
	/**
	 * 登录json字符串
	 * @param loginName
	 * @param appId
	 * @return
	 */
	public static String getLoginJsonStr(String loginName,String password,String appId){
		if(StringUtils.isEmpty(loginName) || StringUtils.isEmpty(appId))return "";
		String sbStr =  "";
		sbStr =	"{ " ;
		sbStr   += " 'Data': { " ;
		sbStr   +=  "  'Head': {" ;
		sbStr   +=  "       'User': { " ;
		sbStr    +=  "            'LoginName': '', " ;
		sbStr    +=  "            'Password': '', ";
		sbStr    +=  "            'UserType': '' ";
		sbStr    +=  "       } ";
		sbStr    +=  "    }, ";
		sbStr    +=  "   'Request': {   ";
		sbStr    +=  "            'LoginNameOrMobilephone': '"+loginName+"', " ;
		sbStr    +=  "            'LoginName': '"+loginName+"', " ;
		sbStr    +=  "            'Password': '"+password+"', " ;
		sbStr    +=  "            'AppId': '"+appId+"'";
		sbStr    +=  "    }";
		sbStr    +=  " }";
		sbStr   +=  "}";
		return sbStr;
	}
	/**
	 * 得到直接跳转的Json字符串
	 * @return
	 */
	public static String getRedirectJsonStr(String loginName,String password,String turl,String appId){
		String sbStr = "";
		if(StringUtils.isEmpty(loginName) || StringUtils.isEmpty(password)
				||  StringUtils.isEmpty(turl) || StringUtils.isEmpty(appId))return sbStr;
		sbStr =	"{ " ;
    	sbStr   += " 'Data': { " ;
    	sbStr   +=  "  'Head': {" ;
    	sbStr   +=  "       'User': { " ;
    	sbStr    +=  "            'LoginName': '', " ;
    	sbStr    +=  "            'Password': '', ";
    	sbStr    +=  "            'UserType': '' ";
    	sbStr    +=  "       } ";
    	sbStr    +=  "    }, ";
    	sbStr    +=  "   'Request': {   ";
    	sbStr    +=  "            'AppId': '"+appId+"',";
    	sbStr    +=  "            'LoginName': '"+loginName+"'," ;
    	sbStr    +=  "            'Password':'"+password+"',";
    	sbStr    +=  "            'Turl':'"+turl+"'";
    	sbStr    +=  "    }";
    	sbStr    +=  " }";
    	sbStr   +=  "}";
		return sbStr;
	}

}
