package com.hnykl.bp.android.json;

import org.apache.log4j.Logger;

import com.hnykl.bp.android.des.DESHelper;
import com.hnykl.bp.web.system.common.AndroidUserConfigMgr;
/**
 * json工具类
 */
public class JsonUtils {
	private static Logger logger = Logger.getLogger(JsonUtils.class);
	 /**
     * 往用户中心加密json字符串
     * @param jsonStr
     * @return
     */
    public static String getUumsDesEnJsonStr(String jsonStr){
		String desStr ="";
		try{
			String deskey = AndroidUserConfigMgr.getAndroidDesKey();
			DESHelper dESHelper = new DESHelper(deskey);
			desStr = dESHelper.encrypt(jsonStr);
		}catch(Exception ex){
			desStr = jsonStr;
			logger.error("加密json失败!", ex);
		}
		return desStr;
    }
    /**
     * 往用户中心解密json字符串
     * @param jsonStr
     * @return
     */
    public static String getUumsDesDeJsonStr(String jsonStr){
		String desStr ="";
		try{
			String deskey = AndroidUserConfigMgr.getAndroidDesKey();
			DESHelper dESHelper = new DESHelper(deskey);
			desStr = dESHelper.decrypt(jsonStr);
		}catch(Exception ex){
			desStr = jsonStr;
			logger.error("解密json失败!", ex);
		}
		return desStr;
    }
}
