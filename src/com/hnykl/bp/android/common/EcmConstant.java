package com.hnykl.bp.android.common;
/**
 * 
 * 内容管理常量类
 */
public class EcmConstant {
	/**
	 * 栏目码
	 *
	 */
	public static class CategoryCode{
		//升学顾问ABC 栏目code
		public final static String COLLEGE_COUNSELOR_ABC_CODE = "001001001001001";
		//动态升学指数 栏目 code
		public final static String DYNAMIC_TRANSITION_INDEX_CODE = "001001001001002";
	}
}
