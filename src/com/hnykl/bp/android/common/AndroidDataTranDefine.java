package com.hnykl.bp.android.common;

import java.io.IOException;
import java.io.InputStream;
import org.apache.log4j.Logger;
import java.util.Map;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

public class AndroidDataTranDefine {
    /**
     * logger
     */
    private static Logger logger = Logger.getLogger(AndroidDataTranDefine.class);
    
    private static Map dataMap = null;
    
    private static void loadConfigFile() throws Exception {
        if (dataMap == null) {
            InputStream is = AndroidDataTranDefine.class.getResourceAsStream(AndroidConstant.ANDROID_DATA_TRAN_DEFINE_CONFIG_FILE);
            try { 
                if (is == null) {
                	logger.error("未找到配置文件\"" + AndroidConstant.ANDROID_DATA_TRAN_DEFINE_CONFIG_FILE + "\"");
                    throw new IOException("读取action数据传输配置失败!");
                }
            } catch (IOException e) {
                throw new Exception(e);
            }
            XStream xstream = new XStream(new DomDriver());
            dataMap = (Map) xstream.fromXML(is);
        }
    }

 
    public static String getConfigValue(String name) throws Exception {

        if (name == null)
            return null;

        String sCode = "";
        if (dataMap == null) {
            loadConfigFile();
        }

        if (dataMap.get(name) != null) {
            sCode = (String) dataMap.get(name);
        }
        return sCode.trim();
    }
    
    public static  void  main(String []arg){
    	try{
    		loadConfigFile();
    		String uumsKey = getConfigValue("UUMS_KEY"); 
    		System.out.println(uumsKey);
    	}catch(Exception ex){
    		
    	}
    }
}
