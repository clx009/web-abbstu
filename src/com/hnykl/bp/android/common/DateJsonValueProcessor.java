package com.hnykl.bp.android.common;

import java.text.SimpleDateFormat;

import net.sf.json.JsonConfig;
import net.sf.json.processors.JsonValueProcessor;

public class DateJsonValueProcessor implements JsonValueProcessor {
	private static final String DEFAULT_FORMAT = "yyyy-MM-dd";

	private SimpleDateFormat sdf;

	public DateJsonValueProcessor() {
		super();
		this.sdf = new SimpleDateFormat(DEFAULT_FORMAT);
	}

	public DateJsonValueProcessor(String timestampPattern) {
		super();
		this.sdf = new SimpleDateFormat(timestampPattern);
	}

	@Override
	public Object processArrayValue(Object value, JsonConfig config) {
		return process(value);
	}

	@Override
	public Object processObjectValue(String key, Object value, JsonConfig config) {
		return process(value);
	}

	private Object process(Object value) {
		if (value != null)
			return sdf.format(value);
		return "";
	}

}
