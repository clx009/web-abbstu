package com.hnykl.bp.android.common;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.hnykl.bp.web.system.pojo.base.TSUser;

import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

public class AndroidRequest {
	@SuppressWarnings({ "rawtypes", "serial" })
	private static final HashMap<String, Class> REQ_HEAD_CLASS_MAP = new HashMap<String, Class>() {
		{
			put("User", TSUser.class);
		}
	};
	private static final String REQ_HEAD_PATH = "Data.Head";
	private static final String REQ_BODY_PATH = "Data.Request";

	Map<String, Object> requestMap = new HashMap<String, Object>();  
 
	public AndroidRequest(String requestData) {
		JsonConfig jc = new JsonConfig();
		jc.setClassMap(new HashMap<Object, Object>());
		jc.setRootClass(Map.class);
		jc.setArrayMode(JsonConfig.MODE_LIST);
		JSONObject jobj = JSONObject.fromObject(requestData, jc);
		decodeJSONObject(jobj, null);
	}

	public Map<?, ?> getRequestHeadMap() {
		// 解析头部对象
		String headJson = this.requestMap.get(REQ_HEAD_PATH).toString();
		JsonConfig hc = new JsonConfig();
		hc.setClassMap(REQ_HEAD_CLASS_MAP);
		hc.setRootClass(Map.class);
		hc.setArrayMode(JsonConfig.MODE_LIST);
		return JSONObject.fromObject(headJson);
	}
	
	public Map<?,?> getRequestBodyMap(Map<String, Class<?>> bodyClassMap){
		String bodyJson = this.requestMap.get(REQ_BODY_PATH).toString();
		JsonConfig bc = new JsonConfig();
		bc.setClassMap(bodyClassMap);
		bc.setRootClass(Map.class);
		bc.setArrayMode(JsonConfig.MODE_LIST);
		return JSONObject.fromObject(bodyJson);
	}

	private void decodeJSONObject(JSONObject json, String parentKey) {
		@SuppressWarnings("unchecked")
		Iterator<String> keys = json.keys();
		JSONObject sonJson = null;
		Object o;
		String key;
		while (keys.hasNext()) {
			key = keys.next();
			o = json.get(key);

			if (parentKey != null && !"".equals(parentKey)) {
				requestMap.put(parentKey + "." + key, o);
			} else {
				requestMap.put(key, o);
			}

			if (o instanceof JSONObject) {
				sonJson = (JSONObject) o;
				if (sonJson.keySet().size() > 0) {
					if (parentKey != null && !"".equals(parentKey)) {
						decodeJSONObject(sonJson, parentKey + "." + key);
					} else {
						decodeJSONObject(sonJson, key);
					}
				}
			}
		}
	}

	public Map<String, Object> getRequestMap() {
		return requestMap;
	}
}
