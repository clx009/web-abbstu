package com.hnykl.bp.android.common;

import java.net.URLEncoder;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Map;

import org.apache.log4j.Logger;

import com.hnykl.bp.android.des.DESHelper;
import com.hnykl.bp.base.tool.character.StringUtils;

import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.util.CycleDetectionStrategy;

public class AndroidResult {
	private static Logger logger = Logger.getLogger(AndroidResult.class);

	public static final String SUCCESS_CODE = "0";
	public static final String FAILURE_CODE = "-1";
	public static final String SUCCESS_MASSEGE = "操作成功";
	public static final String FAILURE_MASSEGE = "操作失败";

	private String replyCode; // 返回的状态代码
	private String replyMsg; // 返回的信息描述
	private Map<?, ?> resultData; // 返回的数据信息

	public AndroidResult(String replyCode, String replyMsg) {
		super();
		this.replyCode = replyCode;
		this.replyMsg = replyMsg;
	}

	public AndroidResult(String replyCode, String replyMsg, Map<?, ?> resultData) {
		super();
		this.replyCode = replyCode;
		this.replyMsg = replyMsg;
		this.resultData = resultData;
	}

	public String getReplyCode() {
		return replyCode;
	}

	public void setReplyCode(String replyCode) {
		this.replyCode = replyCode;
	}

	public String getReplyMsg() {
		return replyMsg;
	}

	public void setReplyMsg(String replyMsg) {
		this.replyMsg = replyMsg;
	}

	public Map<?, ?> getResultData() {
		return resultData;
	}

	public void setResultData(Map<?, ?> resultData) {
		this.resultData = resultData;
	}

	public static AndroidResult defaultSuccess() {
		return new AndroidResult(AndroidResult.SUCCESS_CODE,
				AndroidResult.SUCCESS_MASSEGE);
	}

	public static AndroidResult defaultSuccess(String message) {
		return new AndroidResult(AndroidResult.SUCCESS_CODE,
				AndroidResult.SUCCESS_MASSEGE + " : " + message);
	}

	public static AndroidResult defaultFailure() {
		return new AndroidResult(AndroidResult.FAILURE_CODE,
				AndroidResult.FAILURE_MASSEGE);
	}

	public static AndroidResult defaultFailure(String message) {
		return new AndroidResult(AndroidResult.FAILURE_CODE,
				AndroidResult.FAILURE_MASSEGE + " : " + message);
	}

	public String toAndroidResultString() {
		TimestampJsonValueProcessor timestampJsonValueProcessor = new TimestampJsonValueProcessor();
		DateJsonValueProcessor dateJsonValueProcessor = new DateJsonValueProcessor();
		JsonConfig config = new JsonConfig();
		config.registerJsonValueProcessor(Timestamp.class,
				timestampJsonValueProcessor);
		config.registerJsonValueProcessor(Date.class, dateJsonValueProcessor);
		config.registerJsonValueProcessor(java.util.Date.class, timestampJsonValueProcessor); 
		config.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);
		String retData = JSONObject.fromObject(this, config).toString();
		logger.info("======>return json:" + retData);
		String retStr = "";
		if (StringUtils.isEmpty(retData))
			return retStr;
		try {
			retData = URLEncoder.encode(retData, "UTF-8");
			// 加密
			if ("1".equals(AndroidDataTranDefine.getConfigValue("IS_ENCRYPT"))) {
				String sKey = AndroidDataTranDefine
						.getConfigValue("ANDROID_KEY");
				DESHelper desHelper = new DESHelper(sKey);
				retStr = desHelper.encrypt(retData);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		retStr = StringUtils.isNotEmpty(retStr) ? retStr : retData;
		retStr = retStr.replaceAll("\r", "");
		retStr = retStr.replaceAll("\n", "");
		return retStr;
	}

}
