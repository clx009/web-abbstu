package com.hnykl.bp.android.common;

public class AndroidConstant {
	/**
	 * action的json传输定义配置    
	 */
	public final static String ANDROID_DATA_TRAN_DEFINE_CONFIG_FILE = "/datatrandefine.xml";
	/**
	 * 匿名值
	 */
	public final static String ANDROID_TOKEN_ANONYMITY_LOGINNAME = "anonymity";
	
	/**
	 * 匿名密码
	 */
	public final static String ANDROID_TOKEN_ANONYMITY_PASSWORD = "123456";
	
	/**
	 * 失效
	 */
	public final static String ANDROID_TOKEN_LOGININVALID_LOGINNAME = "loginInvalid";
    /**
	* 失效密码
	*/
	public final static String ANDROID_TOKEN_LOGINVALID_PASSWORD = "111111";
	

	/**
	 * 返回成功 
	 */
	public final static String ANDROID_RET_JSON_SUCCESS_CODE = "0";
	/**
	 * 返回失败
	 */
	public final static String ANDROID_RET_JSON_FAIL_CODE = "-1";
	/**
	 * 返回成功文字信息
	 */
	public final static String ANDROID_RET_JSON_SUCCESS_MSG = "操作成功!";
	/**
	 * 返回失败文字信息
	 */
	public final static String ANDROID_RET_JSON_FAIL_MSG = "操作失败!";
	/**
	 * android端固定死appId
	 */
	public final static String ANDROID_CLIENT_APPID = "KKpaiwr1bZyMORYj8ztzE1Y+R/b1z2+dtnrDOovps6Y=";
}
