package com.hnykl.bp.android.common;

import java.net.URLDecoder;
import java.net.URLEncoder;

import org.apache.log4j.Logger;

import com.hnykl.bp.android.des.DESHelper;
import com.hnykl.bp.base.tool.character.CharsetUtils;
import com.hnykl.bp.web.system.common.AndroidUserConfigMgr;

/**
 * 加密的工具类
 */
public class DesUtils {
	private static Logger logger = Logger.getLogger(DesUtils.class);
	
	 /**
     * 加密json字符串
     * @param jsonStr
     * @return
     */
    public static String enStr(String jsonStr,boolean isHttpPost){
		String desStr ="";
		try{
			String deskey = AndroidUserConfigMgr.getAndroidDesKey();
			DESHelper dESHelper = new DESHelper(deskey);
			desStr = dESHelper.encrypt(jsonStr);
			logger.info("enStr after:"+desStr);
			if(isHttpPost){ //http传输要encode
				desStr = URLEncoder.encode(desStr, "UTF-8");
			}
			logger.info("URLEncoder.encode after:"+desStr);
		}catch(Exception ex){
			desStr = jsonStr;
			logger.error("加密json失败!", ex);
		}
		return desStr;
    }
    /**
     * 解密json字符串
     * @param jsonStr
     * @return
     */
    public static String desStr(String jsonStr,boolean isHttpPost){
		String desStr ="";
		try{
			if(CharsetUtils.isUrlEncode(jsonStr) && isHttpPost){
				jsonStr = URLDecoder.decode(jsonStr);
			}
			String deskey = AndroidUserConfigMgr.getAndroidDesKey();
			DESHelper dESHelper = new DESHelper(deskey);
			desStr = dESHelper.decrypt(jsonStr);
		}catch(Exception ex){
			desStr = jsonStr;
			logger.error("解密json失败!", ex);
		}
		return desStr;
    }
	
}
