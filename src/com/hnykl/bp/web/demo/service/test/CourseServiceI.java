package com.hnykl.bp.web.demo.service.test;

import com.hnykl.bp.web.demo.entity.test.CourseEntity;

import com.hnykl.bp.base.core.common.service.CommonService;

public interface CourseServiceI extends CommonService{

	/**
	 * 保存课程
	 *@Author 
	 *@date   2013-11-10
	 *@param  course
	 */
	void saveCourse(CourseEntity course);
	/**
	 * 更新课程
	 *@Author 
	 *@date   2013-11-10
	 *@param  course
	 */
	void updateCourse(CourseEntity course);

}
