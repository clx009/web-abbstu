package com.hnykl.bp.web.demo.service.test;

import com.hnykl.bp.web.demo.entity.test.BpJdbcEntity;
import net.sf.json.JSONObject;

import com.hnykl.bp.base.core.common.model.json.DataGrid;
import com.hnykl.bp.base.core.common.service.CommonService;

public interface BpJdbcServiceI extends CommonService{
	public void getDatagrid1(BpJdbcEntity pageObj, DataGrid dataGrid);
	public void getDatagrid2(BpJdbcEntity pageObj, DataGrid dataGrid);
	public JSONObject getDatagrid3(BpJdbcEntity pageObj, DataGrid dataGrid);
	public void listAllByJdbc(DataGrid dataGrid);
}
