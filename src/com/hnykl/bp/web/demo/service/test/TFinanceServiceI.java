package com.hnykl.bp.web.demo.service.test;

import com.hnykl.bp.web.demo.entity.test.TFinanceEntity;
import com.hnykl.bp.web.demo.entity.test.TFinanceFilesEntity;

import com.hnykl.bp.base.core.common.service.CommonService;

public interface TFinanceServiceI extends CommonService{

	void deleteFile(TFinanceFilesEntity file);

	void deleteFinance(TFinanceEntity finance);

}
