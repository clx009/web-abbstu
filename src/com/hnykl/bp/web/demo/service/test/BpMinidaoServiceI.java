package com.hnykl.bp.web.demo.service.test;

import java.util.List;

import com.hnykl.bp.web.demo.entity.test.BpMinidaoEntity;

/**
 * Minidao例子
 * @author fancq
 *
 */
public interface BpMinidaoServiceI {
	public List<BpMinidaoEntity> listAll(BpMinidaoEntity bpMinidao, int page, int rows);
	
	public BpMinidaoEntity getEntity(Class clazz, String id);
	
	public void insert(BpMinidaoEntity bpMinidao);
	
	public void update(BpMinidaoEntity bpMinidao);
	
	public void delete(BpMinidaoEntity bpMinidao);
	
	public void deleteAllEntitie(List<BpMinidaoEntity> entities);
	
	public Integer getCount();
	
	public Integer getSumSalary();
}
