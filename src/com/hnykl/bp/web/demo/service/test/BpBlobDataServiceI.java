package com.hnykl.bp.web.demo.service.test;

import com.hnykl.bp.base.core.common.service.CommonService;
import org.springframework.web.multipart.MultipartFile;

public interface BpBlobDataServiceI extends CommonService{
	public void saveObj(String documentTitle, MultipartFile file);

}
