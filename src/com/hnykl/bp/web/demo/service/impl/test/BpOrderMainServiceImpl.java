package com.hnykl.bp.web.demo.service.impl.test;

import java.util.List;

import com.hnykl.bp.web.demo.entity.test.BpOrderCustomEntity;
import com.hnykl.bp.web.demo.entity.test.BpOrderMainEntity;
import com.hnykl.bp.web.demo.entity.test.BpOrderProductEntity;
import com.hnykl.bp.web.demo.service.test.BpOrderMainServiceI;

import com.hnykl.bp.base.core.common.service.impl.CommonServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("bpOrderMainService")
@Transactional
public class BpOrderMainServiceImpl extends CommonServiceImpl implements BpOrderMainServiceI {

	
	public void addMain(BpOrderMainEntity bpOrderMain,
			List<BpOrderProductEntity> bpOrderProducList,
			List<BpOrderCustomEntity> bpOrderCustomList){
		//保存订单主信息
		this.save(bpOrderMain);
		//保存订单产品明细
		for(BpOrderProductEntity product:bpOrderProducList){
			//外键设置
			product.setGoOrderCode(bpOrderMain.getGoOrderCode());
			this.save(product);
		}
		//保存订单客户明细
		for(BpOrderCustomEntity custom:bpOrderCustomList){
			//外键设置
			custom.setGoOrderCode(bpOrderMain.getGoOrderCode());
			this.save(custom);
		}
	}

	
	public void updateMain(BpOrderMainEntity bpOrderMain,
			List<BpOrderProductEntity> bpOrderProducList,
			List<BpOrderCustomEntity> bpOrderCustomList,
			boolean bpOrderCustomShow) {
		//保存订单主信息
		this.saveOrUpdate(bpOrderMain);
		//删除订单产品明细
		this.commonDao.deleteAllEntitie(this.findByProperty(BpOrderProductEntity.class, "goOrderCode", bpOrderMain.getGoOrderCode()));
		//保存订单产品明细
		for(BpOrderProductEntity product:bpOrderProducList){
			//外键设置
			product.setGoOrderCode(bpOrderMain.getGoOrderCode());
			this.save(product);
		}
		if(bpOrderCustomShow){
			//删除订单客户明细
			this.commonDao.deleteAllEntitie(this.findByProperty(BpOrderCustomEntity.class, "goOrderCode", bpOrderMain.getGoOrderCode()));
			//保存订单客户明细
			for(BpOrderCustomEntity custom:bpOrderCustomList){
				//外键设置
				custom.setGoOrderCode(bpOrderMain.getGoOrderCode());
				this.save(custom);
			}
		}
	}

	
	public void delMain(BpOrderMainEntity bpOrderMain) {
		//删除主表信息
		this.delete(bpOrderMain);
		//删除订单产品明细
		this.deleteAllEntitie(this.findByProperty(BpOrderProductEntity.class, "goOrderCode", bpOrderMain.getGoOrderCode()));
		//删除订单客户明细
		this.commonDao.deleteAllEntitie(this.findByProperty(BpOrderCustomEntity.class, "goOrderCode", bpOrderMain.getGoOrderCode()));
	}
}