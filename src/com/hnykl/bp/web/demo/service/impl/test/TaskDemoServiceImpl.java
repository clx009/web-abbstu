package com.hnykl.bp.web.demo.service.impl.test;

import java.util.Date;

import org.springframework.stereotype.Service;

import com.hnykl.bp.web.demo.service.test.TaskDemoServiceI;
@Service("taskDemoService")
public class TaskDemoServiceImpl implements TaskDemoServiceI {

	
	public void work() {
		com.hnykl.bp.base.core.util.LogUtil.info(new Date().getTime());
		com.hnykl.bp.base.core.util.LogUtil.info("----------任务测试-------");
	}

}
