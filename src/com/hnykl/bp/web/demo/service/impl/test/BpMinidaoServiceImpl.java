package com.hnykl.bp.web.demo.service.impl.test;

import java.util.List;
import com.hnykl.bp.web.demo.dao.test.BpMinidaoDao;
import com.hnykl.bp.web.demo.entity.test.BpMinidaoEntity;
import com.hnykl.bp.web.demo.service.test.BpMinidaoServiceI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Minidao例子
 * @author fancq
 *
 */
@Service("bpMinidaoService")
@Transactional
public class BpMinidaoServiceImpl implements BpMinidaoServiceI {
	@Autowired
	private BpMinidaoDao bpMinidaoDao;
	
	public List<BpMinidaoEntity> listAll(BpMinidaoEntity bpMinidao, int page, int rows) {
		List<BpMinidaoEntity> entities = bpMinidaoDao.getAllEntities2(bpMinidao, page, rows);
		return entities;
	}
	
	public BpMinidaoEntity getEntity(Class clazz, String id) {
		BpMinidaoEntity bpMinidao = (BpMinidaoEntity)bpMinidaoDao.getByIdHiber(clazz, id);
		return bpMinidao;
	}
	
	public void insert(BpMinidaoEntity bpMinidao) {
		bpMinidaoDao.saveByHiber(bpMinidao);
	}
	
	public void update(BpMinidaoEntity bpMinidao) {
		bpMinidaoDao.updateByHiber(bpMinidao);
	}
	
	public void delete(BpMinidaoEntity bpMinidao) {
		bpMinidaoDao.deleteByHiber(bpMinidao);
	}
	
	public void deleteAllEntitie(List<BpMinidaoEntity> entities) {
		for (BpMinidaoEntity entity : entities) {
			bpMinidaoDao.deleteByHiber(entity);
		}
	}
	
	public Integer getCount() {
		return bpMinidaoDao.getCount();
	}
	
	public Integer getSumSalary() {
		return bpMinidaoDao.getSumSalary();
	}
}