package com.hnykl.bp.web.demo.service.impl.test;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hnykl.bp.web.demo.service.test.BpDemoCkfinderServiceI;
import com.hnykl.bp.base.core.common.service.impl.CommonServiceImpl;

@Service("bpDemoCkfinderService")
@Transactional
public class BpDemoCkfinderServiceImpl extends CommonServiceImpl implements
		BpDemoCkfinderServiceI {

}