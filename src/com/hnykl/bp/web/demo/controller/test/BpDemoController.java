package com.hnykl.bp.web.demo.controller.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hnykl.bp.web.demo.entity.test.CKEditorEntity;
import com.hnykl.bp.web.demo.entity.test.BpDemo;
import com.hnykl.bp.web.demo.service.test.BpDemoServiceI;
import com.hnykl.bp.web.system.pojo.base.TSDepart;
import com.hnykl.bp.web.system.service.SystemService;

import org.apache.log4j.Logger;
import com.hnykl.bp.base.core.annotation.config.AutoMenu;
import com.hnykl.bp.base.core.annotation.config.AutoMenuOperation;
import com.hnykl.bp.base.core.common.controller.BaseController;
import com.hnykl.bp.base.core.common.hibernate.qbc.CriteriaQuery;
import com.hnykl.bp.base.core.common.model.json.AjaxJson;
import com.hnykl.bp.base.core.common.model.json.DataGrid;
import com.hnykl.bp.base.core.constant.Globals;
import com.hnykl.bp.base.core.util.MyBeanUtils;
import com.hnykl.bp.base.core.util.StringUtil;
import com.hnykl.bp.tag.core.easyui.TagUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**   
 * @Title: Controller
 * @Description: 单表模型（DEMO）
 * @author 
 * @date 2013-01-23 17:12:40
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/bpDemoController")
@AutoMenu(name = "常用Demo", url = "bpDemoController.do?bpDemo")
public class BpDemoController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(BpDemoController.class);

	@Autowired
	private BpDemoServiceI bpDemoService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	/**
	 * popup 例子
	 * @param request
	 * @return
	 */
	@RequestMapping(params = "popup")
	public ModelAndView popup(HttpServletRequest request) {
		return new ModelAndView("bp/demo/bpDemo/popup");
	}
	/**
	 * popup 例子
	 * @param request
	 * @return
	 */
	@RequestMapping(params = "selectUserList")
	public ModelAndView selectUserList(HttpServletRequest request) {
		String departsReplace = "";
		List<TSDepart> departList = systemService.getList(TSDepart.class);
		for (TSDepart depart : departList) {
			if (departsReplace.length() > 0) {
				departsReplace += ",";
			}
			departsReplace += depart.getDepartname() + "_" + depart.getId();
		}
		request.setAttribute("departsReplace", departsReplace);
		return new ModelAndView("bp/demo/bpDemo/selectUserList");
	}
	
	/**
	 * ckeditor 例子
	 * 
	 * @return
	 */
	@RequestMapping(params = "ckeditor")
	public ModelAndView ckeditor(HttpServletRequest request) {
//		CKEditorEntity t = bpDemoService.get(CKEditorEntity.class, "1");
		CKEditorEntity t = bpDemoService.loadAll(CKEditorEntity.class).get(0);
		request.setAttribute("cKEditorEntity", t);
		if(t.getContents() == null ){
			request.setAttribute("contents", "");
		}else {
			request.setAttribute("contents", new String (t.getContents()));
		}
		return new ModelAndView("bp/demo/bpDemo/ckeditor");
	}
	/**
	 * ckeditor saveCkeditor
	 * 
	 * @return
	 */
	@RequestMapping(params = "saveCkeditor")
	@ResponseBody
	public AjaxJson saveCkeditor(HttpServletRequest request,CKEditorEntity cKEditor , String contents) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(cKEditor.getId())) {
			CKEditorEntity t =bpDemoService.get(CKEditorEntity.class, cKEditor.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(cKEditor, t);
				t.setContents(contents.getBytes());
				bpDemoService.saveOrUpdate(t);
				j.setMsg("更新成功");
			} catch (Exception e) {
				e.printStackTrace();
				j.setMsg("更新失败");
			}
		} else {
			cKEditor.setContents(contents.getBytes());
			bpDemoService.save(cKEditor);
		}
		return j;
	}

	/**
	 * BpDemo 打印预览跳转
	 * @param bpDemo
	 * @param req
	 * @return
	 */
	@RequestMapping(params = "print")
	public ModelAndView print(BpDemo bpDemo, HttpServletRequest req) {
		// 获取部门信息
		List<TSDepart> departList = systemService.getList(TSDepart.class);
		req.setAttribute("departList", departList);

		if (StringUtil.isNotEmpty(bpDemo.getId())) {
			bpDemo = bpDemoService.getEntity(BpDemo.class, bpDemo
					.getId());
			req.setAttribute("jgDemo", bpDemo);
			if ("0".equals(bpDemo.getSex()))
				req.setAttribute("sex", "男");
			if ("1".equals(bpDemo.getSex()))
				req.setAttribute("sex", "女");
		}
		return new ModelAndView("bp/demo/bpDemo/bpDemoPrint");
	}

	/**
	 * BpDemo例子列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "bpDemo")
	public ModelAndView bpDemo(HttpServletRequest request) {
		return new ModelAndView("bp/demo/bpDemo/bpDemoList");
	}

	/**
	 * easyuiAJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(BpDemo bpDemo,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(BpDemo.class, dataGrid);
		//查询条件组装器
		com.hnykl.bp.base.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, bpDemo,request.getParameterMap());
		this.bpDemoService.getDataGridReturn(cq, true);
		String total_salary = String.valueOf(bpDemoService.findOneForJdbc("select sum(salary) as ssum from bp_demo").get("ssum"));
		/*
		 * 说明：格式为 字段名:值(可选，不写该值时为分页数据的合计) 多个合计 以 , 分割
		 */
		dataGrid.setFooter("salary:"+(total_salary.equalsIgnoreCase("null")?"0.0":total_salary)+",age,email:合计");
		TagUtil.datagrid(response, dataGrid);
	}

	
	/**
	 * 权限列表
	 */
	@RequestMapping(params = "combox")
	@ResponseBody
	public List<BpDemo> combox(HttpServletRequest request, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(BpDemo.class);
		List<BpDemo> ls = this.bpDemoService.getListByCriteriaQuery(cq, false);
		return ls;
	}
	/**
	 * 删除BpDemo例子
	 * 
	 * @return
	 */
	@RequestMapping(params = "del")
	@ResponseBody
	public AjaxJson del(BpDemo bpDemo, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		bpDemo = systemService.getEntity(BpDemo.class, bpDemo.getId());
		message = "BpDemo例子: " + bpDemo.getUserName() + "被删除 成功";
		bpDemoService.delete(bpDemo);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加BpDemo例子
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "save")
	@ResponseBody
	@AutoMenuOperation(name="添加",code = "add")
	public AjaxJson save(BpDemo bpDemo, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(bpDemo.getId())) {
			message = "BpDemo例子: " + bpDemo.getUserName() + "被更新成功";
			BpDemo t =bpDemoService.get(BpDemo.class, bpDemo.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(bpDemo, t);
				bpDemoService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			message = "BpDemo例子: " + bpDemo.getUserName() + "被添加成功";
			bpDemo.setStatus("0");
			bpDemoService.save(bpDemo);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		
		return j;
	}
	
	
	/**
	 * 审核报错
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "saveAuthor")
	@ResponseBody
	public AjaxJson saveAuthor(BpDemo bpDemo, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(bpDemo.getId())) {
			message = "测试-用户申请成功";
			BpDemo t =bpDemoService.get(BpDemo.class, bpDemo.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(bpDemo, t);
				t.setStatus("1");
				bpDemoService.saveOrUpdate(t);
				j.setMsg(message);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return j;
	}

	/**
	 * BpDemo例子列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "addorupdate")
	public ModelAndView addorupdate(BpDemo bpDemo, HttpServletRequest req) {
		//获取部门信息
		List<TSDepart> departList = systemService.getList(TSDepart.class);
		req.setAttribute("departList", departList);
		
		Map sexMap = new HashMap();
		sexMap.put(0, "男");
		sexMap.put(1, "女");
		req.setAttribute("sexMap", sexMap);
		
		if (StringUtil.isNotEmpty(bpDemo.getId())) {
			bpDemo = bpDemoService.getEntity(BpDemo.class, bpDemo.getId());
			req.setAttribute("jgDemo", bpDemo);
		}
		return new ModelAndView("bp/demo/bpDemo/bpDemo");
	}
	
	
	/**
	 * 设置签名跳转页面
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(params = "doCheck")
	public ModelAndView doCheck(HttpServletRequest request) {
		String id = request.getParameter("id");
		request.setAttribute("id", id);
		if (StringUtil.isNotEmpty(id)) {
			BpDemo bpDemo = bpDemoService.getEntity(BpDemo.class,id);
			request.setAttribute("bpDemo", bpDemo);
		}
		return new ModelAndView("bp/demo/bpDemo/bpDemo-check");
	}

	/**
	 * 全选删除Demo实例管理
	 * 
	 * @return
	 * @author 
	 * @date 2013-07-13 14:53:00
	 */
	@RequestMapping(params = "doDeleteALLSelect")
	@ResponseBody
	public AjaxJson doDeleteALLSelect(BpDemo bpDemo, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		String ids = request.getParameter("ids");
		String[] entitys = ids.split(",");
	    List<BpDemo> list = new ArrayList<BpDemo>();
		for(int i=0;i<entitys.length;i++){
			bpDemo = systemService.getEntity(BpDemo.class, entitys[i]);
            list.add(bpDemo);			
		}
		message = "删除成功";
		bpDemoService.deleteAllEntitie(list);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		j.setMsg(message);
		return j;
	}
}
