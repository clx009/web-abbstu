package com.hnykl.bp.web.demo.controller.test;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hnykl.bp.web.demo.entity.test.BpJdbcEntity;
import com.hnykl.bp.web.demo.service.test.BpJdbcServiceI;
import com.hnykl.bp.web.system.service.SystemService;
import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import com.hnykl.bp.base.core.common.controller.BaseController;
import com.hnykl.bp.base.core.common.model.json.AjaxJson;
import com.hnykl.bp.base.core.common.model.json.DataGrid;
import com.hnykl.bp.base.core.constant.Globals;
import com.hnykl.bp.base.core.util.MyBeanUtils;
import com.hnykl.bp.base.core.util.StringUtil;
import com.hnykl.bp.tag.core.easyui.TagUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**   
 * @Title: Controller
 * @Description: 通过JDBC访问数据库
 * @author Quainty
 * @date 2013-05-20 13:18:38
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/bpJdbcController")
public class BpJdbcController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(BpJdbcController.class);

	@Autowired
	private BpJdbcServiceI bpJdbcService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 通过JDBC访问数据库列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "bpJdbc")
	public ModelAndView bpJdbc(HttpServletRequest request) {
		return new ModelAndView("bp/demo/test/bpJdbcList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(BpJdbcEntity bpJdbc,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		// 方式1, 用底层自带的方式往对象中设值  -------------------
		/*
		this.bpJdbcService.getDatagrid1(bpJdbc, dataGrid);
		TagUtil.datagrid(response, dataGrid);
		// end of 方式1 ========================================= */ 
		
		// 方式2, 取值自己处理(代码量多一些，但执行效率应该会稍高一些)  -------------------------------
		/*
		this.bpJdbcService.getDatagrid2(bpJdbc, dataGrid);
		TagUtil.datagrid(response, dataGrid);
		// end of 方式2 ========================================= */ 
		
		// 方式3, 取值进一步自己处理(直接转换成easyUI的datagrid需要的东西，执行效率最高，最自由)  -------------------------------
		//*
		JSONObject jObject = this.bpJdbcService.getDatagrid3(bpJdbc, dataGrid);
		responseDatagrid(response, jObject);
		// end of 方式3 ========================================= */
	}

	/**
	 * 删除通过JDBC访问数据库
	 * 
	 * @return
	 */
	@RequestMapping(params = "del")
	@ResponseBody
	public AjaxJson del(BpJdbcEntity bpJdbc, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		
		String sql = "delete from bp_demo where id='" + bpJdbc.getId() + "'";
		bpJdbcService.executeSql(sql);

		message = "删除成功";
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加通过JDBC访问数据库
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "save")
	@ResponseBody
	public AjaxJson save(BpJdbcEntity bpJdbc, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(bpJdbc.getId())) {
			message = "更新成功";
			BpJdbcEntity t = bpJdbcService.get(BpJdbcEntity.class, bpJdbc.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(bpJdbc, t);
				bpJdbcService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			message = "添加成功";
			bpJdbcService.save(bpJdbc);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		
		return j;
	}

	/**
	 * 通过JDBC访问数据库列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "addorupdate")
	public ModelAndView addorupdate(BpJdbcEntity bpJdbc, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(bpJdbc.getId())) {
			bpJdbc = bpJdbcService.getEntity(BpJdbcEntity.class, bpJdbc.getId());
			req.setAttribute("bpJdbcPage", bpJdbc);
		}
		return new ModelAndView("bp/demo/test/bpJdbc");
	}
	
	
	// -----------------------------------------------------------------------------------
	// 以下各函数可以提成共用部件 (Add by Quainty)
	// -----------------------------------------------------------------------------------
	public void responseDatagrid(HttpServletResponse response, JSONObject jObject) {
		response.setContentType("application/json");
		response.setHeader("Cache-Control", "no-store");
		try {
			PrintWriter pw=response.getWriter();
			pw.write(jObject.toString());
			pw.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}	
	@RequestMapping(params = "dictParameter")
	public String dictParameter(){
		return "bp/demo/base/jdbc/jdbc-list";
	}
	
	/**
	 * JDBC DEMO 显示列表
	 * 
	 * @return
	 */
	@RequestMapping(params = "listAllDictParaByJdbc")
	public void listAllDictParaByJdbc(HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		
		this.bpJdbcService.listAllByJdbc(dataGrid);
		TagUtil.datagrid(response, dataGrid);
	}
}
