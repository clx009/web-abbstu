package com.hnykl.bp.web.demo.controller.test;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hnykl.bp.web.demo.entity.test.BpOrderCustomEntity;
import com.hnykl.bp.web.demo.entity.test.BpOrderMainEntity;
import com.hnykl.bp.web.demo.entity.test.BpOrderProductEntity;
import com.hnykl.bp.web.demo.page.BpOrderMainPage;
import com.hnykl.bp.web.demo.service.test.BpOrderMainServiceI;
import com.hnykl.bp.web.system.service.SystemService;

import org.apache.log4j.Logger;
import com.hnykl.bp.base.core.common.controller.BaseController;
import com.hnykl.bp.base.core.common.hibernate.qbc.CriteriaQuery;
import com.hnykl.bp.base.core.common.model.json.AjaxJson;
import com.hnykl.bp.base.core.common.model.json.DataGrid;
import com.hnykl.bp.base.core.constant.Globals;
import com.hnykl.bp.base.core.util.StringUtil;
import com.hnykl.bp.tag.core.easyui.TagUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**   
 * @Title: Controller
 * @Description: 订单信息
 * @author 
 * @date 2013-03-19 22:01:34
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/bpOrderMainController")
public class BpOrderMainController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(BpOrderMainController.class);

	@Autowired
	private BpOrderMainServiceI bpOrderMainService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 订单信息列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "bpOrderMain")
	public ModelAndView bpOrderMain(HttpServletRequest request) {
		return new ModelAndView("bp/demo/test/bpOrderMainList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(BpOrderMainEntity.class, dataGrid);
		this.bpOrderMainService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除订单信息
	 * 
	 * @return
	 */
	@RequestMapping(params = "del")
	@ResponseBody
	public AjaxJson del(BpOrderMainEntity bpOrderMain, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		bpOrderMain = systemService.getEntity(BpOrderMainEntity.class, bpOrderMain.getId());
		message = "删除成功";
		bpOrderMainService.delMain(bpOrderMain);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加订单及明细信息
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "save")
	@ResponseBody
	public AjaxJson save(BpOrderMainEntity bpOrderMain ,BpOrderMainPage bpOrderMainPage,	
			HttpServletRequest request) {
		List<BpOrderProductEntity> bpOrderProducList =  bpOrderMainPage.getBpOrderProductList();
		List<BpOrderCustomEntity>  bpOrderCustomList = bpOrderMainPage.getBpOrderCustomList();
		Boolean bpOrderCustomShow = "true".equals(request.getParameter("bpOrderCustomShow"));
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(bpOrderMain.getId())) {
			message = "更新成功";
			bpOrderMainService.updateMain(bpOrderMain, bpOrderProducList, bpOrderCustomList,bpOrderCustomShow);
			systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
		} else {
			message = "添加成功";
			bpOrderMainService.addMain(bpOrderMain, bpOrderProducList, bpOrderCustomList);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}
	/**
	 * 订单信息列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "addorupdate")
	public ModelAndView addorupdate(BpOrderMainEntity bpOrderMain, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(bpOrderMain.getId())) {
			bpOrderMain = bpOrderMainService.getEntity(BpOrderMainEntity.class, bpOrderMain.getId());
			req.setAttribute("bpOrderMainPage", bpOrderMain);
		}
		return new ModelAndView("bp/demo/test/bpOrderMain");
	}
	/**
	 * 加载产品列表页面
	 * 
	 * @return
	 */
	@RequestMapping(params = "bpOrderProductList")
	public ModelAndView bpOrderProductList(BpOrderMainEntity bpOrderMain, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(bpOrderMain.getGoOrderCode())) {
			List<BpOrderProductEntity> bpOrderProductEntityList =  bpOrderMainService.findByProperty(BpOrderProductEntity.class, "goOrderCode", bpOrderMain.getGoOrderCode());
			req.setAttribute("bpOrderProductList", bpOrderProductEntityList);
		}
		return new ModelAndView("bp/demo/test/bpOrderProductList");
	}
	
	/**
	 * 加载客户列表页面
	 * 
	 * @return
	 */
	@RequestMapping(params = "bpOrderCustomList")
	public ModelAndView bpOrderCustomList(BpOrderMainEntity bpOrderMain, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(bpOrderMain.getGoOrderCode())) {
			List<BpOrderCustomEntity> bpBpOrderCustomEntityList =  bpOrderMainService.findByProperty(BpOrderCustomEntity.class, "goOrderCode", bpOrderMain.getGoOrderCode());
			req.setAttribute("bpOrderCustomList", bpBpOrderCustomEntityList);
		}
		return new ModelAndView("bp/demo/test/bpOrderCustomList");
	}
}
