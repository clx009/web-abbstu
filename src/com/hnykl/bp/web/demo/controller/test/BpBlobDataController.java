package com.hnykl.bp.web.demo.controller.test;
import java.io.BufferedOutputStream;
import java.io.InputStream;
import java.sql.Blob;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hnykl.bp.web.demo.entity.test.BpBlobDataEntity;
import com.hnykl.bp.web.demo.service.test.BpBlobDataServiceI;
import com.hnykl.bp.web.system.service.SystemService;

import org.apache.log4j.Logger;
import com.hnykl.bp.base.core.common.controller.BaseController;
import com.hnykl.bp.base.core.common.hibernate.qbc.CriteriaQuery;
import com.hnykl.bp.base.core.common.model.json.AjaxJson;
import com.hnykl.bp.base.core.common.model.json.DataGrid;
import com.hnykl.bp.base.core.constant.Globals;
import com.hnykl.bp.base.core.util.ExceptionUtil;
import com.hnykl.bp.base.core.util.MyBeanUtils;
import com.hnykl.bp.base.core.util.StringUtil;
import com.hnykl.bp.tag.core.easyui.TagUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

/**   
 * @Title: Controller
 * @Description: Blob型数据操作例子
 * @author Quainty
 * @date 2013-06-07 14:46:08
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/bpBlobDataController")
public class BpBlobDataController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(BpBlobDataController.class);

	@Autowired
	private BpBlobDataServiceI bpBlobDataService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * Blob型数据操作例子列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "bpBlobData")
	public ModelAndView bpBlobData(HttpServletRequest request) {
		return new ModelAndView("bp/demo/test/bpBlobDataList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(BpBlobDataEntity bpBlobData,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(BpBlobDataEntity.class, dataGrid);
		//查询条件组装器
		com.hnykl.bp.base.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, bpBlobData);
		this.bpBlobDataService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除Blob型数据操作例子
	 * 
	 * @return
	 */
	@RequestMapping(params = "del")
	@ResponseBody
	public AjaxJson del(BpBlobDataEntity bpBlobData, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		bpBlobData = systemService.getEntity(BpBlobDataEntity.class, bpBlobData.getId());
		message = "删除成功";
		bpBlobDataService.delete(bpBlobData);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}
	
	@RequestMapping(params = "download")
	public void exportXls(HttpServletRequest request, String fileId, HttpServletResponse response) {
		// 从数据库取得数据
		BpBlobDataEntity obj = systemService.getEntity(BpBlobDataEntity.class, fileId);
	    try {      
	    	Blob attachment = obj.getAttachmentcontent();
			response.setContentType("application/x-msdownload;");
			response.setHeader("Content-disposition", "attachment; filename="
					+ new String((obj.getAttachmenttitle()+"."+obj.getExtend()).getBytes("GBK"), "ISO8859-1"));
	        //从数据库中读取出来    , 输出给下载用
	        InputStream bis = attachment.getBinaryStream();      
	        BufferedOutputStream bos = new BufferedOutputStream(response.getOutputStream());
			byte[] buff = new byte[2048];
			int bytesRead;
			long lTotalLen = 0;
			while (-1 != (bytesRead = bis.read(buff, 0, buff.length))) {
				bos.write(buff, 0, bytesRead);
				lTotalLen += bytesRead;
			}
			response.setHeader("Content-Length", String.valueOf(lTotalLen));
			bos.flush();
			bos.close();
	    } catch (Exception  e){      
	        e.printStackTrace();      
	    }                 
	}
	@RequestMapping(params = "upload")
	@ResponseBody
    public AjaxJson upload(HttpServletRequest request, String documentTitle, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
		for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
			MultipartFile file = entity.getValue();// 获取上传文件对象
			try {
				bpBlobDataService.saveObj(documentTitle, file);
				j.setMsg("文件导入成功！");
			} catch (Exception e) {
				j.setMsg("文件导入失败！");
				logger.error(ExceptionUtil.getExceptionMessage(e));
			}
			//break; // 不支持多个文件导入？
		}

		return j;
    }


	/**
	 * 添加Blob型数据操作例子
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "save")
	@ResponseBody
	public AjaxJson save(BpBlobDataEntity bpBlobData, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(bpBlobData.getId())) {
			message = "更新成功";
			BpBlobDataEntity t = bpBlobDataService.get(BpBlobDataEntity.class, bpBlobData.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(bpBlobData, t);
				bpBlobDataService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			message = "添加成功";
			bpBlobDataService.save(bpBlobData);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		
		return j;
	}

	/**
	 * Blob型数据操作例子列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "addorupdate")
	public ModelAndView addorupdate(BpBlobDataEntity bpBlobData, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(bpBlobData.getId())) {
			bpBlobData = bpBlobDataService.getEntity(BpBlobDataEntity.class, bpBlobData.getId());
			req.setAttribute("bpBlobDataPage", bpBlobData);
		}
		return new ModelAndView("bp/demo/test/bpBlobData");
	}
}
