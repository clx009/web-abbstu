package com.hnykl.bp.web.family.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hnykl.bp.android.common.AndroidResult;
import com.hnykl.bp.base.tool.character.StringUtils;
import com.hnykl.bp.easemob.server.api.ChatGroupAPI;
import com.hnykl.bp.easemob.server.api.SendMessageAPI;
import com.hnykl.bp.easemob.server.comm.ClientContext;
import com.hnykl.bp.easemob.server.comm.EasemobRestAPIFactory;
import com.hnykl.bp.easemob.server.comm.body.CmdMessageBody;
import com.hnykl.bp.easemob.server.comm.wrapper.BodyWrapper;
import com.hnykl.bp.easemob.server.comm.wrapper.ResponseWrapper;
import com.hnykl.bp.web.AbstractAndroidController;
import com.hnykl.bp.web.family.constant.CheckInviteCodeResult;
import com.hnykl.bp.web.family.dto.FamilyUserDTO;
import com.hnykl.bp.web.family.entity.TFFamilyEntity;
import com.hnykl.bp.web.family.entity.TFInviteCodeEntity;
import com.hnykl.bp.web.family.entity.TFMemberEntity;
import com.hnykl.bp.web.family.service.TFFamilyServiceI;
import com.hnykl.bp.web.family.service.TFInviteCodeServiceI;
import com.hnykl.bp.web.family.service.TFMemberServiceI;
import com.hnykl.bp.web.position.entity.CommonAddressEntity;
import com.hnykl.bp.web.position.service.CommonAddressServiceI;
import com.hnykl.bp.web.system.pojo.base.TSUser;
import com.hnykl.bp.web.system.service.UserService;

/**
 * 家人圈管理手机接口
 * 
 * @author leihong
 * 
 */
@Controller
@RequestMapping("/memberController")
public class MemberController extends AbstractAndroidController {
	@Autowired
	TFMemberServiceI tFMemberService;
	
	@Autowired
	UserService userService;
	
	@Autowired
	TFInviteCodeServiceI tFInviliteCodeService;

	@Autowired
	TFFamilyServiceI tFFamilyService;
	
	@Autowired
	private CommonAddressServiceI commonAddressService;
	
	
	EasemobRestAPIFactory factory = ClientContext.getInstance()
			.init(ClientContext.INIT_FROM_PROPERTIES).getAPIFactory();

	/**
	 * 根据邀请码加入群组
	 * */
	@RequestMapping(params = "joinFamily")
	@ResponseBody
	public String joinFamily() {
		Map<String, Object> resultMap = new HashMap<String, Object>();

		String userName = StringUtils.getMapDataStr(reqBodyMap, "userName");
		String inviteCode = StringUtils.getMapDataStr(reqBodyMap, "inviteCode");
		String userId = StringUtils.getMapDataStr(reqBodyMap, "userId");
		String nickname = StringUtils.getMapDataStr(reqBodyMap, "nickname");

		try {
			TFMemberEntity entity = new TFMemberEntity();

			// 验证邀请码是否有效
			TFInviteCodeEntity ice = new TFInviteCodeEntity();
			ice.setInviteCode(inviteCode);
			CheckInviteCodeResult checkInviteCode = tFInviliteCodeService
					.checkInviteCode(ice);
			if (checkInviteCode == CheckInviteCodeResult.SUCCESS) {// 邀请码有效

				List<TFInviteCodeEntity> inviteCodeEntitys = tFInviliteCodeService
						.findByProperty(TFInviteCodeEntity.class, "inviteCode",
								inviteCode);
				TFInviteCodeEntity inviteCodeEntity = null;
				if (inviteCodeEntitys.isEmpty()) {
					return AndroidResult.defaultFailure("邀请码被删除了")
							.toAndroidResultString();
				} else {
					inviteCodeEntity = inviteCodeEntitys.get(0);
				}
				String familyId = inviteCodeEntity.getFamilyId();
				
				//验证重复添加
				if(!tFMemberService.checkMemberExsits(familyId,userId))
					return AndroidResult.defaultFailure("该成员已存在，不能重复添加").toAndroidResultString();
				
				String inviteType = inviteCodeEntity.getInviteType();

				entity.setType(inviteType);
				entity.setUserId(userId);
				entity.setFamilyId(familyId);
				entity.setNickname(nickname);
				String memberId = (String) tFMemberService.save(entity);
				entity.setId(memberId);

				TFFamilyEntity fe = tFFamilyService.getEntity(
						TFFamilyEntity.class, familyId);
				ChatGroupAPI chatgroup = (ChatGroupAPI) factory
						.newInstance(EasemobRestAPIFactory.CHATGROUP_CLASS);
				ResponseWrapper responseWraper2 = (ResponseWrapper) chatgroup
						.addSingleUserToChatGroup(fe.getGroupId(), userName);
				if (responseWraper2 != null
						&& responseWraper2.getResponseStatus() == 200) {// 环信群组增加成员成功
					
					SendMessageAPI senMessage = (SendMessageAPI)factory.newInstance(EasemobRestAPIFactory.SEND_MESSAGE_CLASS);
					Map<String, String> ext = new HashMap<String,String>();
					ext.put("action", "joinFamily");
					ext.put("familyId", fe.getId());
					ext.put("familyName", fe.getName());
					
					BodyWrapper cmdMessageBody = new CmdMessageBody("chatgroups", new String[]{fe.getGroupId()}, null, ext, "");
					@SuppressWarnings("unused")
					ResponseWrapper sendMessage = (ResponseWrapper)senMessage.sendMessage(cmdMessageBody);
					resultMap.put("family", fe);
					
					return new AndroidResult(AndroidResult.SUCCESS_CODE,
							AndroidResult.SUCCESS_MASSEGE, resultMap)
							.toAndroidResultString();
				} else {
					tFMemberService.delete(entity);
					return AndroidResult.defaultFailure(
							"环信群组增加成员失败 stauts:"
									+ responseWraper2.getResponseStatus()
									+ "body:"
									+ responseWraper2.getResponseBody())
							.toAndroidResultString(); 
				}

			} else {// 邀请码无效
				return AndroidResult.defaultFailure(checkInviteCode.toString())
						.toAndroidResultString();
			}
		} catch (Exception e) {
			e.printStackTrace();
			return AndroidResult.defaultFailure(e.getMessage())
					.toAndroidResultString();
		}
	}

	/**
	 * 用户退出群组
	 * */
	@RequestMapping(params = "dropOutFamily")
	@ResponseBody
	public String dropOutFamily() {
		String memberId = StringUtils.getMapDataStr(reqBodyMap, "memberId");
		String groupId = StringUtils.getMapDataStr(reqBodyMap, "groupId");
		String userId = StringUtils.getMapDataStr(reqBodyMap, "userId");

		try {
			TSUser user = userService.getEntity(TSUser.class, userId);
			String userName = user.getUserName();
			ChatGroupAPI chatgroup = (ChatGroupAPI) factory
					.newInstance(EasemobRestAPIFactory.CHATGROUP_CLASS);
			ResponseWrapper rw = (ResponseWrapper) chatgroup
					.removeSingleUserFromChatGroup(groupId, userName);
			if (rw != null && rw.getResponseStatus() == 200) {// 从环信群组删除成员
				TFMemberEntity entity = new TFMemberEntity();
				entity.setId(memberId);
				try {
					tFMemberService.delete(entity);
					SendMessageAPI senMessage = (SendMessageAPI)factory.newInstance(EasemobRestAPIFactory.SEND_MESSAGE_CLASS);
					Map<String, String> ext = new HashMap<String,String>();
					ext.put("action", "dropOutFamily");
					ext.put("memberId", memberId); 
					BodyWrapper cmdMessageBody = new CmdMessageBody("chatgroups", new String[]{groupId}, null, ext, ""); 
					@SuppressWarnings("unused")
					ResponseWrapper sendMessage = (ResponseWrapper)senMessage.sendMessage(cmdMessageBody);
					return AndroidResult.defaultSuccess()
							.toAndroidResultString();
				} catch (RuntimeException e) {
					chatgroup.addSingleUserToChatGroup(groupId, userName);// 本地删除失败重新添加环信群组成员，类似回滚
					e.printStackTrace();
					return AndroidResult.defaultFailure(e.getMessage())
							.toAndroidResultString();
				}
			} else {
				return AndroidResult.defaultFailure(
						"环信群组删除成员失败status:" + rw.getResponseStatus() + " body:"
								+ rw.getResponseBody()).toAndroidResultString();
			}
		} catch (Exception e) {
			e.printStackTrace();
			return AndroidResult.defaultFailure(e.getMessage())
					.toAndroidResultString();
		}
	}

	/**
	 * 查找群组成员
	 * */
	@RequestMapping(params = "findFamilyMembers")
	@ResponseBody
	public String findFamilyMembers() {

		Map<String, List<FamilyUserDTO>> resultMap = new HashMap<String, List<FamilyUserDTO>>();

		String familyId = StringUtils.getMapDataStr(reqBodyMap, "familyId");
		
		List<CommonAddressEntity>  commonAddressEntityList = new ArrayList<CommonAddressEntity>();
		
		try {

			List<FamilyUserDTO> members = tFFamilyService.findFamilyMembers(familyId);
			for(int i=0;members != null && i < members.size();i++ ){
				commonAddressEntityList = commonAddressService.findMemeberCommonAddress(members.get(i).getUserId() );
				members.get(i).setCommonAddressEntityList(commonAddressEntityList);
			}
			resultMap.put("members", members);
			return new AndroidResult(AndroidResult.SUCCESS_CODE,
					AndroidResult.SUCCESS_MASSEGE, resultMap)
					.toAndroidResultString();

		} catch (Exception e) {
			return AndroidResult.defaultFailure(e.getMessage())
					.toAndroidResultString();
		}
	}

}
