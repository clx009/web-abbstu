package com.hnykl.bp.web.family.controller;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.text.SimpleDateFormat;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.hnykl.bp.base.core.common.controller.BaseController;
import com.hnykl.bp.base.core.common.exception.BusinessException;
import com.hnykl.bp.base.core.common.hibernate.qbc.CriteriaQuery;
import com.hnykl.bp.base.core.common.model.json.AjaxJson;
import com.hnykl.bp.base.core.common.model.json.DataGrid;
import com.hnykl.bp.base.core.constant.Globals;
import com.hnykl.bp.base.core.util.StringUtil;
import com.hnykl.bp.tag.core.easyui.TagUtil;
import com.hnykl.bp.web.system.pojo.base.TSDepart;
import com.hnykl.bp.web.system.service.SystemService;
import com.hnykl.bp.base.core.util.MyBeanUtils;
import com.hnykl.bp.base.tool.character.StringUtils;

import com.hnykl.bp.web.family.entity.TFFamilyEntity;
import com.hnykl.bp.web.family.service.TFFamilyServiceI;

/**   
 * @Title: Controller
 * @Description: t_f_family
 * @author onlineGenerator
 * @date 2016-05-24 18:10:19
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/tFFamilyController")
public class TFFamilyController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(TFFamilyController.class);

	@Autowired
	private TFFamilyServiceI tFFamilyService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	} 

	/**
	 * t_f_family列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "tFFamily")
	public ModelAndView tFFamily(HttpServletRequest request) {
		return new ModelAndView("family/tFFamily/tFFamilyList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(TFFamilyEntity tFFamily,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(TFFamilyEntity.class, dataGrid);
		//查询条件组装器
		com.hnykl.bp.base.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, tFFamily, request.getParameterMap());
		try{
		//自定义追加查询条件
		}catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
		cq.add();
		this.tFFamilyService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除t_f_family
	 * 
	 * @return
	 */
	@RequestMapping(params = "doDel")
	@ResponseBody
	public AjaxJson doDel(TFFamilyEntity tFFamily, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		tFFamily = systemService.getEntity(TFFamilyEntity.class, tFFamily.getId());
		message = "t_f_family删除成功";
		try{
			tFFamilyService.delete(tFFamily);
			systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		}catch(Exception e){
			e.printStackTrace();
			message = "t_f_family删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	
	/**
	 * 批量删除t_f_family
	 * 
	 * @return
	 */
	 @RequestMapping(params = "doBatchDel")
	@ResponseBody
	public AjaxJson doBatchDel(String ids,HttpServletRequest request){
		AjaxJson j = new AjaxJson();
		message = "t_f_family删除成功";
		try{
			for(String id:ids.split(",")){
				TFFamilyEntity tFFamily = systemService.getEntity(TFFamilyEntity.class, 
				id
				);
				tFFamilyService.delete(tFFamily);
				systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
			}
		}catch(Exception e){
			e.printStackTrace();
			message = "t_f_family删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加t_f_family
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doAdd")
	@ResponseBody
	public AjaxJson doAdd(TFFamilyEntity tFFamily, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "t_f_family添加成功";
		try{
			tFFamilyService.save(tFFamily);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}catch(Exception e){
			e.printStackTrace();
			message = "t_f_family添加失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	
	/**
	 * 更新t_f_family
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doUpdate")
	@ResponseBody
	public AjaxJson doUpdate(TFFamilyEntity tFFamily, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "t_f_family更新成功";
		TFFamilyEntity t = tFFamilyService.get(TFFamilyEntity.class, tFFamily.getId());
		try {
			MyBeanUtils.copyBeanNotNull2Bean(tFFamily, t);
			tFFamilyService.saveOrUpdate(t);
			systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
		} catch (Exception e) {
			e.printStackTrace();
			message = "t_f_family更新失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	

	/**
	 * t_f_family新增页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goAdd")
	public ModelAndView goAdd(TFFamilyEntity tFFamily, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(tFFamily.getId())) {
			tFFamily = tFFamilyService.getEntity(TFFamilyEntity.class, tFFamily.getId());
			req.setAttribute("tFFamilyPage", tFFamily);
		}
		return new ModelAndView("family/tFFamily/tFFamily-add");
	}
	/**
	 * t_f_family编辑页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goUpdate")
	public ModelAndView goUpdate(TFFamilyEntity tFFamily, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(tFFamily.getId())) {
			tFFamily = tFFamilyService.getEntity(TFFamilyEntity.class, tFFamily.getId());
			req.setAttribute("tFFamilyPage", tFFamily);
		}
		return new ModelAndView("family/tFFamily/tFFamily-update");
	}
}
