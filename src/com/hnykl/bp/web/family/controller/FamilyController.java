package com.hnykl.bp.web.family.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hnykl.bp.android.common.AndroidResult;
import com.hnykl.bp.base.tool.character.StringUtils;
import com.hnykl.bp.base.tool.date.DateUtil;
import com.hnykl.bp.base.tool.validateCode.ValidateCodeUtils;
import com.hnykl.bp.web.AbstractAndroidController;
import com.hnykl.bp.web.family.entity.TFFamilyEntity;
import com.hnykl.bp.web.family.entity.TFInviteCodeEntity;
import com.hnykl.bp.web.family.service.TFFamilyServiceI;
import com.hnykl.bp.web.family.service.TFInviteCodeServiceI;

/**
 * 家人圈管理手机接口
 * 
 * @author leihong
 * 
 */
@Controller
@RequestMapping("/familyController")
public class FamilyController extends AbstractAndroidController {

	@Autowired
	private TFInviteCodeServiceI tFInviteCodeService;
	@Autowired
	private TFFamilyServiceI tFFamilyService;

	/**
	 * 群组成员邀请码
	 * */
	@RequestMapping(params = "sendFamilyInviteCode")
	@ResponseBody
	public String sendFamilyInviteCode() {
		String userId = StringUtils.getMapDataStr(reqBodyMap, "userId");
		String familyId = StringUtils.getMapDataStr(reqBodyMap, "familyId");
		String memberType = StringUtils.getMapDataStr(reqBodyMap, "memberType");
		try {

			TFInviteCodeEntity entity = new TFInviteCodeEntity();
			String inviteCode = ValidateCodeUtils.get4ValidateCode();

			while (!tFInviteCodeService.findByProperty(
					TFInviteCodeEntity.class, "inviteCode", inviteCode)
					.isEmpty()) {
				inviteCode = ValidateCodeUtils.get4ValidateCode();
			}

			entity.setFamilyId(familyId);
			entity.setInviterId(userId);
			entity.setInviteType(memberType);
			entity.setCreateTime(DateUtil.getSysTimeStamp());
			entity.setInviteCode(inviteCode);
			tFInviteCodeService.save(entity);
			HashMap<String, String> resultMap = new HashMap<String, String>();
			resultMap.put("inviteCode", inviteCode);
			return new AndroidResult(replyCode, replyMsg, resultMap)
					.toAndroidResultString();
		} catch (Exception e) {
			e.printStackTrace();
			return AndroidResult.defaultFailure().toAndroidResultString();
		}
	}

	@RequestMapping(params = "findFamily")
	@ResponseBody
	public String findFamily() {
		String userId = StringUtils.getMapDataStr(reqBodyMap, "userId");
		try {
			List<TFFamilyEntity> findFamily = tFFamilyService
					.findFamily(userId);
			Map<String, Object> resultMap = new HashMap<String, Object>();
			resultMap.put("familys", findFamily);
			return new AndroidResult(AndroidResult.SUCCESS_CODE,
					AndroidResult.SUCCESS_MASSEGE, resultMap)
					.toAndroidResultString();
		} catch (Exception e) {
			e.printStackTrace();
			return AndroidResult.defaultFailure(e.getMessage())
					.toAndroidResultString();
		}
	}

}
