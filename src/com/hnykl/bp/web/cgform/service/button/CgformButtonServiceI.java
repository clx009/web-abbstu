package com.hnykl.bp.web.cgform.service.button;

import java.util.List;

import com.hnykl.bp.web.cgform.entity.button.CgformButtonEntity;

import com.hnykl.bp.base.core.common.service.CommonService;

/**
 * 
 * @author  
 *
 */
public interface CgformButtonServiceI extends CommonService{
	
	/**
	 * 查询按钮list
	 * @param formId
	 * @return
	 */
	public List<CgformButtonEntity> getCgformButtonListByFormId(String formId);

	/**
	 * 校验按钮唯一性
	 * @param cgformButtonEntity
	 * @return
	 */
	public List<CgformButtonEntity> checkCgformButton(CgformButtonEntity cgformButtonEntity);
	
	
}
