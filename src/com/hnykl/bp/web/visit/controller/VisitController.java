package com.hnykl.bp.web.visit.controller;
 
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.ezmorph.object.DateMorpher;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.util.JSONUtils;

import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hnykl.bp.android.common.AndroidResult;
import com.hnykl.bp.base.configuration.BaseConfigMgr;
import com.hnykl.bp.base.tool.bean.BeanUtils;
import com.hnykl.bp.base.tool.character.StringUtils;
import com.hnykl.bp.web.AbstractAndroidController;
import com.hnykl.bp.web.system.pojo.base.TSType;
import com.hnykl.bp.web.system.pojo.base.TSTypegroup;
import com.hnykl.bp.web.system.service.SystemService;
import com.hnykl.bp.web.visit.entity.MaterialEntity;
import com.hnykl.bp.web.visit.entity.OptionEntity;
import com.hnykl.bp.web.visit.entity.VisitEntity;
import com.hnykl.bp.web.visit.entity.VisitOptionEntity;
import com.hnykl.bp.web.visit.service.VisitServiceI;

@Controller
@RequestMapping("/visitController")
public class VisitController extends AbstractAndroidController {

	@Autowired
	private VisitServiceI visitService;
	
	@Autowired
	private SystemService systemService;

	/**
	 * 申请家访、校访 、专家咨询
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(params = "createVisit")
	@ResponseBody
	public String createVisit() {
		try {
			String[] dateFormats = new String[] {"yyyy-MM-dd HH:mm:ss","yyyy-MM-dd","yyyy/MM/dd HH:mm:ss","yyyy/MM/dd"}; 
			JSONUtils.getMorpherRegistry().registerMorpher(new DateMorpher(dateFormats));
			VisitEntity visit = null;
			Object visitJson = JSONObject.fromObject(reqBodyMap).get("visit");
			if(visitJson!=null){
				visit = (VisitEntity) JSONObject.toBean(JSONObject.fromObject(visitJson.toString()),VisitEntity.class);
			}
			Map dataMap = ((Map)visitJson); 
			String typeId = StringUtils.getMapDataStr(dataMap, "type");
			String creatorId = StringUtils.getMapDataStr(dataMap, "creatorId");
			visit.getCreator().setId(creatorId);
			List<VisitOptionEntity> visitOptions = new ArrayList<VisitOptionEntity>();
			Object visitOptionsJson = JSONObject.fromObject(dataMap).get("visitOptions");
			if(visitOptionsJson!=null){
				visitOptions = JSONArray.toList(JSONArray.fromObject(visitOptionsJson.toString()), VisitOptionEntity.class);
			}
			visit.setVisitOptions(visitOptions);
			visit.setCreateTime(new Date());
			
			//判断是哪季并写入到数据库
			List<Map> dataList = visitService.statisticsVisit(visit.getCreator().getId());
			//通过当前时间判断，是属于哪个季度的
			Calendar cal = Calendar.getInstance();
			int month=cal.get(Calendar.MONTH)+1;
			int year=cal.get(Calendar.YEAR );
			String jj = "";
			if(month >= 1 && month <=3) jj = "春季";
			else if(month >= 4 && month <=6) jj = "夏季";
			else if(month >= 7 && month <=9) jj = "秋季";
			else if(month >= 10 && month <=12) jj = "冬季";
			boolean flag = false;
			for(int i=0;dataList != null && i<dataList.size();i++){
				if(jj.equals(dataList.get(i).get("season"))){
					flag = true;
					visit.setTitle(year+"年      "+ jj +"第"+(((dataList.get(i).get("totalNum") != null)?Integer.parseInt(dataList.get(i).get("totalNum").toString()):0) +1)+"次访谈"  );
				}
			}
			if(!flag)visit.setTitle(year+"年      "+ jj +"第1次访谈"  );
			visitService.createVisit(visit);
			return AndroidResult.defaultSuccess("申请已提交")
					.toAndroidResultString();
		} catch (Exception e) {
			e.printStackTrace();
			return AndroidResult.defaultFailure(e.getMessage())
					.toAndroidResultString();
		}
	}

	/**
	 * 查询访申请问列表
	 * 
	 * @return
	 */
	@RequestMapping(params = "visitList")
	@ResponseBody
	public String visitList() {
		try {
			String userId = StringUtils.getMapDataStr(reqBodyMap, "userId");// 委托人id
			String type = StringUtils.getMapDataStr(reqBodyMap, "type");// 委托人id
			Map<String, List<Map>> resultData = new HashMap<String, List<Map>>();
			List<Map> dataList = new ArrayList<Map>();
			String[] types = type.split(",");
			for(String t:types){
				VisitEntity paramVisitEntity = new VisitEntity();
				paramVisitEntity.getCreator().setId(userId);
				paramVisitEntity.setType(t);
				List<VisitEntity> visits = visitService.findVisitList(paramVisitEntity);
				Map itemMap = null;

				for(int i=0;visits != null && i<visits.size();i++){
					VisitEntity visitEntity =  visits.get(i);
					itemMap = BeanUtils.getAllFieldValues(visitEntity);
					itemMap.put("type",visitEntity.getType() );
					itemMap.remove("creator");
					itemMap.remove("visitOptions");
					
					
					//加载资料
					TSTypegroup tsTypegroup = systemService.getTypeGroup(
							"VisitFileType", "访谈文件类别");
					TSType tsType = systemService.getType("VisitReport", "访谈报告",
							tsTypegroup);
					@SuppressWarnings("unchecked")
					List<MaterialEntity> reports = visitService.getSession()
							.createCriteria(MaterialEntity.class)
							.add(Restrictions.eq("TSType", tsType))
							.add(Restrictions.eq("businessKey", visitEntity.getId())).list();
					Map<String, String> reportsMap = new HashMap<String,String>();
					for(MaterialEntity me:reports){
						reportsMap.put("reportUrl",BaseConfigMgr.getWebVisitUrl() + me.getRealpath());
					}
					itemMap.put("reports",reportsMap);
					
					tsType = systemService.getType("VisitMaterial", "访谈资料",
							tsTypegroup);
					@SuppressWarnings("unchecked")
					List<MaterialEntity> materials = visitService.getSession()
							.createCriteria(MaterialEntity.class)
							.add(Restrictions.eq("TSType", tsType))
							.add(Restrictions.eq("businessKey", visitEntity.getId())).list();
					Map<String, String> materialMap = new HashMap<String,String>();
					for(MaterialEntity me:materials){
						materialMap.put("materialUrl", BaseConfigMgr.getWebVisitUrl() + me.getRealpath());
					}
					itemMap.put("materials",materialMap);
					dataList.add(itemMap);
					
				}
			}
			
			resultData.put("visits", dataList);
			return new AndroidResult(AndroidResult.SUCCESS_CODE,
					AndroidResult.SUCCESS_MASSEGE, resultData)
					.toAndroidResultString();
		} catch (Exception e) {
			e.printStackTrace();
			return AndroidResult.defaultFailure(e.getMessage())
					.toAndroidResultString();
		}
	}
	@RequestMapping(params = "findVisitOptionList")
	@ResponseBody
	public String findVisitOptionList(){
		try {
			String type = StringUtils.getMapDataStr(reqBodyMap, "type");
			
			Map<String, List<OptionEntity>> resultData = new HashMap<String, List<OptionEntity>>();
			List<OptionEntity> options = visitService.findOptions(type);
			
			resultData.put("options", options);
			return new AndroidResult(AndroidResult.SUCCESS_CODE,
					AndroidResult.SUCCESS_MASSEGE, resultData)
					.toAndroidResultString();
		} catch (Exception e) {
			e.printStackTrace();
			return AndroidResult.defaultFailure(e.getMessage())
					.toAndroidResultString();
		}
	}
	
	
	/**
	 * 查询访问详情
	 * 
	 * @return
	 */
	@RequestMapping(params = "visitDetail")
	@ResponseBody
	public String visitDetail() {
		try {
			String id = StringUtils.getMapDataStr(reqBodyMap, "id");// 委托人id
			Map<String, VisitEntity> resultData = new HashMap<String, VisitEntity>();
			VisitEntity visitEntity = visitService.get(VisitEntity.class, id);
			resultData.put("visit", visitEntity);
			return new AndroidResult(AndroidResult.SUCCESS_CODE,
					AndroidResult.SUCCESS_MASSEGE, resultData)
					.toAndroidResultString();
		} catch (Exception e) {
			e.printStackTrace();
			return AndroidResult.defaultFailure(e.getMessage())
					.toAndroidResultString();
		}
	}

}
