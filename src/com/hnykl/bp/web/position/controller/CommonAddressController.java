package com.hnykl.bp.web.position.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hnykl.bp.android.common.AndroidResult;
import com.hnykl.bp.base.tool.character.StringUtils;
import com.hnykl.bp.base.tool.date.DateUtil;
import com.hnykl.bp.web.AbstractAndroidController;
import com.hnykl.bp.web.position.entity.CommonAddressEntity;
import com.hnykl.bp.web.position.service.CommonAddressServiceI;

/**
 * 常用地址管理控制器
 * 
 * @author leihong
 * 
 */
@Controller
@RequestMapping("/commonAddressController")
public class CommonAddressController extends AbstractAndroidController {

	@Autowired
	CommonAddressServiceI commonAddressService;

	/**
	 * 手机端接口 根据家人圈成员id获取该成员的常用地址列表
	 * 
	 * @return 成员地址实体列表
	 */
	@RequestMapping(params = "findMemberCommonAddress")
	@ResponseBody
	public String findMemberCommonAddress() {
		String familyUserId = StringUtils.getMapDataStr(reqBodyMap,
				"familyUserId");
		try {
			Map<String, List<CommonAddressEntity>> resultData = new HashMap<String, List<CommonAddressEntity>>();
			resultData.put("commAddress", commonAddressService.findMemeberCommonAddress(familyUserId));

			return new AndroidResult(AndroidResult.SUCCESS_CODE,
					AndroidResult.SUCCESS_MASSEGE, resultData)
					.toAndroidResultString();

		} catch (Exception e) {
			e.printStackTrace();
			return AndroidResult.defaultFailure().toAndroidResultString();
		}
	}

	/**
	 * 创建家人圈成员常用地址
	 * 
	 * @return
	 */
	@RequestMapping(params = "createCommonAddress")
	@ResponseBody
	public String createCommonAddress() {
		String userId = StringUtils.getMapDataStr(reqBodyMap, "userId");
		String familyUserId = StringUtils.getMapDataStr(reqBodyMap,
				"familyUserId");
		String address = StringUtils.getMapDataStr(reqBodyMap, "address");
		String longitude = StringUtils.getMapDataStr(reqBodyMap, "longitude");
		String latitude = StringUtils.getMapDataStr(reqBodyMap, "latitude");
		String radius = StringUtils.getMapDataStr(reqBodyMap, "radius");
		String name = StringUtils.getMapDataStr(reqBodyMap, "name");
		String type = StringUtils.getMapDataStr(reqBodyMap, "type");

		try {
			CommonAddressEntity commonAddressEntity = new CommonAddressEntity(
					familyUserId, address, longitude, latitude, radius,
					DateUtil.getSysTimeStamp(), userId, null, null,name,type);
			commonAddressService.saveCommonAddress(commonAddressEntity);
			Map<String, CommonAddressEntity> resultData = new HashMap<String, CommonAddressEntity>();
			resultData.put("commonAddress", commonAddressEntity);
			return new AndroidResult(AndroidResult.SUCCESS_CODE,
					AndroidResult.SUCCESS_MASSEGE, resultData)
					.toAndroidResultString();
		} catch (Exception e) {
			e.printStackTrace();
			return AndroidResult.defaultFailure().toAndroidResultString();
		}
	}

	/**
	 * 手机端接口 修改成员常用地址信息
	 * 
	 * @return
	 */
	@RequestMapping(params = "modifyCommonAddress")
	@ResponseBody
	public String modifyCommonAddress() {

		String userId = StringUtils.getMapDataStr(reqBodyMap, "userId");
		String commAddrId = StringUtils.getMapDataStr(reqBodyMap, "commAddrId");
		String address = StringUtils.getMapDataStr(reqBodyMap, "address");
		String longitude = StringUtils.getMapDataStr(reqBodyMap, "longitude");
		String latitude = StringUtils.getMapDataStr(reqBodyMap, "latitude");
		String radius = StringUtils.getMapDataStr(reqBodyMap, "radius");
		String name = StringUtils.getMapDataStr(reqBodyMap, "name");
		String type = StringUtils.getMapDataStr(reqBodyMap, "type");

		try {
			CommonAddressEntity commonAddressEntity = new CommonAddressEntity(
					null, address, longitude, latitude, radius, null, null,
					DateUtil.getSysTimeStamp(), userId,name,type);
			commonAddressEntity.setId(commAddrId);
			commonAddressService.modifyCommonAddress(commonAddressEntity);
			return AndroidResult.defaultSuccess().toAndroidResultString();
		} catch (Exception e) {
			e.printStackTrace();
			return AndroidResult.defaultFailure().toAndroidResultString();
		}

	}

	/**
	 * 手机接口 根据常用地址id，删除常用地址
	 * 
	 * @return
	 */
	@RequestMapping(params = "deleteCommonAddress")
	@ResponseBody
	public String deleteCommonAddress() {
		String id = StringUtils.getMapDataStr(reqBodyMap, "commAddrId");
		try {
			commonAddressService
					.deleteEntityById(CommonAddressEntity.class, id);
			return AndroidResult.defaultSuccess().toAndroidResultString();
		} catch (Exception e) {
			e.printStackTrace();
			return AndroidResult.defaultFailure().toAndroidResultString();
		}
	}
}
