package com.hnykl.bp.web.position.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.hnykl.bp.android.common.AndroidResult;
import com.hnykl.bp.base.configuration.BaseConfigMgr;
import com.hnykl.bp.base.tool.character.StringUtils;
import com.hnykl.bp.base.tool.date.DateUtil;
import com.hnykl.bp.base.tool.file.FileUtil;
import com.hnykl.bp.easemob.server.api.SendMessageAPI;
import com.hnykl.bp.easemob.server.comm.ClientContext;
import com.hnykl.bp.easemob.server.comm.EasemobRestAPIFactory;
import com.hnykl.bp.easemob.server.comm.body.TextMessageBody;
import com.hnykl.bp.easemob.server.comm.wrapper.BodyWrapper;
import com.hnykl.bp.easemob.server.comm.wrapper.ResponseWrapper;
import com.hnykl.bp.web.AbstractAndroidController;
import com.hnykl.bp.web.family.service.TFMemberServiceI;
import com.hnykl.bp.web.position.entity.TPPositionEntity;
import com.hnykl.bp.web.position.service.TPPositionServiceI;
import com.hnykl.bp.web.system.entity.MessageUserEntity;

/**
 * 位置信息接口操作控制器
 * */
@Controller
@RequestMapping("/positionController")
public class PositionController extends AbstractAndroidController {
	@Autowired
	TFMemberServiceI tFMemberService;
	
	@Autowired
	private TPPositionServiceI tPPositionService;
	
	private Map<String, TPPositionEntity> curPos = new HashMap<String, TPPositionEntity>();
 
	EasemobRestAPIFactory factory = ClientContext.getInstance()
			.init(ClientContext.INIT_FROM_PROPERTIES).getAPIFactory();
	
	private static final  double EARTH_RADIUS = 6378137;//赤道半径(单位m)  
    
    /** 
     * 转化为弧度(rad) 
     * */  
    private static double rad(double d)  
    {  
       return d * Math.PI / 180.0;  
    }  
      
    /** 
     * 基于余弦定理求两经纬度距离 
     * @param lon1 第一点的经度 
     * @param lat1 第一点的纬度 
     * @param lon2 第二点的经度 
     * @param lat3 第二点的纬度 
     * @return 返回的距离，单位km 
     * */  
    public static double LantitudeLongitudeDist(double lon1, double lat1,double lon2, double lat2) {  
        double radLat1 = rad(lat1);  
        double radLat2 = rad(lat2);  
  
        double radLon1 = rad(lon1);  
        double radLon2 = rad(lon2);  
  
        if (radLat1 < 0)  
            radLat1 = Math.PI / 2 + Math.abs(radLat1);// south  
        if (radLat1 > 0)  
            radLat1 = Math.PI / 2 - Math.abs(radLat1);// north  
        if (radLon1 < 0)  
            radLon1 = Math.PI * 2 - Math.abs(radLon1);// west  
        if (radLat2 < 0)  
            radLat2 = Math.PI / 2 + Math.abs(radLat2);// south  
        if (radLat2 > 0)  
            radLat2 = Math.PI / 2 - Math.abs(radLat2);// north  
        if (radLon2 < 0)  
            radLon2 = Math.PI * 2 - Math.abs(radLon2);// west  
        double x1 = EARTH_RADIUS * Math.cos(radLon1) * Math.sin(radLat1);  
        double y1 = EARTH_RADIUS * Math.sin(radLon1) * Math.sin(radLat1);  
        double z1 = EARTH_RADIUS * Math.cos(radLat1);  
  
        double x2 = EARTH_RADIUS * Math.cos(radLon2) * Math.sin(radLat2);  
        double y2 = EARTH_RADIUS * Math.sin(radLon2) * Math.sin(radLat2);  
        double z2 = EARTH_RADIUS * Math.cos(radLat2);  
  
        double d = Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2)+ (z1 - z2) * (z1 - z2));  
        //余弦定理求夹角  
        double theta = Math.acos((EARTH_RADIUS * EARTH_RADIUS + EARTH_RADIUS * EARTH_RADIUS - d * d) / (2 * EARTH_RADIUS * EARTH_RADIUS));  
        double dist = theta * EARTH_RADIUS;  
        return dist;  
    }	

	/**
	 * 上传位置信息
	 * 
	 * @return
	 */
	@RequestMapping(params = "uploadPosition")
	@ResponseBody
	public String uploadPosition(@Valid String userId, @Valid String longitude,
			@Valid String latitude, @Valid String describe,
			@Valid String username, @Valid String createTime,
			@Valid String timeZone, @RequestParam MultipartFile file) {
		try {
			String filePath = "";
			if (file != null) {

				String uploadPath = BaseConfigMgr.getUploadpath();
				String savePath = uploadPath + "/"
						+ "positionScreenshot" + "/" + username
						+ "/" + DateUtil.getSysDate()
						+ "/";

				String newFileName = UUID.randomUUID() + "["
						+ createTime.substring(11).replace(":", "") + "]"
						+ ".jpg";

				filePath = savePath + newFileName;

				if (!FileUtil.fileExists(savePath)) {
					FileUtil.createDir(savePath);
				}
				FileUtil.writeFile(newFileName, savePath, file.getBytes());
			}
			
			if (curPos.containsKey(userId)) {
				TPPositionEntity curEntity = curPos.get(userId);
				double distance = LantitudeLongitudeDist(Long.valueOf(curEntity.getLongitude()),
														 Long.valueOf(curEntity.getLatitude()),
														 Long.valueOf(longitude),
														 Long.valueOf(latitude));
				if (distance <= 100.00) {
					curEntity.setStayTime();
				}
														 
			}
			
			TPPositionEntity entity = new TPPositionEntity(userId, longitude,
					latitude, filePath, describe,
					Timestamp.valueOf(createTime), timeZone, "0");

			tPPositionService.savePosition(entity);

			return AndroidResult.defaultSuccess("位置已上传")
					.toAndroidResultString();
		} catch (Exception e) {
			e.printStackTrace();
			return AndroidResult.defaultFailure(e.getMessage())
					.toAndroidResultString();
		}
	}

	/**
	 * 手机端接口 根据成员id查询某时间段内位置列表
	 * 
	 * @return 成员位置实体列表
	 */
	@RequestMapping(params = "findMemberPositions")
	@ResponseBody
	public String findMemberPositions() {
		String userId = StringUtils.getMapDataStr(reqBodyMap, "userId");
		String startTime = StringUtils.getMapDataStr(reqBodyMap, "startTime");
		String endTime = StringUtils.getMapDataStr(reqBodyMap, "endTime");

		try {
			Timestamp start = Timestamp.valueOf(startTime);
			Timestamp end = Timestamp.valueOf(endTime);

			Map<String, List<TPPositionEntity>> resultData = new HashMap<String, List<TPPositionEntity>>();
			resultData.put("positons",
					tPPositionService.findMemberPositions(userId, start, end));
			return new AndroidResult(AndroidResult.SUCCESS_CODE,
					AndroidResult.SUCCESS_MASSEGE, resultData)
					.toAndroidResultString();

		} catch (Exception e) {
			e.printStackTrace();
			return AndroidResult.defaultFailure(e.getMessage())
					.toAndroidResultString();
		}
	}

	/**
	 * 上传不带位置图片的位置信息
	 * 
	 * @return
	 */
	@RequestMapping(params = "uploadPositionWithoutFile")
	@ResponseBody
	public String uploadPositionWithoutFile() {
		try {
			String userId = StringUtils.getMapDataStr(reqBodyMap, "userId");
			String longitude = StringUtils.getMapDataStr(reqBodyMap,"longitude");
			String latitude = StringUtils.getMapDataStr(reqBodyMap, "latitude");
			String describe = StringUtils.getMapDataStr(reqBodyMap, "describe");
			String createTime = StringUtils.getMapDataStr(reqBodyMap,
					"createTime");
			String timeZone = StringUtils.getMapDataStr(reqBodyMap, "timeZone");
			if(StringUtils.isEmpty(longitude)
					|| StringUtils.isEmpty(latitude)){
				return AndroidResult.defaultFailure("经纬度为空！")
						.toAndroidResultString();
			}
			TPPositionEntity entity = new TPPositionEntity(userId, longitude,
					latitude, "", describe, Timestamp.valueOf(createTime),
					timeZone, "0");
			tPPositionService.savePosition(entity);
			return AndroidResult.defaultSuccess("位置已经上传").toAndroidResultString();
		} catch (Exception e) {
			return AndroidResult.defaultFailure(e.getMessage())
					.toAndroidResultString();
		}
	}

	/**
	 * 报平安
	 * 
	 * @return
	 */ 
	@RequestMapping(params = "reportSafe")
	@ResponseBody
	public String reportSafe(@Valid String familyId, @Valid String userId,
			@Valid String longitude, @Valid String latitude,
			@Valid String describe, @Valid String username,
			@Valid String createTime, @Valid String timeZone,
			@RequestParam MultipartFile file) {
		try {
			if(StringUtils.isEmpty(longitude)
					|| StringUtils.isEmpty(latitude)){
				return AndroidResult.defaultFailure("经纬度为空！")
						.toAndroidResultString();
			}
			String filePath = "";
			if (file != null) {

				String uploadPath = BaseConfigMgr.getUploadpath();
				String basePath = BaseConfigMgr.getWebRootPath();
				String savePath = uploadPath + "/"
						+ "positionScreenshot" + "/" + username
						+ "/" + DateUtil.getSysDate()
						+ "/";

				String newFileName = UUID.randomUUID() + "["
						+ createTime.substring(11).replace(":", "") + "]"
						+ ".jpg";

				filePath = savePath + newFileName;

				if (!FileUtil.fileExists(basePath +"/"+savePath)) {
					FileUtil.createDir(basePath +"/"+savePath);
				}
				FileUtil.writeFile(newFileName, basePath +"/"+savePath, file.getBytes());
			}
			TPPositionEntity entity = new TPPositionEntity(userId, longitude,
					latitude, filePath, describe,
					Timestamp.valueOf(createTime), timeZone, "0");

			entity.setType("1");
			tPPositionService.savePosition(entity);
			SendMessageAPI senMessage = (SendMessageAPI) factory
					.newInstance(EasemobRestAPIFactory.SEND_MESSAGE_CLASS);
			Map<String, String> ext = new HashMap<String, String>();
			ext.put("action", "reportSafe");
			ext.put("userId", userId);
			ext.put("username", username);
			
			//根据familyId找到所有需要受到通知的成员
			List<Map> adultUserNames = tFMemberService.findAdultMember(familyId);
			if(adultUserNames.isEmpty()){
				return AndroidResult.defaultSuccess("已上报平安，但未通知任何人").toAndroidResultString();
			}
			String[] targets = new String[adultUserNames.size()];
			
			MessageUserEntity messageUserEntity = new MessageUserEntity();
			List<MessageUserEntity> messageUserEntityList = new ArrayList<MessageUserEntity>();
			for(int i=0;i<adultUserNames.size();i++){
				targets[i] = (String)adultUserNames.get(i).get("username");
				messageUserEntity = new MessageUserEntity();
				messageUserEntity.setMessageId(entity.getId());
				messageUserEntity.setFromUserId(userId);
				messageUserEntity.setToUserId((String)adultUserNames.get(i).get("id"));
				messageUserEntityList.add(messageUserEntity);
			} 
			tPPositionService.batchSave(messageUserEntityList);
			
			BodyWrapper textMessageBody = new TextMessageBody("users",
					targets, null, ext, ""); 
			@SuppressWarnings("unused")
			ResponseWrapper sendMessage = (ResponseWrapper) senMessage
					.sendMessage(textMessageBody);
			return AndroidResult.defaultSuccess("已上报平安")
					.toAndroidResultString(); 
			
		} catch (Exception e) {
			e.printStackTrace();
			return AndroidResult.defaultFailure(e.getMessage())
					.toAndroidResultString();
		}
	}
	
	/**
	 *分析足迹
	 * @return
	 */
	@RequestMapping(params = "analysePosition")
	@ResponseBody
	public String analysePosition(){
		try {
			String userId = StringUtils.getMapDataStr(reqBodyMap, "userId");
			String startTime = StringUtils.getMapDataStr(reqBodyMap,"startTime");
			String endTime = StringUtils.getMapDataStr(reqBodyMap,"endTime");
			Map itemMap = tPPositionService.analysePosition(userId, startTime, endTime);
			return new AndroidResult(AndroidResult.SUCCESS_CODE,
					AndroidResult.SUCCESS_MASSEGE, itemMap)
					.toAndroidResultString();
		} catch (Exception e) {
			return AndroidResult.defaultFailure(e.getMessage())
					.toAndroidResultString();
		}
	}
	
	
}
