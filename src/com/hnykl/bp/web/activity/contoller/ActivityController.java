package com.hnykl.bp.web.activity.contoller;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map; 

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping; 
import org.springframework.web.bind.annotation.ResponseBody;
 
import com.hnykl.bp.android.common.AndroidResult; 
import com.hnykl.bp.base.tool.character.StringUtils;
import com.hnykl.bp.web.AbstractAndroidController;
import com.hnykl.bp.web.activity.entity.ActivityEntity;
import com.hnykl.bp.web.activity.service.ActivityServiceI;

/**
 * 
 * @author Administrator
 *
 */

@Controller
@RequestMapping("/activityController")
public class ActivityController extends AbstractAndroidController{
	@Autowired
	private ActivityServiceI activityService;
	
	/**
	 * 
	 * @return
	 */
	@RequestMapping(params="createActivities")
	@ResponseBody
	public String createActivities(){   
		try {  
			@SuppressWarnings("unchecked")
			List<ActivityEntity> entities = (List<ActivityEntity>) translateJsonToList(reqBodyMap.get("ActivityItems"),ActivityEntity.class); 
			activityService.saveActivities(entities);
			return AndroidResult.defaultSuccess("创建活动成功").toAndroidResultString();
		} catch (Exception e) {
			e.printStackTrace();
			return AndroidResult.defaultFailure(e.getMessage()).toAndroidResultString();
		} 
	}
	
	/**
	 * 
	 * @return
	 */
	@RequestMapping(params="modifyActivities")
	@ResponseBody
	public String modifyActivities(){ 
		try { 
			@SuppressWarnings("unchecked")
			List<ActivityEntity> entities = (List<ActivityEntity>) translateJsonToList(reqBodyMap.get("ActivityItems"),ActivityEntity.class);  
			activityService.modifyActivities(entities);
			return AndroidResult.defaultSuccess("活动已修改").toAndroidResultString();
		} catch (Exception e) { 
			e.printStackTrace();
			return AndroidResult.defaultFailure(e.getMessage()).toAndroidResultString();
		}
	}
	
	/**
	 * 
	 * @return
	 */
	@RequestMapping(params="findActivity")
	@ResponseBody
	public String findActivity(){
		String userId =  StringUtils.getMapDataStr(reqBodyMap, "userId");
		String startTime =  StringUtils.getMapDataStr(reqBodyMap, "startTime");
		String endTime =  StringUtils.getMapDataStr(reqBodyMap, "endTime");
		
		try {
			Map<String,List<ActivityEntity>> resultData=new HashMap<String,List<ActivityEntity>>();
			List<ActivityEntity> findActivity = activityService.findActivity(userId,Timestamp.valueOf(startTime),Timestamp.valueOf(endTime));
			resultData.put("activities", findActivity);
			return new AndroidResult(AndroidResult.SUCCESS_CODE, AndroidResult.SUCCESS_MASSEGE, resultData).toAndroidResultString();
		} catch (Exception e) {
			e.printStackTrace();
			return AndroidResult.defaultFailure(e.getMessage()).toAndroidResultString();
		}
		
	}
	/**
	 * 
	 * @return
	 */
	@RequestMapping(params="deleteActivity")
	@ResponseBody
	public String deleteActivity(){
		String id = StringUtils.getMapDataStr(reqBodyMap, "id");
		try {
			activityService.deleteEntityById(ActivityEntity.class, id);
			return AndroidResult.defaultSuccess("活动已删除").toAndroidResultString();
		} catch (Exception e) {
			e.printStackTrace();
			return AndroidResult.defaultFailure(e.getMessage()).toAndroidResultString();
		}
	}
	
	/**
	 * 
	 * @return
	 */
	@RequestMapping(params="deleteActivities")
	@ResponseBody
	public String deleteActivities(){
		try {
			@SuppressWarnings("unchecked")
			List<String> ids = (List<String>) this.translateJsonToList(reqBodyMap.get("ids"), String.class);
			activityService.deleteActivities(ids);
			return AndroidResult.defaultSuccess("活动已删除").toAndroidResultString();
		} catch (Exception e) {
			e.printStackTrace();
			return AndroidResult.defaultFailure(e.getMessage()).toAndroidResultString();
		}
	}
}
