package com.hnykl.bp.web.system.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.node.ContainerNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.hnykl.bp.android.common.AndroidResult;
import com.hnykl.bp.base.configuration.BaseConfigMgr;
import com.hnykl.bp.base.core.constant.Globals;
import com.hnykl.bp.base.tool.character.StringUtils;
import com.hnykl.bp.easemob.server.api.IMUserAPI;
import com.hnykl.bp.easemob.server.comm.ClientContext;
import com.hnykl.bp.easemob.server.comm.EasemobRestAPIFactory;
import com.hnykl.bp.easemob.server.comm.wrapper.BodyWrapper;
import com.hnykl.bp.easemob.server.comm.wrapper.ResponseWrapper;
import com.hnykl.bp.web.AbstractAndroidController;
import com.hnykl.bp.web.exam.entity.ExamResultEntity;
import com.hnykl.bp.web.exam.service.ExamResultServiceI;
import com.hnykl.bp.web.family.entity.TFFamilyEntity;
import com.hnykl.bp.web.family.service.TFFamilyServiceI;
import com.hnykl.bp.web.position.service.TPPositionServiceI;
import com.hnykl.bp.web.system.enums.PhoneValidateCodeCheckResult;
import com.hnykl.bp.web.system.pojo.base.TSPhoneValidateCode;
import com.hnykl.bp.web.system.pojo.base.TSUser;
import com.hnykl.bp.web.system.service.PhoneValidateCodeService;
import com.hnykl.bp.web.system.service.UserService;

/**
 * 登陆初始化控制器
 * 
 * @author
 * 
 */
@Controller
@RequestMapping("/loginController")
public class LoginController extends AbstractAndroidController {
	@SuppressWarnings("unused")
	private Logger log = Logger.getLogger(LoginController.class);
	private UserService userService;
	private PhoneValidateCodeService phoneValidateCodeService;
	@Autowired
	private TFFamilyServiceI tFFamilyService;
	@Autowired
	private TPPositionServiceI tPPositionService;

	EasemobRestAPIFactory factory = ClientContext.getInstance()
			.init(ClientContext.INIT_FROM_PROPERTIES).getAPIFactory();

	IMUserAPI iMUserAPI = (IMUserAPI) factory
			.newInstance(EasemobRestAPIFactory.USER_CLASS);

	@Autowired
	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	@Autowired
	private ExamResultServiceI examResultService;
	
	@Autowired
	public void setPhoneValidateCodeService(
			PhoneValidateCodeService phoneValidateCodeService) {
		this.phoneValidateCodeService = phoneValidateCodeService;
	}

	/**
	 * 检查用户名称
	 * 
	 * @param user
	 * @param req
	 * @return
	 */
	@RequestMapping(params = "login")
	@ResponseBody
	public String login(HttpServletRequest request, HttpServletResponse response) {
		String userName = StringUtils.getMapDataStr(reqBodyMap, "userName");
		String password = StringUtils.getMapDataStr(reqBodyMap, "password");
		try {
			TSUser user = new TSUser();
     		user.setUserName(userName);
			user.setPassword(password);
			user.setUserName("13458560689");
			user.setPassword("123456");
			TSUser retUser = userService.checkUserExits(user); 
			if (retUser != null) {
				/*List<TFFamilyEntity> findFamily = tFFamilyService
						.findFamily(retUser.getId());*/
				//取得地址
				//List<TPPositionEntity> tPPositionEntityList = tPPositionService.findMemberPositions(retUser.getId());
				
				List<TSUser> studentList = userService.findStudentListByUser(
						retUser.getId(), "12");
				
				if(studentList.size() != 0){
					for(int i=0; i<studentList.size(); i++){
						List<String> retli = new ArrayList<String>(); 
						List<ExamResultEntity> li = examResultService.queryExamDate(studentList.get(i).getId());
						for(int t=0;t<li.size();t++){
							retli.add(li.get(t).getSchoolYear()+"_"+li.get(t).getTerm());
						}
						studentList.get(i).setExamDate(retli);
						studentList.get(i).setHeadPortraitUrl(BaseConfigMgr.getWebVisitUrl()+studentList.get(i).getHeadPortraitUrl());
					}
				}
				
				 
				retUser.setTSDepart(null);
				Map<String, Object> resultData = new HashMap<String, Object>();
				retUser.setHeadPortraitUrl(BaseConfigMgr.getWebVisitUrl()+retUser.getHeadPortraitUrl());
				
				
				resultData.put("user", retUser);
				/*resultData.put("familys", findFamily);*/
				
				resultData.put("studentList", studentList);
				
				return new AndroidResult(AndroidResult.SUCCESS_CODE,
						AndroidResult.SUCCESS_MASSEGE, resultData)
						.toAndroidResultString();
			} else {
				return AndroidResult.defaultFailure("用户名不存在或用户名密码错误")
						.toAndroidResultString();
			}
		} catch (Exception e) {
			e.printStackTrace();
			return AndroidResult.defaultFailure(e.getMessage())
					.toAndroidResultString();
		}
	}

	/**
	 * 第三方登录
	 * 
	 * @param openId
	 * @return
	 */
	@RequestMapping(params = "thirdpartLogin")
	@ResponseBody
	public String thirdpartLogin() {
		String openId = StringUtils.getMapDataStr(reqBodyMap, "openId");
		String thirdpartyLoginType = StringUtils.getMapDataStr(reqBodyMap, "thirdpartyLoginType");

		try {
			/*EasemobRestAPIFactory factory = ClientContext.getInstance()
					.init(ClientContext.INIT_FROM_PROPERTIES).getAPIFactory();

			IMUserAPI iMUserAPI = (IMUserAPI) factory
					.newInstance(EasemobRestAPIFactory.USER_CLASS);
			final String newpassword = "123456";
			BodyWrapper bw = new BodyWrapper() {
				@Override
				public Boolean validate() {
					return org.apache.commons.lang3.StringUtils
							.isNotBlank(newpassword);
				}
		
				@Override
				public ContainerNode<?> getBody() {
					return JsonNodeFactory.instance.objectNode().put(
							"newpassword", newpassword);
				}
			};
			List<TSUser> userList = userService.getList(TSUser.class);
			for(int i=0;userList != null && i<userList.size();i++){
				userService.pwdInit(userList.get(i), "123456");
				ResponseWrapper r = (ResponseWrapper) iMUserAPI
						.modifyIMUserPasswordWithAdminToken(userList.get(i).getUserName(), bw);
			}
		*/  
			TSUser tsUser = userService.checkUserExsits(openId,thirdpartyLoginType);
			Map dataMap = new HashMap();
			if (tsUser == null) {
				dataMap.put("isBinding", "N");
				return new AndroidResult(AndroidResult.SUCCESS_CODE,
						AndroidResult.SUCCESS_MASSEGE, dataMap)
						.toAndroidResultString();
			} else {
				dataMap.put("isBinding", "Y");
				dataMap.put("userName", tsUser.getUserName());
				//dataMap.put("password",  PasswordUtil.decrypt("123456", tsUser.getUserName(), PasswordUtil.getStaticSalt()));
				return new AndroidResult(AndroidResult.SUCCESS_CODE,
						AndroidResult.SUCCESS_MASSEGE, dataMap)
						.toAndroidResultString();
			}
		} catch (Exception e) {
			e.printStackTrace();
			return AndroidResult.defaultFailure(e.getMessage())
					.toAndroidResultString();
		}
	}

	/**
	 * 找回密码
	 * 
	 * @param openId
	 * @return
	 */
	@RequestMapping(params = "modifyPasswordForForget")
	@ResponseBody
	public String modifyPasswordForForget() {
 
		String loginName = StringUtils.getMapDataStr(reqBodyMap, "loginName");
		String password = StringUtils.getMapDataStr(reqBodyMap, "password");
		String TelValidateCode = StringUtils.getMapDataStr(reqBodyMap,
				"telValidateCode");

		try {
			TSPhoneValidateCode tSPhoneValidateCode = new TSPhoneValidateCode();
			tSPhoneValidateCode.setMobilephone(loginName);
			tSPhoneValidateCode.setValidateCode(TelValidateCode);
			PhoneValidateCodeCheckResult checkPhoneValidateCode = phoneValidateCodeService
					.checkPhoneValidateCode(tSPhoneValidateCode);

			if (checkPhoneValidateCode == PhoneValidateCodeCheckResult.SUCCESS) {
				
				/*final String newpassword = PasswordUtil.encrypt(loginName,
						password, PasswordUtil.getStaticSalt());*/
				final String newpassword = password;//临时改成不加密

				BodyWrapper bw = new BodyWrapper() {
					@Override
					public Boolean validate() {
						return org.apache.commons.lang3.StringUtils
								.isNotBlank(newpassword);
					}

					@Override
					public ContainerNode<?> getBody() {
						return JsonNodeFactory.instance.objectNode().put(
								"newpassword", newpassword);
					}
				};
				
				ResponseWrapper r = (ResponseWrapper) iMUserAPI
						.modifyIMUserPasswordWithAdminToken(loginName, bw);
				if (r != null && r.getResponseStatus() == 200) {
					TSUser user = new TSUser();
					user.setUserName(loginName);
					user.setPassword(password);
					user.setTSDepart(null);
					user.setStatus(Globals.User_Normal);
					userService.pwdInit(user, user.getPassword());
					return AndroidResult.defaultSuccess("密码修改成功")
							.toAndroidResultString();
				} else {
					return AndroidResult.defaultFailure("修改环信密码失败 ")
							.toAndroidResultString();
				}
			} else {
				return AndroidResult.defaultFailure("验证码错误")
						.toAndroidResultString();
			}
		} catch (Exception e) {
			e.printStackTrace();
			return AndroidResult.defaultFailure(e.getMessage())
					.toAndroidResultString();
		}
	}
	
	public void dowithModifyPassword(){
	
	}
	public static void main(String[] args) {
		
	}

}
