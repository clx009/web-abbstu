package com.hnykl.bp.web.system.controller;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hnykl.bp.android.common.AndroidResult;
import com.hnykl.bp.base.configuration.BaseConfigMgr;
import com.hnykl.bp.base.core.constant.Globals;
import com.hnykl.bp.base.core.util.PasswordUtil;
import com.hnykl.bp.base.core.util.UUIDGenerator;
import com.hnykl.bp.base.tool.character.StringUtils;
import com.hnykl.bp.base.tool.date.DateUtil;
import com.hnykl.bp.easemob.server.api.ChatGroupAPI;
import com.hnykl.bp.easemob.server.api.IMUserAPI;
import com.hnykl.bp.easemob.server.comm.ClientContext;
import com.hnykl.bp.easemob.server.comm.EasemobRestAPIFactory;
import com.hnykl.bp.easemob.server.comm.body.ChatGroupBody;
import com.hnykl.bp.easemob.server.comm.body.IMUserBody;
import com.hnykl.bp.easemob.server.comm.wrapper.BodyWrapper;
import com.hnykl.bp.easemob.server.comm.wrapper.ResponseWrapper;
import com.hnykl.bp.web.AbstractAndroidController;
import com.hnykl.bp.web.family.entity.TFFamilyEntity;
import com.hnykl.bp.web.family.service.TFFamilyServiceI;
import com.hnykl.bp.web.system.enums.PhoneValidateCodeCheckResult;
import com.hnykl.bp.web.system.pojo.base.TSPhoneValidateCode;
import com.hnykl.bp.web.system.pojo.base.TSUser;
import com.hnykl.bp.web.system.service.PhoneValidateCodeService;
import com.hnykl.bp.web.system.service.SystemService;
import com.hnykl.bp.web.system.service.UserService;

/**
 * 登陆初始化控制器
 * 
 * @author
 * 
 */
@Controller
@RequestMapping("/registerController")
public class RegisterController extends AbstractAndroidController {

	@SuppressWarnings("unused")
	private Logger log = Logger.getLogger(RegisterController.class);

	EasemobRestAPIFactory factory = ClientContext.getInstance()
			.init(ClientContext.INIT_FROM_PROPERTIES).getAPIFactory();

	IMUserAPI iMUserAPI = (IMUserAPI) factory
			.newInstance(EasemobRestAPIFactory.USER_CLASS);

	private SystemService systemService;
	private PhoneValidateCodeService phoneValidateCodeService;

	@Autowired
	private TFFamilyServiceI tFFamilyService;

	@Autowired
	private UserService userService;

	@Autowired
	public void setSystemService(SystemService systemService) {
		this.systemService = systemService;
	}

	@Autowired
	public void setPhoneValidateCodeService(
			PhoneValidateCodeService phoneValidateCodeService) {
		this.phoneValidateCodeService = phoneValidateCodeService;
	}

	@RequestMapping(params = "userAgreement")
	@ResponseBody
	public String userAgreement(HttpServletResponse response) throws IOException {
		Map<String, String> resultMap = new HashMap<String, String>();
		String userAgreementUrl = BaseConfigMgr.getWebVisitUrl() + "index.html";
		resultMap.put("userAgreement", userAgreementUrl);
		response.sendRedirect(userAgreementUrl);
		return (new AndroidResult(replyCode, replyMsg, resultMap))
				.toAndroidResultString();

	}
	
	
	@RequestMapping(params = "register")
	@ResponseBody
	public String register(HttpServletRequest request) {
		String userName = StringUtils.getMapDataStr(reqBodyMap, "userName");
		String password = StringUtils.getMapDataStr(reqBodyMap, "password");
		String nickname = StringUtils.getMapDataStr(reqBodyMap, "nickname");

//		String phoneValidateCode = StringUtils.getMapDataStr(reqBodyMap,
//				"phoneValidateCode");
		String type = StringUtils.getMapDataStr(reqBodyMap, "type");

		// 验证手机验证码
/*		TSPhoneValidateCode tSPhoneValidateCode = new TSPhoneValidateCode();
		tSPhoneValidateCode.setMobilephone(userName);
		tSPhoneValidateCode.setValidateCode(phoneValidateCode);*/

//		boolean checkUserExsits = systemService.checkUserExsits(userName);
//		if (checkUserExsits) {// 用户名不存在
//			PhoneValidateCodeCheckResult checkPhoneValidateCode = phoneValidateCodeService
//					.checkPhoneValidateCode(tSPhoneValidateCode);
			Date nowDate = new Date();
//			if (checkPhoneValidateCode == PhoneValidateCodeCheckResult.SUCCESS) {
				// if (true) {
				try {
					TSUser user = new TSUser();
					user.setUserName(userName);
					user.setPassword(PasswordUtil.encrypt(password,
							user.getUserName(), PasswordUtil.getStaticSalt()));
					user.setTSDepart(null);
					user.setStatus(Globals.User_Normal);
					user.setType(type);
					user.setNickname(nickname);
					user.setCreateTime(nowDate);
					user.setModifyTime(nowDate);
					user.setMobilePhone(userName);
					user.setHeadPortraitUrl(BaseConfigMgr
							.getUserRegisterDefaultHeadPortraitPath());

					String userId = (String) systemService.save(user);
					user.setId(userId);
					BodyWrapper userBody = new IMUserBody(userName, password
					/*
					 * PasswordUtil.encrypt(userName, password,
					 * PasswordUtil.getStaticSalt())
					 */, nickname);// 临时改成不加密
					ResponseWrapper responseWraper = (ResponseWrapper) iMUserAPI
							.createNewIMUserSingle(userBody);
					System.out
							.println("====>responseWraper.getResponseStatus():"
									+ responseWraper.getResponseStatus());
					if (responseWraper != null
							&& responseWraper.getResponseStatus() == 200) { // 环信用户创建成功
						if ("11".equals(type)) {// 家长注册，默认增加一个家人圈
							ChatGroupAPI chatgroup = (ChatGroupAPI) factory
									.newInstance(EasemobRestAPIFactory.CHATGROUP_CLASS);
							ChatGroupBody chatGroupBody = new ChatGroupBody(
									nickname, nickname, false, 5L, false,
									userName, new String[] { userName });
							ResponseWrapper responseWraper2 = (ResponseWrapper) chatgroup
									.createChatGroup(chatGroupBody);
							if (responseWraper2 != null
									&& responseWraper2.getResponseStatus() == 200) {// 环信群组创建成功
								String groupId = "";
								@SuppressWarnings("unchecked")
								Map<String, Object> responseBodyMap = JSONObject
										.fromObject(responseWraper2
												.getResponseBody().toString());
								if (!responseBodyMap.isEmpty()) {
									@SuppressWarnings("unchecked")
									Map<String, String> responseDataMap = JSONObject
											.fromObject(responseBodyMap
													.get("data"));
									if (!responseDataMap.isEmpty()) {
										groupId = responseDataMap
												.get("groupid");
									}
								}

								if (!"".equals(groupId)) {
									// 创建默认群组
									TFFamilyEntity entity = new TFFamilyEntity();
									entity.setCreatorId(userId);
									entity.setName(nickname);
									entity.setCreateTime(DateUtil
											.getSysTimeStamp());
									entity.setGroupId(groupId);
									tFFamilyService.createDefaultFamily(entity);
									return AndroidResult.defaultSuccess("注册成功")
											.toAndroidResultString();
								} else {
									return AndroidResult.defaultFailure(
											"环信创建群组失败").toAndroidResultString();
								}
							} else {
								return new AndroidResult("2", "未创建默认群组")
										.toAndroidResultString();
							}
						} else {
							return AndroidResult.defaultSuccess()
									.toAndroidResultString();
						}
					} else {
						systemService.delete(user);
						return AndroidResult.defaultFailure("创建环信用户失败")
								.toAndroidResultString();
					}

				} catch (Exception e) {
					e.printStackTrace();
					return AndroidResult.defaultFailure(e.getMessage())
							.toAndroidResultString();
				}
/*			} else {
				return AndroidResult.defaultFailure(
						checkPhoneValidateCode.getMessage())
						.toAndroidResultString();
			}*/
/*		} else {// 用户名存在
			return AndroidResult.defaultFailure("手机号已注册，不能重复注册")
					.toAndroidResultString();
		}*/

	}

	/**
	 * 检查用户名是否存在
	 * 
	 * @return
	 */
	@RequestMapping(params = "isExsitsLoginName")
	@ResponseBody
	public String isExsitsLoginName() {
		String loginName = StringUtils.getMapDataStr(reqBodyMap, "loginName");
		Map<String, String> resultMap = new HashMap<String, String>();
		try {
			boolean checkUserExsits = systemService.checkUserExsits(loginName);
			if (checkUserExsits) {
				resultMap.put("isExsits", "N");
				return new AndroidResult(AndroidResult.SUCCESS_CODE,
						AndroidResult.SUCCESS_CODE, resultMap)
						.toAndroidResultString();
			} else {
				resultMap.put("isExsits", "Y");
				return new AndroidResult(AndroidResult.SUCCESS_CODE,
						AndroidResult.SUCCESS_CODE, resultMap)
						.toAndroidResultString();
			}

		} catch (Exception e) {
			e.printStackTrace();
			return AndroidResult.defaultFailure(e.getMessage())
					.toAndroidResultString();
		}
	}

	/**
	 * 第三方注册
	 * 
	 * @param OpenId
	 *            第三方生成openId
	 * @param PhoneNumber
	 *            绑定的手机号
	 * @param telValidateCode
	 *            短信验证码
	 * */
	@RequestMapping(params = "thirdpartRegister")
	@ResponseBody
	public String thirdpartRegister() {
		String openId = StringUtils.getMapDataStr(reqBodyMap, "openId");
		String thirdpartyLoginType = StringUtils.getMapDataStr(reqBodyMap,
				"thirdpartyLoginType");
		String phoneNumber = StringUtils.getMapDataStr(reqBodyMap,
				"phoneNumber");
		String password = StringUtils.getMapDataStr(reqBodyMap, "password");
		try {
			TSUser user = new TSUser();
			user.setUserName(phoneNumber);
			user.setPassword(password);
			TSUser retUser = userService.checkUserExits(user);
			if (retUser != null) {
				userService.updateOpenId(retUser.getId(), openId,
						thirdpartyLoginType);
				Map resultDataMap = new HashMap();
				resultDataMap.put("phoneNumber", phoneNumber);
				resultDataMap.put("password", password);
				return (new AndroidResult(replyCode, replyMsg, resultDataMap))
						.toAndroidResultString();
			} else {
				return AndroidResult.defaultFailure("用户名不存在或用户名密码错误")
						.toAndroidResultString();
			}
		} catch (Exception e) {
			e.printStackTrace();
			return AndroidResult.defaultFailure(e.getMessage())
					.toAndroidResultString();
		}
	}

	/**
	 * 第三方注册
	 * 
	 * @param OpenId
	 *            第三方生成openId
	 * @param PhoneNumber
	 *            绑定的手机号
	 * @param telValidateCode
	 *            短信验证码
	 * */
	@RequestMapping(params = "anonymousRegister")
	@ResponseBody
	public String anonymousRegister() {
		String openId = StringUtils.getMapDataStr(reqBodyMap, "openId");
		String thirdpartyLoginType = StringUtils.getMapDataStr(reqBodyMap,
				"thirdpartyLoginType");
		String nickname = StringUtils.getMapDataStr(reqBodyMap, "nickname");
		String type = StringUtils.getMapDataStr(reqBodyMap, "type");
		String headPortraitUrl = StringUtils.getMapDataStr(reqBodyMap, "headPortraitUrl");
		TSUser user = new TSUser();
		
/*		openId = "oQraqwNiJpgsF1IN8ZN6YDHSQiVs";
		thirdpartyLoginType = "2";
		nickname = "浅唱";
		type = "11";
		headPortraitUrl = "11";*/
		
		try {
			
			TSUser retUser = userService.checkUserExsits(openId,
					thirdpartyLoginType);
			if (retUser == null) { 
				if("1".equals(thirdpartyLoginType))
					user.setOpenId(openId);
				else
					user.setWxOpenId(openId);
				user.setNickname(nickname);
				user.setThirdpartyLoginType(thirdpartyLoginType);
				user.setUserName(UUIDGenerator.generate());
				user.setType(type);
				user.setHeadPortraitUrl(headPortraitUrl);
				user.setStatus(Globals.User_Normal);
				user.setCreateTime(new Date());
				user.setModifyTime(new Date());
				user.setTSDepart(null);
				
				String userId = (String) systemService.save(user);
				user.setId(userId);
				BodyWrapper userBody = new IMUserBody(user.getUserName(),
						openId
						/*
						 * PasswordUtil.encrypt(userName, password,
						 * PasswordUtil.getStaticSalt())
						 */, nickname);// 临时改成不加密
				ResponseWrapper responseWraper = (ResponseWrapper) iMUserAPI
						.createNewIMUserSingle(userBody);
				System.out.println("====>responseWraper.getResponseStatus():"
						+ responseWraper.getResponseStatus());
				if (responseWraper != null
						&& responseWraper.getResponseStatus() == 200) { // 环信用户创建成功
					if ("11".equals(type)) {// 家长注册，默认增加一个家人圈
						ChatGroupAPI chatgroup = (ChatGroupAPI) factory
								.newInstance(EasemobRestAPIFactory.CHATGROUP_CLASS);
						ChatGroupBody chatGroupBody = new ChatGroupBody(
								nickname, nickname, false, 5L, false,
								user.getUserName(),
								new String[] { user.getUserName() });
						ResponseWrapper responseWraper2 = (ResponseWrapper) chatgroup
								.createChatGroup(chatGroupBody);
						if (responseWraper2 != null
								&& responseWraper2.getResponseStatus() == 200) {// 环信群组创建成功
							String groupId = "";
							@SuppressWarnings("unchecked")
							Map<String, Object> responseBodyMap = JSONObject
									.fromObject(responseWraper2
											.getResponseBody().toString());
							if (!responseBodyMap.isEmpty()) {
								@SuppressWarnings("unchecked")
								Map<String, String> responseDataMap = JSONObject
										.fromObject(responseBodyMap.get("data"));
								if (!responseDataMap.isEmpty()) {
									groupId = responseDataMap.get("groupid");
								}
							}

							if (!"".equals(groupId)) {
								// 创建默认群组
								TFFamilyEntity entity = new TFFamilyEntity();
								entity.setCreatorId(userId);
								entity.setName(nickname);
								entity.setCreateTime(DateUtil.getSysTimeStamp());
								entity.setGroupId(groupId);
								tFFamilyService.createDefaultFamily(entity);
							} else {
								return AndroidResult.defaultFailure("环信创建群组失败")
										.toAndroidResultString();
							}
						} else {
							return new AndroidResult("2", "未创建默认群组")
									.toAndroidResultString();
						}
					} else {
						return AndroidResult.defaultSuccess()
								.toAndroidResultString();
					}
				} else {
					systemService.delete(user);
					return AndroidResult.defaultFailure("创建环信用户失败")
							.toAndroidResultString();
				}
			}else{
				user.setId(retUser.getId());
			}
			
			TSUser loginUser = userService.checkUserExsits(openId,
					thirdpartyLoginType);

			// 直接登录
			List<TFFamilyEntity> findFamily = tFFamilyService
					.findFamily(loginUser.getId()); 
			Map<String, Object> resultData = new HashMap<String, Object>();
			user.setHeadPortraitUrl(BaseConfigMgr.getWebVisitUrl()
					+ user.getHeadPortraitUrl());
			resultData.put("user", loginUser);
			resultData.put("familys", findFamily);

			return new AndroidResult(AndroidResult.SUCCESS_CODE,
					AndroidResult.SUCCESS_MASSEGE, resultData)
					.toAndroidResultString();
		} catch (Exception e) {
			e.printStackTrace();
			return AndroidResult.defaultFailure(e.getMessage())
					.toAndroidResultString();
		}
	}

}
