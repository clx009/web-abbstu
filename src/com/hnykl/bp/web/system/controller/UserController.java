package com.hnykl.bp.web.system.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import jersey.repackaged.com.google.common.collect.Lists;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.druid.support.json.JSONUtils;
import com.hnykl.bp.android.common.AndroidResult;
import com.hnykl.bp.base.configuration.BaseConfigMgr;
import com.hnykl.bp.base.tool.bean.BeanUtils;
import com.hnykl.bp.base.tool.character.StringUtils;
import com.hnykl.bp.easemob.server.api.IMUserAPI;
import com.hnykl.bp.easemob.server.comm.ClientContext;
import com.hnykl.bp.easemob.server.comm.EasemobRestAPIFactory;
import com.hnykl.bp.easemob.server.comm.body.IMUserBody;
import com.hnykl.bp.easemob.server.comm.wrapper.BodyWrapper;
import com.hnykl.bp.easemob.server.comm.wrapper.ResponseWrapper;
import com.hnykl.bp.web.AbstractAndroidController;
import com.hnykl.bp.web.ca.entity.TCaApplyEntity;
import com.hnykl.bp.web.ca.service.TCaApplyServiceI;
import com.hnykl.bp.web.system.enums.PhoneValidateCodeCheckResult;
import com.hnykl.bp.web.system.pojo.base.TSPhoneValidateCode;
import com.hnykl.bp.web.system.pojo.base.TSUser;
import com.hnykl.bp.web.system.service.PhoneValidateCodeService;
import com.hnykl.bp.web.system.service.UserService;
import com.hnykl.bp.web.visit.entity.OptionEntity;
import com.hnykl.bp.web.visit.entity.VisitEntity;
import com.hnykl.bp.web.visit.entity.VisitOptionEntity;
import com.hnykl.bp.web.visit.service.VisitServiceI;

/**
 * @ClassName: UserController
 * @Description: TODO(用户管理处理类)
 * @author
 */
@Controller
@RequestMapping("/userController")
public class UserController extends AbstractAndroidController {
	/**
	 * Logger for this class
	 */
	@SuppressWarnings("unused")
	private static final Logger logger = Logger.getLogger(UserController.class);

	private UserService userService;

	private PhoneValidateCodeService phoneValidateCodeService;

	@Autowired
	private VisitServiceI visitService;
	
	@Autowired
	private TCaApplyServiceI tCaApplyService;

	EasemobRestAPIFactory factory = ClientContext.getInstance()
			.init(ClientContext.INIT_FROM_PROPERTIES).getAPIFactory();

	IMUserAPI iMUserAPI = (IMUserAPI) factory
			.newInstance(EasemobRestAPIFactory.USER_CLASS);

	@Autowired
	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	@Autowired
	public void setPhoneValidateCodeService(
			PhoneValidateCodeService phoneValidateCodeService) {
		this.phoneValidateCodeService = phoneValidateCodeService;
	}

	/**
	 * 修改个人资料
	 * 
	 * @param username
	 *            用户名
	 * @param timeZone
	 *            时区
	 * @param nickname
	 *            昵称
	 * @param messageAlert
	 *            消息提醒
	 * @param realName
	 *            姓名
	 * */
	@RequestMapping(params = "changeProfile")
	@ResponseBody
	public String changeProfile() {

		String username = StringUtils.getMapDataStr(reqBodyMap, "username");
		String timeZone = StringUtils.getMapDataStr(reqBodyMap, "timeZone");
		String nickname = StringUtils.getMapDataStr(reqBodyMap, "nickname");
		String messageAlert = StringUtils.getMapDataStr(reqBodyMap,
				"messageAlert");
		String realName = StringUtils.getMapDataStr(reqBodyMap, "realName");

		try {
			TSUser user = new TSUser();
			user.setUserName(username);
			user.setTimeZone(timeZone);
			user.setNickname(nickname);
			user.setMessageAlert(messageAlert);
			user.setRealName(realName);
			userService.changeProfile(user);
			return AndroidResult.defaultSuccess().toAndroidResultString();
		} catch (Exception e) {
			
			
			e.printStackTrace();
			return AndroidResult.defaultFailure(e.getMessage())
					.toAndroidResultString();
		}
	}

	/**
	 * 修改密码
	 * */
	@RequestMapping(params = "changePassword")
	@ResponseBody
	public String changePassword() {

		String userId = StringUtils.getMapDataStr(reqBodyMap, "userId");
		String username = StringUtils.getMapDataStr(reqBodyMap, "username");
		String currentPassword = StringUtils.getMapDataStr(reqBodyMap,
				"currentPassword");
		String newPassword = StringUtils.getMapDataStr(reqBodyMap,
				"newPassword");
		try {

			TSUser user = new TSUser();
			user.setUserName(username);
			user.setId(userId);
			user.setPassword(currentPassword);

			TSUser checkUserExits = userService.checkUserExits(user);
			checkUserExits = new TSUser();
			if (checkUserExits != null) {
				BodyWrapper userBody = new IMUserBody(userId, newPassword, "");
				ResponseWrapper r = (ResponseWrapper) iMUserAPI
						.modifyIMUserPasswordWithAdminToken(userId, userBody);
				if (r != null && r.getResponseStatus() == 200) {
					userService.pwdInitByUserId(user, newPassword);
					return AndroidResult.defaultSuccess("密码已修改，请重新登录")
							.toAndroidResultString();
				} else {
					return AndroidResult.defaultFailure("环信接口调用失败 ")
							.toAndroidResultString();
				}
			} else {
				return AndroidResult.defaultSuccess("密码验证失败")
						.toAndroidResultString();
			}

		} catch (Exception e) {
			e.printStackTrace();
			return AndroidResult.defaultFailure(e.getMessage())
					.toAndroidResultString();
		}
	}

	/**
	 * 修改手机号
	 * */
	@RequestMapping(params = "changePhoneNumber")
	@ResponseBody
	public String changePhoneNumber() {
		String username = StringUtils.getMapDataStr(reqBodyMap, "username");
		String newPhoneNumber = StringUtils.getMapDataStr(reqBodyMap,
				"newPhoneNumber");
		String phoneValidateCode = StringUtils.getMapDataStr(reqBodyMap,
				"phoneValidateCode");

		// 验证手机验证码
		TSPhoneValidateCode tSPhoneValidateCode = new TSPhoneValidateCode();
		tSPhoneValidateCode.setMobilephone(username);
		tSPhoneValidateCode.setValidateCode(phoneValidateCode);

		try {
			PhoneValidateCodeCheckResult checkPhoneValidateCode = phoneValidateCodeService
					.checkPhoneValidateCode(tSPhoneValidateCode);
			if (checkPhoneValidateCode == PhoneValidateCodeCheckResult.SUCCESS) {
				TSUser user = new TSUser();
				user.setUserName(username);
				userService.changeUserName(user, newPhoneNumber);
				return AndroidResult.defaultSuccess().toAndroidResultString();
			} else {
				return AndroidResult.defaultFailure(
						checkPhoneValidateCode.getMessage())
						.toAndroidResultString();
			}
		} catch (Exception e) {
			e.printStackTrace();
			return AndroidResult.defaultFailure(e.getMessage())
					.toAndroidResultString();
		}
	}

	/**
	 * 修改电子邮箱
	 * */
	@RequestMapping(params = "changeEmail")
	@ResponseBody
	public String changeEmail() {
		String username = StringUtils.getMapDataStr(reqBodyMap, "username");
		String email = StringUtils.getMapDataStr(reqBodyMap, "email");
		String phoneValidateCode = StringUtils.getMapDataStr(reqBodyMap,
				"phoneValidateCode");

		// 验证手机验证码
		TSPhoneValidateCode tSPhoneValidateCode = new TSPhoneValidateCode();
		tSPhoneValidateCode.setMobilephone(username);
		tSPhoneValidateCode.setValidateCode(phoneValidateCode);

		try {
			PhoneValidateCodeCheckResult checkPhoneValidateCode = phoneValidateCodeService
					.checkPhoneValidateCode(tSPhoneValidateCode);
			if (checkPhoneValidateCode == PhoneValidateCodeCheckResult.SUCCESS) {
				TSUser user = new TSUser();
				user.setUserName(username);
				user.setEmail(email);
				userService.changeProfile(user);
				return AndroidResult.defaultSuccess().toAndroidResultString();
			} else {
				return AndroidResult.defaultFailure(
						checkPhoneValidateCode.getMessage())
						.toAndroidResultString();
			}
		} catch (Exception e) {
			e.printStackTrace();
			return AndroidResult.defaultFailure(e.getMessage())
					.toAndroidResultString();
		}
	}

	public static void main(String[] args) {
		Calendar cal = Calendar.getInstance();
		System.out.println(System.currentTimeMillis());
	}

	@RequestMapping(params = "changeHeadPortrait")
	@ResponseBody
	public String changeHeadPortrait(HttpServletRequest request,
			@Valid String username, @RequestParam MultipartFile file) {
		String uploadPath = BaseConfigMgr.getUploadpath();

		String fileName = file.getOriginalFilename();
		String suffix = fileName.substring(fileName.lastIndexOf("."));

		String savePath = uploadPath + "/headPortrait/" + username + "/"
				+ username + System.currentTimeMillis();

		/*
		 * String path = request.getSession().getServletContext()
		 * .getRealPath(savePath);
		 */
		String path = BaseConfigMgr.getWebRootPath();

		File targetFile = new File(path + "/" + savePath + suffix);
		if (!targetFile.exists()) {
			targetFile.mkdirs();
		}
		try {
			file.transferTo(targetFile);

			TSUser user = new TSUser();
			user.setUserName(username);

			user.setHeadPortraitUrl(savePath + suffix);

			userService.changeProfile(user);
			Map dataMap = new HashMap();
			dataMap.put("headPortraitUrl", BaseConfigMgr.getWebVisitUrl() + "/"
					+ user.getHeadPortraitUrl());
			return new AndroidResult(replyCode, replyMsg, dataMap)
					.toAndroidResultString();

		} catch (Exception e) {
			e.printStackTrace();
			return AndroidResult.defaultFailure(e.getMessage())
					.toAndroidResultString();
		}
	}

	/**
	 * 
	 * @return
	 */
	@RequestMapping(params = "findStudentListByUser")
	@ResponseBody
	public String findStudentListByUser() {
		String userId = StringUtils.getMapDataStr(reqBodyMap, "userId");// 当前登录用户id
		try {
			Map<String, Object> resultData = new HashMap<String, Object>();
			List<TSUser> studentList = userService.findStudentListByUser(
					userId, "12");
			resultData.put("studentList", studentList);
			return new AndroidResult(AndroidResult.SUCCESS_CODE,
					AndroidResult.SUCCESS_MASSEGE, resultData)
					.toAndroidResultString();
		} catch (Exception e) {
			e.printStackTrace();
			return AndroidResult.defaultFailure(e.getMessage())
					.toAndroidResultString();
		}
	}
	
	/**
	 * 
	 * @return 查询用户信息
	 */
	@RequestMapping(params = "findUserInfo")
	@ResponseBody
	public String findUserInfo() {
		String userId = StringUtils.getMapDataStr(reqBodyMap, "userId");// 当前登录用户id
		try {
			Map<String, Object> resultData = new HashMap<String, Object>();
			TSUser studentList = userService.findUserInfo(userId);
			resultData.put("userinfo", studentList);
			return new AndroidResult(AndroidResult.SUCCESS_CODE,
					AndroidResult.SUCCESS_MASSEGE, resultData)
					.toAndroidResultString();
		} catch (Exception e) {
			e.printStackTrace();
			return AndroidResult.defaultFailure(e.getMessage())
					.toAndroidResultString();
		}
	}


	/**
	 * 
	 * @return
	 */
	@RequestMapping(params = "findUserRealTimeInfo")
	@ResponseBody
	public String findUserRealTimeInfo() {
		String userId = StringUtils.getMapDataStr(reqBodyMap, "userId");// 当前登录用户id  

		try { 

			
			List<VisitEntity> vl1 = visitService.findVisitListByUserIds(userId.split(","),"1");
			List<VisitEntity> vl2 = visitService.findVisitListByUserIds(userId.split(","),"2");
			List<VisitEntity> vl3 = visitService.findVisitListByUserIds(userId.split(","),"3");
			
			List<TCaApplyEntity> tl1 = tCaApplyService.findTCaApplyListByUserIds(userId.split(","),"1"); 
			List<TCaApplyEntity> tl2 = tCaApplyService.findTCaApplyListByUserIds(userId.split(","),"2"); 
			List<TCaApplyEntity> tl3 = tCaApplyService.findTCaApplyListByUserIds(userId.split(","),"3"); 
			List<TCaApplyEntity> tl4 = tCaApplyService.findTCaApplyListByUserIds(userId.split(","),"4"); 
			List<TCaApplyEntity> tl5 = tCaApplyService.findTCaApplyListByUserIds(userId.split(","),"5"); 

			Map<String, Object> resultData = new HashMap<String, Object>();
			resultData.put("vl1", convertList(vl1));
			resultData.put("vl2", convertList(vl2));
			resultData.put("vl3", convertList(vl3));
			resultData.put("tl1", convertTCaApplyList(tl1)); 
			resultData.put("tl2", convertTCaApplyList(tl2)); 
			resultData.put("tl3", convertTCaApplyList(tl3)); 
			resultData.put("tl4", convertTCaApplyList(tl4)); 
			resultData.put("tl5", convertTCaApplyList(tl5)); 
			return new AndroidResult(AndroidResult.SUCCESS_CODE,
					AndroidResult.SUCCESS_MASSEGE, resultData)
					.toAndroidResultString();
		} catch (Exception e) {
			e.printStackTrace();
			return AndroidResult.defaultFailure(e.getMessage())
					.toAndroidResultString();
		}
	}

	private List<Map> convertList(List<VisitEntity> visitList) {
		Map itemMap = null;
		List<Map> visitLst = new ArrayList<Map>();
		for (int i = 0; visitList != null && i < visitList.size(); i++) {
			VisitEntity visitEntity = visitList.get(i);
			itemMap = BeanUtils.getAllFieldValues(visitEntity);
			itemMap.put("type", visitEntity.getType());
			itemMap.remove("creator");
			itemMap.remove("visitOptions");
			Map creatorMap = BeanUtils.getAllFieldValues(visitEntity.getCreator()); 
			itemMap.put("creator", creatorMap);
			
			List<Map> optList = Lists.newArrayList();
			for(VisitOptionEntity voe:visitEntity.getVisitOptions()){
				OptionEntity oe = voe.getOption();
				Map optMap = BeanUtils.getAllFieldValues(oe);
				optList.add(optMap);
			}
			
			visitLst.add(itemMap);
		}
		return visitLst;
	}
	
	private List<Map> convertTCaApplyList(List<TCaApplyEntity> list) {
		Map itemMap = null;
		List<Map> lst = new ArrayList<Map>();
		for (int i = 0; list != null && i < list.size(); i++) {
			TCaApplyEntity entity = list.get(i);
			itemMap = BeanUtils.getAllFieldValues(entity);
			itemMap.remove("subject");
			itemMap.remove("attachments");
			itemMap.remove("applicant");
			lst.add(itemMap);
		}
		return lst;
	}

}