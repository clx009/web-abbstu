package com.hnykl.bp.web.system.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hnykl.bp.android.common.AndroidResult;
import com.hnykl.bp.base.configuration.BaseConfigMgr;
import com.hnykl.bp.base.tool.character.StringUtils;
import com.hnykl.bp.base.tool.date.DateUtil;
import com.hnykl.bp.base.tool.sms.SmsTemplate;
import com.hnykl.bp.base.tool.validateCode.ValidateCodeUtils;
import com.hnykl.bp.web.AbstractAndroidController;
import com.hnykl.bp.web.system.enums.PhoneValidateCodeCheckResult;
import com.hnykl.bp.web.system.pojo.base.TSPhoneValidateCode;
import com.hnykl.bp.web.system.service.PhoneValidateCodeService;
import com.hnykl.bp.web.system.service.SystemService;

/**
 * 登陆初始化控制器
 * 
 * @author
 * 
 */
@Controller
@RequestMapping("/phoneValidateCodeController")
public class PhoneValidateCodeController extends AbstractAndroidController {
	@SuppressWarnings("unused")
	private Logger log = Logger.getLogger(PhoneValidateCodeController.class);
	private PhoneValidateCodeService phoneValidateCodeService;
	private SystemService systemService;
	
	@Autowired
	public void setPhoneValidateCodeService(
			PhoneValidateCodeService phoneValidateCodeService) {
		this.phoneValidateCodeService = phoneValidateCodeService;
	}

	@Autowired
	public void setSystemService(SystemService systemService) {
		this.systemService = systemService;
	}
	
	
	// 验证手机验证码
	@RequestMapping(params = "verifyTelValidateCode")
	@ResponseBody
	public String verifyTelValidateCode(HttpServletRequest request) {
		String userName = StringUtils.getMapDataStr(reqBodyMap, "userName");
		String phoneValidateCode = StringUtils.getMapDataStr(reqBodyMap,
				"phoneValidateCode");
		
		TSPhoneValidateCode tSPhoneValidateCode = new TSPhoneValidateCode();
		tSPhoneValidateCode.setMobilephone(userName);
		tSPhoneValidateCode.setValidateCode(phoneValidateCode);
		PhoneValidateCodeCheckResult checkPhoneValidateCode = phoneValidateCodeService
				.checkPhoneValidateCode(tSPhoneValidateCode);
		if (checkPhoneValidateCode == PhoneValidateCodeCheckResult.SUCCESS) {
			return AndroidResult.defaultSuccess(
					checkPhoneValidateCode.getMessage())
					.toAndroidResultString();
		}else{
			return AndroidResult.defaultFailure(
					checkPhoneValidateCode.getMessage())
					.toAndroidResultString();
		}
	}
	
	
	@RequestMapping(params = "sendTelValidateCode")
	@ResponseBody
	public String sendTelValidateCode() {
		Map<String, String> dataMap = new HashMap<String, String>();

		String phoneNumber = StringUtils.getMapDataStr(reqBodyMap,
				"phoneNumber");
		String type = StringUtils.getMapDataStr(reqBodyMap, "type");

		boolean checkUserExsits = systemService.checkUserExsits(phoneNumber);
		if ((checkUserExsits && type.equals("register")) || (!checkUserExsits && type.equals("forgetPassword"))){ 
		try {
			String validateCode = ValidateCodeUtils.get4ValidateCode();
			long maxtime = BaseConfigMgr.getUserSmsSendMaxtime();
			String sendSMS = "";

			String userSmsTemplateId = this.getUserSmsTemplateId(type);
			if (userSmsTemplateId != null && !"".equals(userSmsTemplateId)) {
				sendSMS = SmsTemplate.sendSMS(phoneNumber,
						this.getUserSmsTemplateId(type), validateCode + ","
								+ maxtime);

				Map<?, ?> obj = (Map<?, ?>) JSONObject.fromObject(sendSMS);
				if (obj != null) {
					Map<?, ?> obj1 = (Map<?, ?>) JSONObject.fromObject(obj
							.get("resp"));
					if (obj1 != null && "000000".equals(obj1.get("respCode"))) {
						Map<?, ?> obj2 = (Map<?, ?>) JSONObject.fromObject(obj1
								.get("templateSMS"));
						if (obj2 != null) {
							dataMap.put(
									"smsId",
									(obj2.get("smsId") != null) ? obj2.get(
											"smsId").toString() : "");
						}
					}
				}

				TSPhoneValidateCode tSphoneValidateCode = new TSPhoneValidateCode();
				tSphoneValidateCode.setMobilephone(phoneNumber);
				tSphoneValidateCode.setType(type);
				tSphoneValidateCode.setValidateCode(validateCode);
				tSphoneValidateCode.setCreateTime(DateUtil.getSysTimeStamp());
				if (dataMap.containsKey("smsId")) {
					tSphoneValidateCode.setValidateCodeId(dataMap.get("smsId"));
					phoneValidateCodeService.save(tSphoneValidateCode);
					Map<String, Object> resultData = new HashMap<String, Object>();
					resultData.put("validateCode", validateCode);
					return new AndroidResult(AndroidResult.SUCCESS_CODE,
							AndroidResult.SUCCESS_MASSEGE, resultData)
							.toAndroidResultString();
				} else {
					return AndroidResult.defaultFailure("调用短信接口失败")
							.toAndroidResultString();
				}
			} else {
				return AndroidResult.defaultFailure("未定义的短信类型")
						.toAndroidResultString();
			}

		} catch (Exception e) {
			e.printStackTrace();
			return AndroidResult.defaultFailure(e.getMessage())
					.toAndroidResultString();
		}
		}else if(!checkUserExsits && type.equals("register")){
			return AndroidResult.defaultFailure("手机号已注册，不能重复注册")
					.toAndroidResultString();
		}else if(checkUserExsits && type.equals("forgetPassword")){
			return AndroidResult.defaultFailure("手机号未注册,请先注册账号")
					.toAndroidResultString();
		}else 
		   return AndroidResult.defaultFailure("未定义的类型")
					.toAndroidResultString();	
	}

	private String getUserSmsTemplateId(String smsTemplateType) {
		return BaseConfigMgr.getBaseConfigure().getString(
				"user.sms.send." + smsTemplateType + ".templateId");
	}

}
