package com.hnykl.bp.web.system.controller; 
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.hnykl.bp.android.common.AndroidResult;
import com.hnykl.bp.base.core.common.controller.BaseController;
import com.hnykl.bp.base.core.common.exception.BusinessException;
import com.hnykl.bp.base.core.common.hibernate.qbc.CriteriaQuery;
import com.hnykl.bp.base.core.common.model.json.AjaxJson;
import com.hnykl.bp.base.core.common.model.json.DataGrid;
import com.hnykl.bp.base.core.constant.Globals;
import com.hnykl.bp.base.core.util.DataUtils;
import com.hnykl.bp.base.core.util.StringUtil;
import com.hnykl.bp.tag.core.easyui.TagUtil;
import com.hnykl.bp.web.AbstractAndroidController;
import com.hnykl.bp.web.ecm.entity.EcmContentEntity;
import com.hnykl.bp.web.system.entity.MessageEntity; 
import com.hnykl.bp.web.system.service.MessageServiceI;
import com.hnykl.bp.web.system.service.SystemService;
import com.hnykl.bp.base.core.util.MyBeanUtils; 
import com.hnykl.bp.base.tool.character.StringUtils;
import com.hnykl.bp.easemob.server.MessageService;
import com.hnykl.bp.easemob.server.api.IMUserAPI;
import com.hnykl.bp.easemob.server.api.SendMessageAPI;
import com.hnykl.bp.easemob.server.comm.ClientContext;
import com.hnykl.bp.easemob.server.comm.EasemobRestAPIFactory;
import com.hnykl.bp.easemob.server.comm.body.CmdMessageBody;
import com.hnykl.bp.easemob.server.comm.wrapper.BodyWrapper;
import com.hnykl.bp.easemob.server.comm.wrapper.ResponseWrapper;
/**   
 * @Title: Controller
 * @Description: 
 * @author onlineGenerator
 * @date 2016-06-14 17:39:31
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/messageController")
public class MessageController extends AbstractAndroidController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(MessageController.class); 

	@Autowired
	private MessageServiceI messageService;
	@Autowired
	private SystemService systemService;
	private String msg;
	
	public String getMessage() {
		return msg;
	}

	public void setMessage(String msg) {
		this.msg = msg;
	}


	@RequestMapping(params = "findMsgList")
	@ResponseBody
	public String findMsgList() {
		String userId = StringUtils.getMapDataStr(reqBodyMap, "userId");
		try {
			List<Map> dataList =  messageService.queryMsgByUserId(userId);
			HashMap<String, List<Map>> resultMap = new HashMap<String, List<Map>>();
			resultMap.put("msgItems", dataList);
			return new AndroidResult(replyCode, replyMsg, resultMap)
					.toAndroidResultString();
		} catch (Exception e) {
			e.printStackTrace();
			return AndroidResult.defaultFailure().toAndroidResultString();
		}
	}
	@RequestMapping(params = "fetchMsgById")
	@ResponseBody
	public String fetchMsgById() {
		String msgId = StringUtils.getMapDataStr(reqBodyMap, "msgId");
		try {
			MessageEntity messageEntity =  messageService.get(MessageEntity.class,msgId);
			Map dataMap = new HashMap();
			com.hnykl.bp.base.tool.bean.BeanUtils.populate(messageEntity, dataMap);
			return new AndroidResult(replyCode, replyMsg, dataMap)
					.toAndroidResultString();
		} catch (Exception e) {
			e.printStackTrace();
			return AndroidResult.defaultFailure().toAndroidResultString();
		}
	}
}
