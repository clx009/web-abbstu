package com.hnykl.bp.web.system.common;

import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;

import com.hnykl.bp.base.configuration.ConfigurationException;
import com.hnykl.bp.base.configuration.ConfigurationRegister;
/**
 * android用户配置
 */
public class AndroidUserConfigMgr {
	 /**
     *  Logger
     */
    private static final Logger logger = Logger.getLogger(AndroidUserConfigMgr.class);
	
	 private static Configuration adminUserConf;

	    public static Configuration getAndroidUserConfigure() {
	        if (adminUserConf == null) {
	            try {
	                ConfigurationRegister confRegister = ConfigurationRegister
	                        .getInstance();
	                confRegister.refreshConfiguration("user");
	                adminUserConf = confRegister.getConfiguration("user");
	            } catch (ConfigurationException ex) {
	                return null;
	            }
	        }
	        return adminUserConf;
	    }

	    public static boolean refresh() {
	        try {
	            ConfigurationRegister confRegister = ConfigurationRegister
	                    .getInstance();
	            confRegister.refreshConfiguration("user");
	            return true;
	        } catch (ConfigurationException ex) {
	            return false;
	        }
	    }
		/**
		 * 得到匿名访问时token的失效时间
		 * @return
		 */
	    public static int getAndroidTokenTimeout() {
	        int value = 0;
	        try {
	            value = getAndroidUserConfigure().getInt("cnffandroid.token.timeout.time");
	        } catch (Exception e) {
	        	logger.warn("获取配置中token传输过程有失效时间失败:" + e.getMessage());
	        }
	        if(value == 0) value= 60000;
	        return value;
	    }
	    /**
	     * 得到匿名访问时token的失效时间
	     * @return
	     */
	    public static String getAndroidDesKey() {
	    	String value = "";
	    	try {
	    		value = getAndroidUserConfigure().getString("cnffandroid.touums.json.des.key");
	    	} catch (Exception e) {
	    		logger.warn("获取配置中token传输过程有失效时间失败:" + e.getMessage());
	    	}
	    	return value;
	    }
	    
	    
	    
}
