package com.hnykl.bp.web.system.enums;

public enum LoginState {
	
	SUCCESS(1, "登录成功"), FAILURE(-1, "用户名或密码不正确"),SYS_ERRO(-2,"系统异常");
	private int state;
	private String stateInfo;

	LoginState(int state, String stateInfo) {
		this.state = state;
		this.stateInfo = stateInfo;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public String getStateInfo() {
		return stateInfo;
	}

	public void setStateInfo(String stateInfo) {
		this.stateInfo = stateInfo;
	}
	
	public static LoginState stateOf(int index){
		for(LoginState state:values()){
			if(state.getState()==index){
				return state;
			}
		}
		return null;
	}

}
