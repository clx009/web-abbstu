package com.hnykl.bp.web.ca.controller;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.hnykl.bp.android.common.AndroidResult;
import com.hnykl.bp.base.configuration.BaseConfigMgr;
import com.hnykl.bp.base.tool.date.DateUtil;
import com.hnykl.bp.base.tool.file.FileUtil;
import com.hnykl.bp.web.AbstractAndroidController;
import com.hnykl.bp.web.ca.entity.TCaApplyEntity;
import com.hnykl.bp.web.ca.service.TCaApplyServiceI;
import com.hnykl.bp.web.exam.entity.SubjectEntity;
import com.hnykl.bp.web.system.pojo.base.TSAttachment;
import com.hnykl.bp.web.system.pojo.base.TSUser;
import com.hnykl.bp.web.system.service.TSAttachmentServiceI;

@Controller
@RequestMapping("tCaApplyController")
public class TCaApplyController extends AbstractAndroidController {

	@Autowired
	private TCaApplyServiceI tCaApplyService;

	@Autowired
	private TSAttachmentServiceI tSAttachmentService;

	DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	/**
	 * 
	 * @param userId
	 *            //当前用户id ，也就是申请者id
	 * @param subjectid
	 *            //科目id
	 * @param comments
	 *            //申请说明
	 * @param files
	 *            //文件
	 * @return
	 */
	@RequestMapping(params = "applyCoach")
	@ResponseBody
	public String applyCoach(@RequestParam(required = false) String userId,
			@RequestParam(required = false) String type,
			@RequestParam(required = false) String subjectId,
			@RequestParam(required = false) String startTime,
			@RequestParam(required = false) String endTime,
			@RequestParam(required = false) String comments,
			@RequestParam(required = false) String addressDesc,
			@RequestParam(required = false) String longitude,
			@RequestParam(required = false) String latitude,
			@RequestParam(required = false) MultipartFile[] files) {
		TCaApplyEntity entity = new TCaApplyEntity();
		TSUser applicant = new TSUser();
		applicant.setId(userId);
		entity.setApplicant(applicant);

		try {
			if (!StringUtils.isEmpty(addressDesc)) {
				entity.setAddressDesc(addressDesc);
			}
			if (!StringUtils.isEmpty(longitude)) {
				entity.setLongitude(longitude);
			}
			if (!StringUtils.isEmpty(latitude)) {
				entity.setLatitude(latitude);
			}
			if (!StringUtils.isEmpty(subjectId)) {
				SubjectEntity subject = new SubjectEntity();
				subject.setId(subjectId);
				entity.setSubject(subject);
			}
			
			// if (!StringUtils.isEmpty(startTime)) {
			Date startDate = new Date();
			startDate = format.parse(startTime);
			entity.setStartTime(startDate);
			// }
			// if (!StringUtils.isEmpty(endTime)) {
			Date endDate = new Date();
			endDate = format.parse(endTime);
			entity.setEndTime(endDate);
			// }

			long diff = endDate.getTime() - startDate.getTime();
			System.out.println("通话时间 :"+diff);
			String minutes = (diff / (1000  )) + "秒";
			entity.setTimeSpan(minutes);
		} catch (Exception e1) {
			e1.printStackTrace();
		}

		entity.setType(type);
		entity.setStatus("0");
		entity.setComments(comments);
		entity.setCreateTime(new Date());
		String applyId = (String) tCaApplyService.saveTCaApply(entity);
		try {
			if (files != null) {
				for (MultipartFile file : files) {
					String suffix = file.getOriginalFilename().substring(
							file.getOriginalFilename().lastIndexOf("."));
					String uploadPath = BaseConfigMgr.getUploadpath();
					String savePath = uploadPath + "/" + "coach/apply/"
							+ DateUtil.getSysDate() + "/" + applyId + "/";

					String newFileName = UUID.randomUUID() + suffix;

					String filePath = savePath + newFileName;

					if (!FileUtil.fileExists(savePath)) {
						FileUtil.createDir(savePath);
					}

					FileUtil.writeFile(newFileName, savePath, file.getBytes());
					TSAttachment a = new TSAttachment();
					a.setAttachmenttitle("coach apply");
					a.setBusinessKey(applyId);
					a.setCreatedate(new Timestamp(System.currentTimeMillis()));
					a.setExtend(suffix);
					a.setRealpath(filePath);
					a.setSubclassname("com.hnykl.bp.web.ca.entity.TCaApplyEntity");
					tSAttachmentService.save(a);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
			return AndroidResult.defaultFailure(e.getMessage())
					.toAndroidResultString();
		}

		return AndroidResult.defaultSuccess("申请成功").toAndroidResultString();
	}

}
