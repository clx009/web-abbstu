package com.hnykl.bp.web.shijian.controller;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.text.SimpleDateFormat;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.hnykl.bp.android.common.AndroidResult;
import com.hnykl.bp.base.configuration.BaseConfigMgr;
import com.hnykl.bp.base.core.common.controller.BaseController;
import com.hnykl.bp.base.core.common.exception.BusinessException;
import com.hnykl.bp.base.core.common.hibernate.qbc.CriteriaQuery;
import com.hnykl.bp.base.core.common.model.json.AjaxJson;
import com.hnykl.bp.base.core.common.model.json.DataGrid;
import com.hnykl.bp.base.core.constant.Globals;
import com.hnykl.bp.base.core.util.StringUtil;
import com.hnykl.bp.tag.core.easyui.TagUtil;
import com.hnykl.bp.web.AbstractAndroidController;
import com.hnykl.bp.web.character.entity.TSCharactertestEntity;
import com.hnykl.bp.web.system.pojo.base.TSDepart;
import com.hnykl.bp.web.system.service.SystemService;
import com.hnykl.bp.base.core.util.MyBeanUtils;
import com.hnykl.bp.base.tool.character.StringUtils;

import com.hnykl.bp.web.shijian.entity.TSShsjEntity;
import com.hnykl.bp.web.shijian.service.TSShsjServiceI;

/**   
 * @Title: Controller
 * @Description: t_s_shsj
 * @author onlineGenerator
 * @date 2016-10-07 16:33:37
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/tSShsjController")
public class TSShsjController extends AbstractAndroidController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(TSShsjController.class);

	@Autowired
	private TSShsjServiceI tSShsjService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	@RequestMapping(params = "findSocialPractices")
	@ResponseBody
	public String findSocialPractices() {
		String userId = StringUtils.getMapDataStr(reqBodyMap, "userId");
//		userId = "8af4a8c456648cdc015667ae92ef007c";
		try {
			Map<String, Object> resultMap = new HashMap<String, Object>();
			List<TSShsjEntity> academics = tSShsjService.findSocialPractice(userId,"1");
			for(TSShsjEntity e:academics){
				e.setPath(BaseConfigMgr.getWebBaseUrl()+e.getPath());
			}
			resultMap.put("academics", academics);
			List<TSShsjEntity> socials = tSShsjService.findSocialPractice(userId,"2");
			for(TSShsjEntity e:socials){
				e.setPath(BaseConfigMgr.getWebBaseUrl()+e.getPath());
			}
			resultMap.put("socials", socials);
			List<TSShsjEntity> expands = tSShsjService.findSocialPractice(userId,"3");
			for(TSShsjEntity e:expands){
				e.setPath(BaseConfigMgr.getWebBaseUrl()+e.getPath());
			}
			resultMap.put("expands", expands);
			List<TSShsjEntity> internship = tSShsjService.findSocialPractice(userId,"4");
			for(TSShsjEntity e:internship){
				e.setPath(BaseConfigMgr.getWebBaseUrl()+e.getPath());
			}
			resultMap.put("internship", internship);
			List<TSShsjEntity> arts = tSShsjService.findSocialPractice(userId,"5");
			for(TSShsjEntity e:arts){
				e.setPath(BaseConfigMgr.getWebBaseUrl()+e.getPath());
			}
			resultMap.put("arts", arts);
			
			return new AndroidResult(AndroidResult.SUCCESS_CODE,
					AndroidResult.SUCCESS_MASSEGE, resultMap)
					.toAndroidResultString();
		} catch (Exception e) {
			e.printStackTrace();
			return AndroidResult.defaultFailure(e.getMessage())
					.toAndroidResultString();
		}
	}
}
