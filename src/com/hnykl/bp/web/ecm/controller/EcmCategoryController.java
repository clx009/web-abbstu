package com.hnykl.bp.web.ecm.controller;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.hnykl.bp.base.core.common.controller.BaseController;
import com.hnykl.bp.base.core.common.exception.BusinessException;
import com.hnykl.bp.base.core.common.hibernate.qbc.CriteriaQuery;
import com.hnykl.bp.base.core.common.model.json.AjaxJson;
import com.hnykl.bp.base.core.common.model.json.ComboTree;
import com.hnykl.bp.base.core.common.model.json.DataGrid;
import com.hnykl.bp.base.core.common.model.json.TreeGrid;
import com.hnykl.bp.base.core.constant.Globals;
import com.hnykl.bp.base.core.util.MyBeanUtils;
import com.hnykl.bp.base.core.util.StringUtil;
import com.hnykl.bp.tag.core.easyui.TagUtil;
import com.hnykl.bp.tag.vo.datatable.SortDirection;
import com.hnykl.bp.tag.vo.easyui.ComboTreeModel;
import com.hnykl.bp.tag.vo.easyui.TreeGridModel;
import com.hnykl.bp.web.ecm.entity.EcmCategoryEntity;
import com.hnykl.bp.web.ecm.service.EcmCategoryServiceI;
import com.hnykl.bp.web.system.pojo.base.TSTerritory;
import com.hnykl.bp.web.system.service.SystemService;

/**   
 * @Title: Controller
 * @Description: 栏目管理
 * @author onlineGenerator
 * @date 2014-11-24 16:39:13
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/ecmCategoryController")
public class EcmCategoryController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(EcmCategoryController.class);

	@Autowired
	private EcmCategoryServiceI ecmCategoryService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 栏目管理列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "ecmCategory")
	public ModelAndView ecmCategory(HttpServletRequest request) {
		return new ModelAndView("ecm/ecmCategory/ecmCategoryList");
	}

	/**
	 * 栏目列表
	 */
	@RequestMapping(params = "ecmCategoryGrid")
	@ResponseBody
	public List<TreeGrid> ecmCategoryGrid(HttpServletRequest request, TreeGrid treegrid) {
		CriteriaQuery cq = new CriteriaQuery(EcmCategoryEntity.class);
			if (treegrid.getId() != null) {
				cq.eq("ecmCategoryEntity.id", treegrid.getId());
			}
			if (treegrid.getId() == null) {
				//cq.createAlias("ecmCategoryEntity", "ee");
				cq.eq("level",Short.valueOf("0"));//这个是最高级的
			}
		
		cq.addOrder("sort", SortDirection.asc);
		cq.add();
		List<EcmCategoryEntity> categoryList = ecmCategoryService.getListByCriteriaQuery(cq, false);
		List<TreeGrid> treeGrids = new ArrayList<TreeGrid>();
		TreeGridModel treeGridModel = new TreeGridModel();
		treeGridModel.setIcon("");
		treeGridModel.setTextField("name");
		treeGridModel.setParentText("ecmCategory_name");
		treeGridModel.setParentId("ecmCategory_id");
		treeGridModel.setSrc("code");
		treeGridModel.setIdField("id");
		treeGridModel.setChildList("ecmCategoryEntitys");
		treeGridModel.setOrder("sort");
		treeGridModel.setDescription("description");
		treeGrids = ecmCategoryService.treegrid(categoryList, treeGridModel);
		return treeGrids;
	}
	
	
	
	
	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(EcmCategoryEntity ecmCategory,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(EcmCategoryEntity.class, dataGrid);
		//查询条件组装器
		com.hnykl.bp.base.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, ecmCategory, request.getParameterMap());
		try{
		//自定义追加查询条件
		}catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
		cq.add();
		this.ecmCategoryService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	
	
	/**
	 * 栏目添加或更新
	 * @param territory
	 * @param req
	 * @return
	 */
	@RequestMapping(params = "addorupdate")
	public ModelAndView addorupdate(EcmCategoryEntity ecmCategoryEntity, HttpServletRequest req) {
		String functionid = req.getParameter("id");
		if (functionid != null) {
			ecmCategoryEntity = ecmCategoryService.getEntity(EcmCategoryEntity.class, functionid);
			req.setAttribute("categoryEntity", ecmCategoryEntity);
		}
		
		if(ecmCategoryEntity.getEcmCategoryEntity() !=null && ecmCategoryEntity.getEcmCategoryEntity().getId()!=null){
			System.out.println(ecmCategoryEntity.getEcmCategoryEntity().getId());
			ecmCategoryEntity.setEcmCategoryEntity((EcmCategoryEntity)ecmCategoryService.getEntity(EcmCategoryEntity.class, ecmCategoryEntity.getEcmCategoryEntity().getId()));
			req.setAttribute("categoryEntity", ecmCategoryEntity);
		}
		return new ModelAndView("ecm/ecmCategory/ecmCategory");
	}
	
	
	
	/**
	 * 地域父级下拉菜单
	 */
	@RequestMapping(params = "setPEcmCategory")
	@ResponseBody
	public List<ComboTree> setPEcmCategory(HttpServletRequest request, ComboTree comboTree) {
		CriteriaQuery cq = new CriteriaQuery(EcmCategoryEntity.class);
		if (comboTree.getId() != null) {
			cq.eq("ecmCategoryEntity.id", comboTree.getId());
		}
		if (comboTree.getId() == null) {
			cq.isNull("ecmCategoryEntity");
		}
		cq.add();
		List<EcmCategoryEntity> ecmCategoryEntityList = ecmCategoryService.getListByCriteriaQuery(cq, false);
		List<ComboTree> comboTrees = new ArrayList<ComboTree>();
		ComboTreeModel comboTreeModel = new ComboTreeModel("id", "name", "ecmCategoryEntitys");
		comboTrees = ecmCategoryService.ComboTree(ecmCategoryEntityList, comboTreeModel, null);
		return comboTrees;
	}
	
	/**
	 * 栏目保存
	 */
	@RequestMapping(params = "saveEcmCategory")
	@ResponseBody
	public AjaxJson saveEcmCategory(EcmCategoryEntity ecmCategoryEntity, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		String functionOrder = ecmCategoryEntity.getSort();
		if(StringUtils.isEmpty(functionOrder)){
			ecmCategoryEntity.setSort("0");
		}
		if (ecmCategoryEntity.getEcmCategoryEntity().getId().equals("")) {
			ecmCategoryEntity.setEcmCategoryEntity(null);
		}else{
			EcmCategoryEntity parent = ecmCategoryService.getEntity(EcmCategoryEntity.class, ecmCategoryEntity.getEcmCategoryEntity().getId());
			ecmCategoryEntity.setLevel(Short.valueOf(parent.getLevel()+1+""));
		}
		if (StringUtil.isNotEmpty(ecmCategoryEntity.getId())) {
			message = "栏目: " + ecmCategoryEntity.getName() + "被更新成功";
			ecmCategoryService.saveOrUpdate(ecmCategoryEntity);
			systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
		} else {
			ecmCategoryEntity.setSort(ecmCategoryEntity.getSort());
			message = "栏目: " + ecmCategoryEntity.getName() + "被添加成功";
			ecmCategoryService.save(ecmCategoryEntity);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		
		return j;
	}
	
	
	/**
	 * 删除栏目管理
	 * 
	 * @return
	 */
	@RequestMapping(params = "doDel")
	@ResponseBody
	public AjaxJson doDel(EcmCategoryEntity ecmCategory, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		ecmCategory = systemService.getEntity(EcmCategoryEntity.class, ecmCategory.getId());
		message = "栏目管理删除成功";
		try{
			ecmCategoryService.delete(ecmCategory);
			systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		}catch(Exception e){
			e.printStackTrace();
			message = "栏目管理删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	
	/**
	 * 批量删除栏目管理
	 * 
	 * @return
	 */
	 @RequestMapping(params = "doBatchDel")
	@ResponseBody
	public AjaxJson doBatchDel(String ids,HttpServletRequest request){
		AjaxJson j = new AjaxJson();
		message = "栏目管理删除成功";
		try{
			for(String id:ids.split(",")){
				EcmCategoryEntity ecmCategory = systemService.getEntity(EcmCategoryEntity.class, 
				id
				);
				ecmCategoryService.delete(ecmCategory);
				systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
			}
		}catch(Exception e){
			e.printStackTrace();
			message = "栏目管理删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加栏目管理
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doAdd")
	@ResponseBody
	public AjaxJson doAdd(EcmCategoryEntity ecmCategory, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "栏目管理添加成功";
		try{
			ecmCategoryService.save(ecmCategory);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}catch(Exception e){
			e.printStackTrace();
			message = "栏目管理添加失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	
	/**
	 * 更新栏目管理
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doUpdate")
	@ResponseBody
	public AjaxJson doUpdate(EcmCategoryEntity ecmCategory, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "栏目管理更新成功";
		EcmCategoryEntity t = ecmCategoryService.get(EcmCategoryEntity.class, ecmCategory.getId());
		try {
			MyBeanUtils.copyBeanNotNull2Bean(ecmCategory, t);
			ecmCategoryService.saveOrUpdate(t);
			systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
		} catch (Exception e) {
			e.printStackTrace();
			message = "栏目管理更新失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	

	/**
	 * 栏目管理新增页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goAdd")
	public ModelAndView goAdd(EcmCategoryEntity ecmCategory, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(ecmCategory.getId())) {
			ecmCategory = ecmCategoryService.getEntity(EcmCategoryEntity.class, ecmCategory.getId());
			req.setAttribute("ecmCategoryPage", ecmCategory);
		}
		return new ModelAndView("ecm/ecmCategory/ecmCategory-add");
	}
	/**
	 * 栏目管理编辑页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goUpdate")
	public ModelAndView goUpdate(EcmCategoryEntity ecmCategory, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(ecmCategory.getId())) {
			ecmCategory = ecmCategoryService.getEntity(EcmCategoryEntity.class, ecmCategory.getId());
			req.setAttribute("ecmCategoryPage", ecmCategory);
		}
		return new ModelAndView("ecm/ecmCategory/ecmCategory-update");
	}
}
