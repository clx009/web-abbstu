package com.hnykl.bp.web.ecm.controller;
import java.io.File;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import jersey.repackaged.com.google.common.collect.Lists;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hnykl.bp.android.common.AndroidResult;
import com.hnykl.bp.base.configuration.BaseConfigMgr;
import com.hnykl.bp.base.tool.bean.BeanUtils;
import com.hnykl.bp.base.tool.character.StringUtils;
import com.hnykl.bp.web.AbstractAndroidController;
import com.hnykl.bp.web.ecm.entity.EcmContentEntity;
import com.hnykl.bp.web.ecm.service.EcmContentServiceI;
import com.hnykl.bp.web.system.service.SystemService;

/**   
 * @Title: Controller
 * @Description: 内容管理
 * @author onlineGenerator
 * @date 2014-11-24 16:25:07
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/contentController")
public class EcmContentController extends AbstractAndroidController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(EcmContentController.class);

	@Autowired
	private EcmContentServiceI ecmContentService;
	@Autowired
	private SystemService systemService;
	
	
	@RequestMapping(params = "findContentList")
	@ResponseBody
	public String findContentList() {
		String categoryCode = StringUtils.getMapDataStr(reqBodyMap, "categoryCode");
//		categoryCode = "001002001001";
		try {
			List<EcmContentEntity> ecmContentEntityList = ecmContentService.findContentList(categoryCode);
			
//			List<EcmContentEntity> ecmContentList = Lists.newArrayList();
			for(int i=0; i<ecmContentEntityList.size(); i++){
				if(ecmContentEntityList.get(i).getAttachmentPath() != null){
				ecmContentEntityList.get(i).setAttachmentPath(BaseConfigMgr.getWebBaseUrl()+ ecmContentEntityList.get(i).getAttachmentPath());
				}
				
				String fileName = ecmContentEntityList.get(i).getId() + ".html";
				String content = ecmContentEntityList.get(i).getContent();
//				File fp=new File("../webapps/abbstu/"+ fileName);
				File fp=new File("/usr/share/tomcat/webapps/" + fileName);
				PrintWriter pfp= new PrintWriter(fp);
				pfp.print(content);
			    pfp.close();
				
				ecmContentEntityList.get(i).setContent(BaseConfigMgr.getWebVisitUrl() + fileName);
			}
			
			HashMap<String, List<EcmContentEntity>> resultMap = new HashMap<String, List<EcmContentEntity>>();
			resultMap.put("contentItems", ecmContentEntityList);
					
//			resultMap.put("contentItems", ecmContentEntityList);
			return new AndroidResult(replyCode, replyMsg, resultMap)
					.toAndroidResultString();
		} catch (Exception e) {
			e.printStackTrace();
			return AndroidResult.defaultFailure().toAndroidResultString();
		}
	}
	
	@RequestMapping(params = "saveContent")
	@ResponseBody
	public String saveContent() {
		EcmContentEntity ecmContentEntity = new EcmContentEntity();
		try {
			BeanUtils.populate(ecmContentEntity, reqBodyMap);
			ecmContentEntity.setIsEnable("1");
			Date nowDate = new Date();
			ecmContentEntity.setCreateTime(nowDate);
			ecmContentEntity.setModifyTime(nowDate);
			ecmContentService.save(ecmContentEntity);
			String categoryCode = StringUtils.getMapDataStr(reqBodyMap, "categoryCode");
			ecmContentService.saveCatContent(categoryCode, ecmContentEntity.getId() );
			return new AndroidResult(replyCode, replyMsg, new HashMap())
					.toAndroidResultString();
		} catch (Exception e) {
			e.printStackTrace();
			return AndroidResult.defaultFailure().toAndroidResultString();
		}
	}
	
	
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


}
