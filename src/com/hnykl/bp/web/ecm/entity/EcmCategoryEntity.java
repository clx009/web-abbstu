package com.hnykl.bp.web.ecm.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;

import com.hnykl.bp.base.core.common.entity.IdEntity;

/**   
 * @Title: Entity
 * @Description: 栏目管理
 * @author onlineGenerator
 * @date 2014-11-24 16:39:13
 * @version V1.0   
 *
 */
@Entity
@Table(name = "ecm_category", schema = "")
@SuppressWarnings("serial")
public class EcmCategoryEntity extends IdEntity implements java.io.Serializable {
	private EcmCategoryEntity ecmCategoryEntity;
	/**name*/
	private java.lang.String name;
	/**type*/
	private java.lang.String type;
	/**code*/
	private java.lang.String code;
	/**isEnable*/
	private java.lang.String isEnable;

	/**description*/
	private java.lang.String description;
	/**createTime*/
	private java.lang.String createTime;
	/**modifyTime*/
	private java.lang.String modifyTime;
	/**createUserId*/
	private java.lang.String createUserId;
	private java.lang.String sort;
	
	private Short level;
	
	private List<EcmCategoryEntity> ecmCategoryEntitys = new ArrayList<EcmCategoryEntity>();
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parent_category_id")
	@ForeignKey(name="null")//取消hibernate的外键生成
	public EcmCategoryEntity getEcmCategoryEntity() {
		return ecmCategoryEntity;
	}

	public void setEcmCategoryEntity(EcmCategoryEntity ecmCategoryEntity) {
		this.ecmCategoryEntity = ecmCategoryEntity;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "ecmCategoryEntity")
	public List<EcmCategoryEntity> getEcmCategoryEntitys() {
		return ecmCategoryEntitys;
	}

	public void setEcmCategoryEntitys(List<EcmCategoryEntity> ecmCategoryEntitys) {
		this.ecmCategoryEntitys = ecmCategoryEntitys;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  name
	 */
	@Column(name ="NAME",nullable=true)
	public java.lang.String getName(){
		return this.name;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  name
	 */
	public void setName(java.lang.String name){
		this.name = name;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  type
	 */
	@Column(name ="TYPE",nullable=true)
	public java.lang.String getType(){
		return this.type;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  type
	 */
	public void setType(java.lang.String type){
		this.type = type;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  code
	 */
	@Column(name ="CODE",nullable=true)
	public java.lang.String getCode(){
		return this.code;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  code
	 */
	public void setCode(java.lang.String code){
		this.code = code;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  isEnable
	 */
	@Column(name ="IS_ENABLE",nullable=true)
	public java.lang.String getIsEnable(){
		return this.isEnable;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  isEnable
	 */
	public void setIsEnable(java.lang.String isEnable){
		this.isEnable = isEnable;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  description
	 */
	@Column(name ="DESCRIPTION",nullable=true)
	public java.lang.String getDescription(){
		return this.description;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  description
	 */
	public void setDescription(java.lang.String description){
		this.description = description;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  createTime
	 */
	@Column(name ="CREATE_TIME",nullable=true)
	public java.lang.String getCreateTime(){
		return this.createTime;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  createTime
	 */
	public void setCreateTime(java.lang.String createTime){
		this.createTime = createTime;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  modifyTime
	 */
	@Column(name ="MODIFY_TIME",nullable=true)
	public java.lang.String getModifyTime(){
		return this.modifyTime;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  modifyTime
	 */
	public void setModifyTime(java.lang.String modifyTime){
		this.modifyTime = modifyTime;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  createUserId
	 */
	@Column(name ="CREATE_USER_ID",nullable=true)
	public java.lang.String getCreateUserId(){
		return this.createUserId;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  createUserId
	 */
	public void setCreateUserId(java.lang.String createUserId){
		this.createUserId = createUserId;
	}
	
	@Column(name ="sort",nullable=true)
	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}
	
	@Column(name = "level",nullable = true,length = 1)
	public Short getLevel() {
		return level;
	}
	public void setLevel(Short level) {
		this.level = level;
	}
	
}
