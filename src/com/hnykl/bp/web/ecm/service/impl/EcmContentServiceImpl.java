package com.hnykl.bp.web.ecm.service.impl;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hnykl.bp.base.core.common.service.impl.CommonServiceImpl;
import com.hnykl.bp.web.ecm.entity.EcmContentEntity;
import com.hnykl.bp.web.ecm.service.EcmContentServiceI;

@Service("ecmContentService")
@Transactional
public class EcmContentServiceImpl extends CommonServiceImpl implements EcmContentServiceI {

	
 	public <T> void delete(T entity) {
 		super.delete(entity);
 		//执行删除操作配置的sql增强
		this.doDelSql((EcmContentEntity)entity);
 	}
 	
 	public <T> Serializable save(T entity) {
 		Serializable t = super.save(entity);
 		//执行新增操作配置的sql增强
 		this.doAddSql((EcmContentEntity)entity);
 		return t;
 	}
 	
 	public <T> void saveOrUpdate(T entity) {
 		super.saveOrUpdate(entity);
 		//执行更新操作配置的sql增强
 		this.doUpdateSql((EcmContentEntity)entity);
 	}
 	
 	/**
	 * 默认按钮-sql增强-新增操作
	 * @param id
	 * @return
	 */
 	public boolean doAddSql(EcmContentEntity t){
	 	return true;
 	}
 	/**
	 * 默认按钮-sql增强-更新操作
	 * @param id
	 * @return
	 */
 	public boolean doUpdateSql(EcmContentEntity t){
	 	return true;
 	}
 	/**
	 * 默认按钮-sql增强-删除操作
	 * @param id
	 * @return
	 */
 	public boolean doDelSql(EcmContentEntity t){
	 	return true;
 	}
 	
 	/**
	 * 替换sql中的变量
	 * @param sql
	 * @return
	 */
 	public String replaceVal(String sql,EcmContentEntity t){
 		sql  = sql.replace("#{id}",String.valueOf(t.getId()));
 		sql  = sql.replace("#{title}",String.valueOf(t.getTitle()));
 		sql  = sql.replace("#{content}",String.valueOf(t.getContent()));
 		sql  = sql.replace("#{attachment_path}",String.valueOf(t.getAttachmentPath()));
 		sql  = sql.replace("#{user_id}",String.valueOf(t.getUserId()));
 		sql  = sql.replace("#{login_name}",String.valueOf(t.getLoginName()));
 		sql  = sql.replace("#{is_enable}",String.valueOf(t.getIsEnable()));
 		sql  = sql.replace("#{status}",String.valueOf(t.getStatus()));
 		sql  = sql.replace("#{priority}",String.valueOf(t.getPriority()));
 		sql  = sql.replace("#{origin}",String.valueOf(t.getOrigin()));
 		sql  = sql.replace("#{create_time}",String.valueOf(t.getCreateTime()));
 		sql  = sql.replace("#{modify_time}",String.valueOf(t.getModifyTime()));
 		sql  = sql.replace("#{UUID}",UUID.randomUUID().toString());
 		return sql;
 	}
 	
 	public List<EcmContentEntity> findContentList(String categoryCode) {
		String query = " SELECT ec.* FROM ecm_content ec,ecm_content_category ecc,ecm_category ecat WHERE ec.id = ecc.content_id AND ecat.CODE = :categoryCode AND ecc.category_id = ecat.id AND ec.is_enable = '1' ";
		SQLQuery queryObject = getSession().createSQLQuery(query);
		queryObject.setParameter("categoryCode", categoryCode); 
		queryObject.addEntity(EcmContentEntity.class);
		List<EcmContentEntity> contentList = queryObject.list(); 
		return contentList;
	}
 	
 	public EcmContentEntity	findContentDetail(String contentId){
 		String query = " from EcmContentEntity ec where  ec.isEnable='1' and ec.id = :contentId ";
		Query queryObject = getSession().createQuery(query); 
		queryObject.setParameter("contentId", contentId); 
		List<EcmContentEntity> contentList = queryObject.list(); 
		if(contentList != null){
			return contentList.get(0);
		}
		return null;
 	}
 	
 	public void saveCatContent(String catCode,String contentId){
 		String sql = " insert into ecm_content_category  select lower(REPLACE(uuid(), '-', '')),?,ec.id  from ecm_category ec where ec.code = ? ";
		executeSql(sql,contentId,catCode);
 	}
 	
 	
 	
}