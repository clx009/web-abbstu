package com.hnykl.bp.web.rt.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hnykl.bp.android.common.AndroidResult;
import com.hnykl.bp.base.tool.character.StringUtils;
import com.hnykl.bp.web.AbstractAndroidController;
import com.hnykl.bp.web.rt.entity.RealTimeEntity;
import com.hnykl.bp.web.rt.service.RealTimeServiceI;

@Controller
@RequestMapping("/realTimeController")
public class RealTimeController extends AbstractAndroidController{
	@Autowired
	RealTimeServiceI realTimeService;
	/**
	 * 专业列表查询
	 * 
	 * @param pid 父专业id
	 * @param isHot 是否热门
	 * @return
	 */
	@RequestMapping(params = "findRealTime")
	@ResponseBody
	public String findRealTime() {
		String userId = StringUtils.getMapDataStr(reqBodyMap, "userId");
		try {
			List<RealTimeEntity> realTimeList = realTimeService.findByUserId(userId);
			Map<String, Object> resultData = new HashMap<String, Object>();
			resultData.put("realTimeList", realTimeList);
			
			return new AndroidResult(AndroidResult.SUCCESS_CODE,
					AndroidResult.SUCCESS_MASSEGE, resultData)
					.toAndroidResultString();
		} catch (Exception e) {
			e.printStackTrace();
			return AndroidResult.defaultFailure(e.getMessage())
					.toAndroidResultString();
		}
	}
}
