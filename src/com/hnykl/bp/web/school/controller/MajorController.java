package com.hnykl.bp.web.school.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hnykl.bp.android.common.AndroidResult;
import com.hnykl.bp.base.tool.bean.BeanUtils;
import com.hnykl.bp.base.tool.character.StringUtils;
import com.hnykl.bp.web.AbstractAndroidController;
import com.hnykl.bp.web.school.dto.MajorDTO;
import com.hnykl.bp.web.school.entity.MajorEntity;
import com.hnykl.bp.web.school.service.MajorServiceI;

@Controller
@RequestMapping("majorController")
public class MajorController extends AbstractAndroidController {

	@Autowired
	private MajorServiceI majorService;

	/**
	 * 专业列表查询
	 * 
	 * @param pid 父专业id
	 * @param isHot 是否热门
	 * @return
	 */
	@RequestMapping(params = "majorList")
	@ResponseBody
	public String majorList() {
		try {
			String pid = StringUtils.getMapDataStr(reqBodyMap, "pid");
			String isHot = StringUtils.getMapDataStr(reqBodyMap, "isHot");
			List<MajorEntity> majors = majorService.findMajor(pid,isHot);
			//遍历一遍把父数据取出来
			MajorEntity majorEntity = null;
			MajorDTO majorDTO = null;
			Map<String,MajorDTO> parentMajorEntityMap = new HashMap<String,MajorDTO>();
			Map  majorDTOMap = null;
			for(int i=0;majors != null && i < majors.size(); i++){
				majorEntity = majors.get(i);
				majorDTO = new MajorDTO();
				majorDTOMap = BeanUtils.getAllFieldValues(majorEntity);
				if(majorEntity.getParent() != null){ //如果有父节点
					BeanUtils.populateBean(majorDTO, majorEntity.getParent());
					if(parentMajorEntityMap.get(majorEntity.getParent().getId() ) == null){
						parentMajorEntityMap.put(majorEntity.getParent().getId(), majorDTO);
					}
					parentMajorEntityMap.get(majorEntity.getParent().getId()).getChildrenList().add(majorDTOMap);
				}
//				else{
//					BeanUtils.populateBean(majorDTO, majorEntity);
//					parentMajorEntityMap.put(majorDTO.getId(), majorDTO);
//				}
			}
			List<MajorDTO> mapValuesList = new ArrayList<MajorDTO>(parentMajorEntityMap.values()); 
			Map<String, Object> resultData = new HashMap<String, Object>();
			resultData.put("majors", mapValuesList);
			
			return new AndroidResult(AndroidResult.SUCCESS_CODE,
					AndroidResult.SUCCESS_MASSEGE, resultData)
					.toAndroidResultString();
		} catch (Exception e) {
			e.printStackTrace();
			return AndroidResult.defaultFailure(e.getMessage())
					.toAndroidResultString();
		}
	}
 

}
