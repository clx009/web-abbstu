package com.hnykl.bp.web.school.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hnykl.bp.android.common.AndroidResult;
import com.hnykl.bp.base.tool.character.StringUtils;
import com.hnykl.bp.web.AbstractAndroidController;
import com.hnykl.bp.web.school.dto.SchoolCollectionDTO;
import com.hnykl.bp.web.school.entity.SchoolCollectEntity;
import com.hnykl.bp.web.school.service.SchoolCollectServiceI;

@Controller
@RequestMapping("schoolCollectController")
public class SchoolCollectController extends AbstractAndroidController {

	@Autowired
	private SchoolCollectServiceI schoolCollectService;

	/**
	 * 收藏学校
	 * 
	 * @return
	 */
	@RequestMapping(params = "collectSchool")
	@ResponseBody
	public String collectSchool() {

		try {
			SchoolCollectEntity schoolCollectEntity = (SchoolCollectEntity) JSONObject
					.toBean(JSONObject.fromObject(reqBodyMap),
							SchoolCollectEntity.class);
			if (!schoolCollectService
					.getSession()
					.createCriteria(SchoolCollectEntity.class)
					.add(Restrictions.eq("userId",
							schoolCollectEntity.getUserId()))
					.add(Restrictions.eq("schoolId",
							schoolCollectEntity.getSchoolId())).list()
					.isEmpty())
				return AndroidResult.defaultFailure("该学校已收藏")
						.toAndroidResultString();
			schoolCollectEntity.setCreateTime(new Date());
			schoolCollectService.save(schoolCollectEntity);
			return AndroidResult.defaultSuccess("学校收藏成功")
					.toAndroidResultString();
		} catch (Exception e) {
			return AndroidResult.defaultFailure(e.getMessage())
					.toAndroidResultString();
		}

	}

	/**
	 * 取消收藏学校
	 * 
	 * @return
	 */
	@RequestMapping(params = "cancelCollectSchool")
	@ResponseBody
	public String cancelCollectSchool() {
		try {
			String userId = StringUtils.getMapDataStr(reqBodyMap, "userId");
			String schoolId = StringUtils.getMapDataStr(reqBodyMap, "schoolId");
			schoolCollectService.deleteCollect(userId,schoolId);
			return AndroidResult.defaultSuccess("取消收藏成功")
					.toAndroidResultString();
		} catch (Exception e) {
			e.printStackTrace();
			return AndroidResult.defaultFailure(e.getMessage())
					.toAndroidResultString();
		}
	}

	@RequestMapping(params = "collectSchoolList")
	@ResponseBody
	public String collectSchoolList() {
		try {
			String familyId = StringUtils.getMapDataStr(reqBodyMap, "familyId");
			Map<String, Object> resultData = new HashMap<String, Object>();
			List<SchoolCollectionDTO> collectSchools = schoolCollectService
					.findCollectSchoolByFamilyId(familyId);
			resultData.put("collectSchools", collectSchools);
			return new AndroidResult(AndroidResult.SUCCESS_CODE,
					AndroidResult.SUCCESS_MASSEGE, resultData)
					.toAndroidResultString();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return AndroidResult.defaultFailure(e.getMessage())
					.toAndroidResultString();
		}
	}

}
