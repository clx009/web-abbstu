package com.hnykl.bp.web.school.controller;

import java.util.HashMap;
import java.util.List;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hnykl.bp.android.common.AndroidResult; 
import com.hnykl.bp.base.tool.character.StringUtils;
import com.hnykl.bp.web.AbstractAndroidController; 
import com.hnykl.bp.web.school.dto.ChooseSchoolRankingQueryDTO;
import com.hnykl.bp.web.school.dto.MajorRankingQueryDTO; 
import com.hnykl.bp.web.school.dto.SchoolOrderDTO; 
import com.hnykl.bp.web.school.service.SchoolMajorServiceI; 

@Controller
@RequestMapping("schoolMajorController")
public class SchoolMajorController extends AbstractAndroidController{
	 
	 
	@Autowired
	private SchoolMajorServiceI schoolMajorService;
	
	/**
	 * 专业排名
	 * 
	 * @return
	 */
	@RequestMapping(params = "majorRanking")
	@ResponseBody
	public String majorRanking() {
		
		MajorRankingQueryDTO rankingQueryDTO = (MajorRankingQueryDTO) JSONObject.toBean(JSONObject.fromObject(reqBodyMap), MajorRankingQueryDTO.class);
		
		try {
//			List<SchoolMajorViewEntity> majorRanking = schoolMajorViewService.getSchoolMajorRanking(rankingQueryDTO);
			List<SchoolOrderDTO> majorRanking = schoolMajorService.getSchoolMajorRanking(rankingQueryDTO);
			HashMap<String, Object> resultData = new HashMap<String,Object>();
			resultData.put("majorRanking", majorRanking);
			return new AndroidResult(AndroidResult.SUCCESS_CODE,
					AndroidResult.SUCCESS_MASSEGE, resultData)
					.toAndroidResultString();
		} catch (Exception e) {
			return AndroidResult.defaultFailure(e.getMessage())
					.toAndroidResultString();
		}
	}
	
	/**
	 * 模拟选校
	 * 
	 * @return
	 */
	@RequestMapping(params = "chooseSchool")
	@ResponseBody
	public String chooseSchool() {
		try { 
			ChooseSchoolRankingQueryDTO chooseSchoolRankingQueryDTO = new ChooseSchoolRankingQueryDTO(); 
			com.hnykl.bp.base.tool.bean.BeanUtils.populate(chooseSchoolRankingQueryDTO, reqBodyMap);
			//用户接收不到的bug改动
			String userId = StringUtils.getMapDataStr(reqBodyMap, "userId");
			String schoolType = StringUtils.getMapDataStr(reqBodyMap, "schoolType");
			chooseSchoolRankingQueryDTO.setUserId(userId);
			chooseSchoolRankingQueryDTO.setPublicOrPrivate(schoolType);
			// (ChooseSchoolRankingQueryDTO) JSONObject.toBean(JSONObject.fromObject(reqBodyMap), ChooseSchoolRankingQueryDTO.class);
//			List<SchoolMajorViewEntity> majorRanking = schoolMajorViewService.getSchoolMajorRanking(chooseSchoolRankingQueryDTO);
			List<SchoolOrderDTO> majorRanking = schoolMajorService.getSchoolMajorRanking(chooseSchoolRankingQueryDTO);
			HashMap<String, Object> resultData = new HashMap<String,Object>();
			resultData.put("majorRanking", majorRanking);
			return new AndroidResult(AndroidResult.SUCCESS_CODE,
					AndroidResult.SUCCESS_MASSEGE, resultData)
					.toAndroidResultString();
		} catch (Exception e) {
			return AndroidResult.defaultFailure(e.getMessage())
					.toAndroidResultString();
		}
	}
	
}
