package com.hnykl.bp.web.school.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hnykl.bp.android.common.AndroidResult;
import com.hnykl.bp.base.tool.character.StringUtils;
import com.hnykl.bp.web.AbstractAndroidController;
import com.hnykl.bp.web.school.dto.RankingQueryDTO;
import com.hnykl.bp.web.school.dto.SchoolOrderDTO;
import com.hnykl.bp.web.school.entity.SchoolEntity;
import com.hnykl.bp.web.school.service.SchoolServiceI;
import com.hnykl.bp.web.system.entity.TerritoryEntity;

@Controller
@RequestMapping("schoolController")
public class SchoolController extends AbstractAndroidController {

	@Autowired
	private SchoolServiceI schoolService;

	/**
	 * 院校排名
	 * 
	 * @return
	 */
	@RequestMapping(params = "schoolRanking")
	@ResponseBody
	public String schoolRanking() {
		try {

			RankingQueryDTO rankingQueryDTO = (RankingQueryDTO) JSONObject
					.toBean(JSONObject.fromObject(reqBodyMap),
							RankingQueryDTO.class);

			List<SchoolOrderDTO> schoolRanking = schoolService
					.getSchoolRanking(rankingQueryDTO);
			Map<String, Object> resultData = new HashMap<String, Object>();
			resultData.put("schoolRanking", schoolRanking);
			return new AndroidResult(AndroidResult.SUCCESS_CODE,
					AndroidResult.SUCCESS_MASSEGE, resultData)
					.toAndroidResultString();

		} catch (Exception e) {
			e.printStackTrace();
			return AndroidResult.defaultFailure(e.getMessage())
					.toAndroidResultString();
		}
	}

	/**
	 * 
	 * @return
	 */
	@RequestMapping(params = "schoolCitys")
	@ResponseBody
	public String schoolCitys() {
		try {
			String pid = StringUtils.getMapDataStr(reqBodyMap, "pid");
			@SuppressWarnings("unchecked")
			List<TerritoryEntity> list = schoolService.getSession()
					.createCriteria(TerritoryEntity.class)
					.add(Restrictions.eqOrIsNull("territoryparentid", pid))
					.list();
//			List<TerritoryEntity> datas = new ArrayList<TerritoryEntity>();
//			for(TerritoryEntity entity:list){
//				datas.addAll(schoolService.getSession()
//					.createCriteria(TerritoryEntity.class)
//					.add(Restrictions.eqOrIsNull("territoryparentid",entity.getId()))
//					.list());
//			}
//			
			Map<String, Object> resultData = new HashMap<String, Object>();
			
			resultData.put("citys", list);
			return new AndroidResult(AndroidResult.SUCCESS_CODE,
					AndroidResult.SUCCESS_MASSEGE, resultData)
					.toAndroidResultString();
		} catch (Exception e) {
			return AndroidResult.defaultFailure(e.getMessage())
					.toAndroidResultString();
		}
	}

	@RequestMapping(params = "school")
	@ResponseBody
	public String school() {
		try {
			String schoolId = StringUtils.getMapDataStr(reqBodyMap, "schoolId");
			Map<String, Object> resultData = new HashMap<String, Object>();
			resultData.put("school", schoolService.getEntity(SchoolEntity.class, schoolId));
			return new AndroidResult(AndroidResult.SUCCESS_CODE,
					AndroidResult.SUCCESS_MASSEGE, resultData)
					.toAndroidResultString();
		} catch (Exception e) {
			e.printStackTrace();
			return AndroidResult.defaultFailure(e.getMessage()).toAndroidResultString();
		}
	}
}
