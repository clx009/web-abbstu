package com.hnykl.bp.web.exam.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hnykl.bp.android.common.AndroidResult;
import com.hnykl.bp.base.tool.character.StringUtils;
import com.hnykl.bp.web.AbstractAndroidController;
import com.hnykl.bp.web.exam.dto.ExamResultDTO;
import com.hnykl.bp.web.exam.dto.StudyRecordDTO;
import com.hnykl.bp.web.exam.dto.StutyRecordDetailDTO;
import com.hnykl.bp.web.exam.entity.ExamResultEntity;
import com.hnykl.bp.web.exam.service.ExamResultServiceI;
import com.hnykl.bp.web.exam.service.SubjectServiceI;
import com.hnykl.bp.web.school.service.TeacherServiceI;
import com.hnykl.bp.web.system.service.SystemService;

/**
 * @Title: Controller
 * @Description: 成绩维护
 * @author onlineGenerator
 * @date 2016-06-27 10:32:34
 * @version V1.0
 * 
 */
@Controller
@RequestMapping("/examResultController")
public class ExamResultController extends AbstractAndroidController {
	/**
	 * Logger for this class
	 */
	@SuppressWarnings("unused")
	private static final Logger logger = Logger
			.getLogger(ExamResultController.class);

	@Autowired
	private ExamResultServiceI examResultService;

	@Autowired
	private SubjectServiceI subjectService;
	
	@Autowired
	private TeacherServiceI teacherService;

	@Autowired
	private SystemService systemService;
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	@RequestMapping(params = "findExamResult")
	@ResponseBody
	public String findExamResult() {
		String userId = StringUtils.getMapDataStr(reqBodyMap,
				"userId");
		String schoolYear = StringUtils.getMapDataStr(reqBodyMap,
				"schoolYear");
		String term = StringUtils.getMapDataStr(reqBodyMap,
				"term");
		
		try {
			ExamResultEntity paramExamResultEntity = new ExamResultEntity();
//			com.hnykl.bp.base.tool.bean.BeanUtils.populate(paramExamResultEntity, reqBodyMap);
			paramExamResultEntity.setUserId(userId);
			paramExamResultEntity.setSchoolYear(schoolYear);
			paramExamResultEntity.setTerm(term);
			List<ExamResultDTO> examResultDTOList = examResultService.queryExamResult(paramExamResultEntity);
			Map<String, Object> resultMap = new HashMap<String, Object>();
			resultMap.put("examResults", examResultDTOList);
			return new AndroidResult(AndroidResult.SUCCESS_CODE,
					AndroidResult.SUCCESS_MASSEGE, resultMap)
					.toAndroidResultString();
		} catch (Exception e) {
			e.printStackTrace();
			return AndroidResult.defaultFailure(e.getMessage())
					.toAndroidResultString();
		}
	}
	@RequestMapping(params = "analyseExamResult")
	@ResponseBody
	public String analyseExamResult(){
		try {
			ExamResultEntity paramExamResultEntity = new ExamResultEntity();
			com.hnykl.bp.base.tool.bean.BeanUtils.populate(paramExamResultEntity, reqBodyMap);
			String subjectId = StringUtils.getMapDataStr(reqBodyMap, "subjectId");
			String[] subjectIds = (StringUtils.isNotEmpty(subjectId))? subjectId.split(","):null;
			List<ExamResultDTO> examResultDTOList = examResultService.analyseExamResult(paramExamResultEntity,subjectIds);
			ExamResultDTO examResultDTO = null; 
			List<ExamResultDTO> tempList = new ArrayList<ExamResultDTO>();
			Map<String,List>dataMap = new HashMap();
			String lastSchoolYearAndTerm = null; 
			for(int i=0; examResultDTOList != null && i< examResultDTOList.size(); i++ ){
				examResultDTO = examResultDTOList.get(i);
				if(dataMap.get(examResultDTO.getSchoolYear() + "第" + examResultDTO.getTerm()+"学期") == null){
					if(lastSchoolYearAndTerm != null){
						addOtherExamResult(dataMap.get(lastSchoolYearAndTerm),subjectIds);
					}
					tempList = new ArrayList<ExamResultDTO>();
					lastSchoolYearAndTerm = examResultDTO.getSchoolYear() + "第" + examResultDTO.getTerm()+"学期";
					tempList.add(examResultDTO);
					dataMap.put(lastSchoolYearAndTerm,tempList);
				}else{
					dataMap.get(lastSchoolYearAndTerm).add(examResultDTO);
				}
			}
			if(lastSchoolYearAndTerm != null){
				addOtherExamResult(dataMap.get(lastSchoolYearAndTerm),subjectIds);
			}
			
			if(tempList != null && tempList.size() > 0){
				addOtherExamResult(tempList,subjectIds);
				dataMap.put(lastSchoolYearAndTerm,tempList);
			}
			return new AndroidResult(AndroidResult.SUCCESS_CODE,
					AndroidResult.SUCCESS_MASSEGE, dataMap)
					.toAndroidResultString();
		} catch (Exception e) {
			e.printStackTrace();
			return AndroidResult.defaultFailure(e.getMessage())
					.toAndroidResultString();
		}
	}
	
	private void addOtherExamResult(List<ExamResultDTO> tempList,String[] subjectIds){
		List<ExamResultDTO> retExamResultList = new ArrayList<ExamResultDTO>();
		boolean isExist = false;
		for(int j=0;subjectIds != null && j<subjectIds.length;j++){
			isExist = false;
			for(int i=0;tempList != null && i<tempList.size();i++){
				if(tempList.get(i).getSubjectId().equals(subjectIds[j])){
					isExist = true;
					break;
				}
			}
			if(!isExist){
				retExamResultList.add(new ExamResultDTO(subjectIds[j],"0"));
			}
		}
		tempList.addAll(retExamResultList);
	}
	
	
	@RequestMapping(params = "analyseExamResult4GPA")
	@ResponseBody
	public String analyseExamResult4GPA(){
		try {
			ExamResultEntity paramExamResultEntity = new ExamResultEntity();
			com.hnykl.bp.base.tool.bean.BeanUtils.populate(paramExamResultEntity, reqBodyMap);
			String subjectId = StringUtils.getMapDataStr(reqBodyMap, "subjectId");
			String[] subjectIds = (StringUtils.isNotEmpty(subjectId) )? subjectId.split(","):null;
			List<Map> examResultDTOList = examResultService.analyseExamResult4GPA(paramExamResultEntity,subjectIds);
			Map<String, Object> resultMap = new HashMap<String, Object>();
			resultMap.put("examResults", examResultDTOList);
			return new AndroidResult(AndroidResult.SUCCESS_CODE,
					AndroidResult.SUCCESS_MASSEGE, resultMap)
			.toAndroidResultString();
		} catch (Exception e) {
			e.printStackTrace();
			return AndroidResult.defaultFailure(e.getMessage())
					.toAndroidResultString();
		}
	}
	
	
	@RequestMapping(params = "studyRecord")
	@ResponseBody
	public String studyRecord(){
		String userId = StringUtils.getMapDataStr(reqBodyMap,
				"userId");
		try {
			List<StudyRecordDTO> studyRecordList = examResultService.findStudyRecordList(userId);
			Map<String, Object> resultMap = new HashMap<String, Object>();
			resultMap.put("studyRecordList", studyRecordList);
			return new AndroidResult(AndroidResult.SUCCESS_CODE,
					AndroidResult.SUCCESS_MASSEGE, resultMap)
			.toAndroidResultString();
		} catch (Exception e) {
			e.printStackTrace();
			return AndroidResult.defaultFailure(e.getMessage())
					.toAndroidResultString();
		}
	}
	
	
	@RequestMapping(params = "studyRecordDetail")
	@ResponseBody
	public String studyRecordDetail(){
		String userId = StringUtils.getMapDataStr(reqBodyMap,
				"userId");
		String subjectId = StringUtils.getMapDataStr(reqBodyMap,
				"subjectId");
		try {
			List<StutyRecordDetailDTO> studyRecordDetailList = examResultService.findStudyRecordDetailList(userId,subjectId);
			Map<String, Object> resultMap = new HashMap<String, Object>();
			resultMap.put("studyRecordDetailList", studyRecordDetailList);
			return new AndroidResult(AndroidResult.SUCCESS_CODE,
					AndroidResult.SUCCESS_MASSEGE, resultMap)
			.toAndroidResultString();
		} catch (Exception e) {
			e.printStackTrace();
			return AndroidResult.defaultFailure(e.getMessage())
					.toAndroidResultString();
		}
	}
	
	
}
