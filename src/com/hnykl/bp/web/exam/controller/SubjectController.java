package com.hnykl.bp.web.exam.controller;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.text.SimpleDateFormat;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jersey.repackaged.com.google.common.collect.Lists;

import net.sf.json.JSONArray;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.hnykl.bp.android.common.AndroidResult;
import com.hnykl.bp.base.core.common.controller.BaseController;
import com.hnykl.bp.base.core.common.exception.BusinessException;
import com.hnykl.bp.base.core.common.hibernate.qbc.CriteriaQuery;
import com.hnykl.bp.base.core.common.model.json.AjaxJson;
import com.hnykl.bp.base.core.common.model.json.DataGrid;
import com.hnykl.bp.base.core.constant.Globals;
import com.hnykl.bp.base.core.util.StringUtil;
import com.hnykl.bp.tag.core.easyui.TagUtil;
import com.hnykl.bp.web.AbstractAndroidController;
import com.hnykl.bp.web.school.entity.SchoolEntity;
import com.hnykl.bp.web.system.pojo.base.TSDepart;
import com.hnykl.bp.web.system.service.SystemService;
import com.hnykl.bp.base.core.util.MyBeanUtils;
import com.hnykl.bp.base.tool.character.StringUtils;

import com.hnykl.bp.web.ecm.entity.EcmContentEntity;
import com.hnykl.bp.web.exam.dto.ExamResultDTO;
import com.hnykl.bp.web.exam.entity.ExamItemEntity;
import com.hnykl.bp.web.exam.entity.ExamResultEntity;
import com.hnykl.bp.web.exam.entity.SubjectEntity;
import com.hnykl.bp.web.exam.service.ExamItemServiceI;
import com.hnykl.bp.web.exam.service.SubjectServiceI;

/**   
 * @Title: Controller
 * @Description: 考试科目
 * @author onlineGenerator
 * @date 2016-06-27 15:03:21
 * @version V1.0   
 *
 */


@Controller
@RequestMapping("/subjectController")
public class SubjectController  extends AbstractAndroidController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(SubjectController.class);

	@Autowired
	private SubjectServiceI subjectService;
	@Autowired
	private ExamItemServiceI examItemService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@RequestMapping(params = "findSubject")
	@ResponseBody
	public String findSubject(){
		try {
			List<ExamItemEntity> examItemEntityList = examItemService.findAll();
			for(int i=0; i<examItemEntityList.size(); i++){
			List<SubjectEntity> subjectEntityList = subjectService.findById(examItemEntityList.get(i).getId());
			List<SubjectEntity> subjectList = Lists.newArrayList();
			for(SubjectEntity se:subjectEntityList){
				se.setExamItem(null);
				subjectList.add(se);
			}
			examItemEntityList.get(i).setSubjectEntityList(subjectList);
			}
			Map<String, Object> resultMap = new HashMap<String, Object>();
			resultMap.put("examItemEntityList", examItemEntityList);
			return new AndroidResult(AndroidResult.SUCCESS_CODE,
					AndroidResult.SUCCESS_MASSEGE, resultMap)
					.toAndroidResultString();
		} catch (Exception e) {
			e.printStackTrace();
			return AndroidResult.defaultFailure(e.getMessage())
					.toAndroidResultString();
		}
	}
	
}
