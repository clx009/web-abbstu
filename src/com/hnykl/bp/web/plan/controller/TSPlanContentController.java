package com.hnykl.bp.web.plan.controller;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.hnykl.bp.base.core.common.controller.BaseController;
import com.hnykl.bp.base.core.common.exception.BusinessException;
import com.hnykl.bp.base.core.common.hibernate.qbc.CriteriaQuery;
import com.hnykl.bp.base.core.common.model.json.AjaxJson;
import com.hnykl.bp.base.core.common.model.json.DataGrid;
import com.hnykl.bp.base.core.constant.Globals;
import com.hnykl.bp.base.core.util.MyBeanUtils;
import com.hnykl.bp.base.core.util.StringUtil;
import com.hnykl.bp.tag.core.easyui.TagUtil;
import com.hnykl.bp.web.plan.entity.TSPlanContentEntity;
import com.hnykl.bp.web.plan.service.TSPlanContentServiceI;
import com.hnykl.bp.web.system.service.SystemService;

/**   
 * @Title: Controller
 * @Description: t_s_plan_content
 * @author onlineGenerator
 * @date 2016-10-07 19:58:37
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/tSPlanContentController")
public class TSPlanContentController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(TSPlanContentController.class);

	@Autowired
	private TSPlanContentServiceI tSPlanContentService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * t_s_plan_content列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "tSPlanContent")
	public ModelAndView tSPlanContent(HttpServletRequest request) {
		return new ModelAndView("plan/tSPlanContent/tSPlanContentList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(TSPlanContentEntity tSPlanContent,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(TSPlanContentEntity.class, dataGrid);
		//查询条件组装器
		com.hnykl.bp.base.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, tSPlanContent, request.getParameterMap());
		try{
		//自定义追加查询条件
		}catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
		cq.add();
		this.tSPlanContentService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除t_s_plan_content
	 * 
	 * @return
	 */
	@RequestMapping(params = "doDel")
	@ResponseBody
	public AjaxJson doDel(TSPlanContentEntity tSPlanContent, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		tSPlanContent = systemService.getEntity(TSPlanContentEntity.class, tSPlanContent.getId());
		message = "t_s_plan_content删除成功";
		try{
			tSPlanContentService.delete(tSPlanContent);
			systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		}catch(Exception e){
			e.printStackTrace();
			message = "t_s_plan_content删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	
	/**
	 * 批量删除t_s_plan_content
	 * 
	 * @return
	 */
	 @RequestMapping(params = "doBatchDel")
	@ResponseBody
	public AjaxJson doBatchDel(String ids,HttpServletRequest request){
		AjaxJson j = new AjaxJson();
		message = "t_s_plan_content删除成功";
		try{
			for(String id:ids.split(",")){
				TSPlanContentEntity tSPlanContent = systemService.getEntity(TSPlanContentEntity.class, 
				id
				);
				tSPlanContentService.delete(tSPlanContent);
				systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
			}
		}catch(Exception e){
			e.printStackTrace();
			message = "t_s_plan_content删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加t_s_plan_content
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doAdd")
	@ResponseBody
	public AjaxJson doAdd(TSPlanContentEntity tSPlanContent, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "t_s_plan_content添加成功";
		try{
			tSPlanContentService.save(tSPlanContent);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}catch(Exception e){
			e.printStackTrace();
			message = "t_s_plan_content添加失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	
	/**
	 * 更新t_s_plan_content
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doUpdate")
	@ResponseBody
	public AjaxJson doUpdate(TSPlanContentEntity tSPlanContent, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "t_s_plan_content更新成功";
		TSPlanContentEntity t = tSPlanContentService.get(TSPlanContentEntity.class, tSPlanContent.getId());
		try {
			MyBeanUtils.copyBeanNotNull2Bean(tSPlanContent, t);
			tSPlanContentService.saveOrUpdate(t);
			systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
		} catch (Exception e) {
			e.printStackTrace();
			message = "t_s_plan_content更新失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	

	/**
	 * t_s_plan_content新增页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goAdd")
	public ModelAndView goAdd(TSPlanContentEntity tSPlanContent, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(tSPlanContent.getId())) {
			tSPlanContent = tSPlanContentService.getEntity(TSPlanContentEntity.class, tSPlanContent.getId());
			req.setAttribute("tSPlanContentPage", tSPlanContent);
		}
		return new ModelAndView("plan/tSPlanContent/tSPlanContent-add");
	}
	/**
	 * t_s_plan_content编辑页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goUpdate")
	public ModelAndView goUpdate(TSPlanContentEntity tSPlanContent, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(tSPlanContent.getId())) {
			tSPlanContent = tSPlanContentService.getEntity(TSPlanContentEntity.class, tSPlanContent.getId());
			req.setAttribute("tSPlanContentPage", tSPlanContent);
		}
		return new ModelAndView("plan/tSPlanContent/tSPlanContent-update");
	}
}
