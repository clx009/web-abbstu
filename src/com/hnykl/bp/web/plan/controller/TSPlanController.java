package com.hnykl.bp.web.plan.controller;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hnykl.bp.android.common.AndroidResult;
import com.hnykl.bp.base.configuration.BaseConfigMgr;
import com.hnykl.bp.base.tool.character.StringUtils;
import com.hnykl.bp.web.AbstractAndroidController;
import com.hnykl.bp.web.plan.entity.TSPlanContentEntity;
import com.hnykl.bp.web.plan.entity.TSPlanEntity;
import com.hnykl.bp.web.plan.service.TSPlanContentServiceI;
import com.hnykl.bp.web.plan.service.TSPlanServiceI;
import com.hnykl.bp.web.system.service.SystemService;

/**   
 * @Title: Controller
 * @Description: t_s_plan
 * @author onlineGenerator
 * @date 2016-10-07 19:58:27
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/tSPlanController")
public class TSPlanController extends AbstractAndroidController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(TSPlanController.class);

	@Autowired
	private TSPlanServiceI tSPlanService;
	
	@Autowired
	private TSPlanContentServiceI tSPlanContentService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	@RequestMapping(params = "findStudyPlan")
	@ResponseBody
	public String findStudyPlan() {
		String userId = StringUtils.getMapDataStr(reqBodyMap, "userId");
//		userId = "8af4a8c456648cdc015667ae92ef007c";
		try {
			TSPlanEntity tSPlanEntity = tSPlanService.findStudyPlan(userId);
			List<TSPlanContentEntity> contents = tSPlanContentService.findStudyPlanContents(userId);
			for(TSPlanContentEntity e:contents){
				e.setPath(BaseConfigMgr.getWebBaseUrl()+e.getPath());
			}
			Map<String, Object> resultMap = new HashMap<String, Object>();
			resultMap.put("data", tSPlanEntity);
			resultMap.put("contents", contents);
			return new AndroidResult(AndroidResult.SUCCESS_CODE,
					AndroidResult.SUCCESS_MASSEGE, resultMap)
					.toAndroidResultString();
		} catch (Exception e) {
			e.printStackTrace();
			return AndroidResult.defaultFailure(e.getMessage())
					.toAndroidResultString();
		}
	}
}
