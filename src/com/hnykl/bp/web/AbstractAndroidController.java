package com.hnykl.bp.web;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.util.JSONUtils;

import org.apache.log4j.Logger;

import com.hnykl.bp.android.cache.CacheManager;
import com.hnykl.bp.android.common.AndroidConstant;
import com.hnykl.bp.android.common.AndroidDataTranDefine;
import com.hnykl.bp.android.common.TimestampMorpher;
import com.hnykl.bp.android.des.DESHelper;
import com.hnykl.bp.android.token.TokenEntity;
import com.hnykl.bp.android.token.impl.TokenServiceImpl;
import com.hnykl.bp.base.tool.character.StringUtils;

/**
 * 供其他action类继承抽象类
 */
public abstract class AbstractAndroidController {

	/**
	 * 返回码
	 */
	protected String replyCode = AndroidConstant.ANDROID_RET_JSON_SUCCESS_CODE;
	/**
	 * 返回信息
	 */
	protected String replyMsg = AndroidConstant.ANDROID_RET_JSON_SUCCESS_MSG;

	private static Logger logger = Logger
			.getLogger(AbstractAndroidController.class);
	/**
	 * 请求的json字符串
	 */
	protected String requestJsonStr;
	/**
	 * 请求的用户头部map信息
	 */
	protected Map reqHeadUserMap = new HashMap();
	/**
	 * 请求的请求map主信息
	 */
	protected Map reqBodyMap = new HashMap();

	/**
	 * 请求的token信息
	 */
	protected String token;
	/**
	 * 登录失效token
	 */
	protected String loginInvalidToken = "";

	/**
	 * 初始化对一些基本的数据进行解析
	 */
	public void init() throws Exception {
		try {
			requestJsonStr = initReqData(requestJsonStr);
			if (StringUtils.isNotEmpty(requestJsonStr)) {
				logger.info("=====>request Json:" + requestJsonStr);
				reqHeadUserMap = getRequestHeadUserMapByJsonData(requestJsonStr);
				reqBodyMap = getRequestBodyMapByJsonData(requestJsonStr);
				/*
				 * if(reqBodyMap != null && reqBodyMap.size() >0 &&
				 * reqBodyMap.get("token") != null){ token
				 * =(String)reqBodyMap.get("token"); }
				 */
			}
		} catch (Exception ex) {
			logger.error("初始化数据信息出现异常！", ex);
			throw new Exception("初始化数据信息出现异常！");
		}
	}

	/**
	 * 判断登录是否失效
	 * 
	 * @return
	 * @throws Exception
	 */
	public String validateLoginInvalid() throws Exception {
		loginInvalidToken = createLoginValidToken();
		if (StringUtils.isNotEmpty(loginInvalidToken)) {
			throw new Exception("Token失效，请重新登录!");
		}
		return loginInvalidToken;
	}

	/**
	 * 验证token
	 * 
	 * @return
	 */
	public TokenEntity verifyToken() throws Exception {
		if (StringUtils.isEmpty(token))
			throw new Exception("Token值不能为空！");
		TokenServiceImpl service = new TokenServiceImpl();
		return service.verifyToken(token);
	}

	/**
	 * 创建token
	 * 
	 * @param loginName
	 * @param password
	 * @return
	 */
	public String createToken(String loginName, String password) {
		TokenServiceImpl service = new TokenServiceImpl();
		String token = service.getToken(loginName, password);
		// 往token中设值
		// CacheManager.setToken(loginName,token);
		// 用memcache代替
		return token;
	}

	/**
	 * 得到缓存中的token
	 * 
	 * @return
	 */
	public String getCacheToken() {
		TokenServiceImpl service = new TokenServiceImpl();
		String cacheToken = CacheManager.getTokenByLoginName(service
				.getLoginName(token));
		return cacheToken;
	}

	/**
	 * 创建 登录失效的TOKEN，如果没失效，则不返回失效的token
	 * 
	 * @return
	 */
	public String createLoginValidToken() {
		TokenServiceImpl service = new TokenServiceImpl();
		String cacheToken = getCacheToken();
		String tokenStr = "";
		if (StringUtils.isEmpty(cacheToken)
				|| ((token != null) && !token.equals(cacheToken))) {
			tokenStr = service.getToken(
					AndroidConstant.ANDROID_TOKEN_LOGININVALID_LOGINNAME,
					AndroidConstant.ANDROID_TOKEN_LOGINVALID_PASSWORD);
		}
		return tokenStr;
	}

	/**
	 * 初始化请求的数据
	 * 
	 * @param requestData
	 * @return
	 */
	protected String initReqData(String requestData) {
		logger.info("=========>request json str:" + requestData);
		String retStr = "";
		if (StringUtils.isEmpty(requestData))
			return retStr;
		try {
			// 编码
			/*
			 * if("1".equals(ServiceInit.getConfigValue("IS_CODE"))){ aByte =
			 * DataProcessUtil.BASE64ToByte(requestData); }else{ aByte =
			 * requestData.getBytes(); }
			 */
			// 解密
			if ("1".equals(AndroidDataTranDefine.getConfigValue("IS_ENCRYPT"))) {
				String sKey = AndroidDataTranDefine
						.getConfigValue("ANDROID_KEY");
				DESHelper desHelper = new DESHelper(sKey);
				retStr = desHelper.decrypt(requestData.trim());
				retStr = URLDecoder.decode(retStr, "UTF-8");
			}
			// 解压
			/*
			 * if("1".equals(ServiceInit.getConfigValue("IS_COMPRESS"))){ aByte
			 * = DataProcessUtil.decompress(aByte).getBytes(); }
			 */
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		retStr = StringUtils.isNotEmpty(retStr) ? retStr : requestData;
		return retStr;
	}

	/**
	 * 处理返回的数据
	 * 
	 * @param retData
	 * @return
	 */
	protected String handRetData(String retData) {
		logger.info("======>return json:" + retData);
		String retStr = "";
		if (StringUtils.isEmpty(retData))
			return retStr;
		// byte [] aByte = retData.getBytes();
		try {
			// 加压
			/*
			 * if("1".equals(ServiceInit.getConfigValue("IS_COMPRESS"))){ aByte
			 * = DataProcessUtil.compress(aByte); }
			 */
			retData = URLEncoder.encode(retData, "UTF-8");
			// 加密
			if ("1".equals(AndroidDataTranDefine.getConfigValue("IS_ENCRYPT"))) {
				String sKey = AndroidDataTranDefine
						.getConfigValue("ANDROID_KEY");
				DESHelper desHelper = new DESHelper(sKey);
				retStr = desHelper.encrypt(retData);
			}
			// 编码
			/*
			 * if("1".equals(ServiceInit.getConfigValue("IS_CODE"))){ retDataStr
			 * = DataProcessUtil.byteToBASE64(aByte); }else{ retDataStr =
			 * retData; }
			 */
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		retStr = StringUtils.isNotEmpty(retStr) ? retStr : retData;
		System.out.println("去掉换行空格前：" + retStr);
		retStr = retStr.replaceAll("\r\n", "");
		System.out.println("去掉换行空格后：" + retStr);
		return retStr;
	}

	/**
	 * 得到请求头部用户信息
	 * 
	 * @param jsonDataStr
	 * @return
	 */
	public Map getRequestHeadUserMapByJsonData(String jsonDataStr) {
		Map retMap = null;
		try {
			Map obj = (Map) JSONObject.fromObject(jsonDataStr);
			if (obj.get("Data") != null
					&& ((Map) obj.get("Data")).get("Head") != null) {
				Map headMap = (Map) ((Map) obj.get("Data")).get("Head");
				retMap = (Map) headMap.get("User");
			}
		} catch (Exception ex) {
			logger.error("解析son请求头中的用户信息出现异常！", ex);
		}
		return retMap;
	}

	/**
	 * 得到请求中的request中的参数
	 * 
	 * @param jsonDataStr
	 * @return
	 */
	public Map getRequestBodyMapByJsonData(String jsonDataStr) {
		Map retMap = null;
		try {
			Map obj = (Map) JSONObject.fromObject(jsonDataStr);
			if (obj.get("Data") != null
					&& ((Map) obj.get("Data")).get("Request") != null) {
				retMap = (Map) ((Map) obj.get("Data")).get("Request");
			}
		} catch (Exception ex) {
			logger.error("解析son请求头中的用户信息出现异常！", ex);
		}
		return retMap;
	}

	/**
	 * 生成返回的json
	 * 
	 * @param replyCode
	 * @param replyMsg
	 * @return
	 */
	public String getReTJsonData(String replyCode, String replyMsg) {
		Map jsonMap = new HashMap();
		jsonMap.put("replyCode", replyCode);
		jsonMap.put("replyMsg", replyMsg);
		return JSONObject.fromObject(jsonMap).toString();
	}

	/**
	 * 生成返回的json 带token
	 * 
	 * @param replyCode
	 * @param replyMsg
	 * @param token
	 * @return
	 */
	public String getReTJsonData(String replyCode, String replyMsg, String token) {
		Map jsonMap = new HashMap();
		jsonMap.put("replyCode", replyCode);
		jsonMap.put("replyMsg", replyMsg);
		Map itemMap = new HashMap();
		itemMap.put("token", token);
		jsonMap.put("ResultData", itemMap);
		return JSONObject.fromObject(jsonMap).toString();
	}

	/**
	 * 返回生成的json
	 * 
	 * @param replyCode
	 * @param replyMsg
	 * @param List
	 * @return
	 */
	public String getReTJsonData(String replyCode, String replyMsg, List List) {
		Map jsonMap = new HashMap();
		jsonMap.put("replyCode", replyCode);
		jsonMap.put("replyMsg", replyMsg);
		Map itemMap = new HashMap();
		itemMap.put("requireItem", List);
		jsonMap.put("ResultData", itemMap);
		return JSONObject.fromObject(jsonMap).toString();
	}

	/**
	 * 生成返回的json
	 * 
	 * @param replyCode
	 * @param replyMsg
	 * @param itemMap
	 * @return
	 */
	public String getReTJsonData(String replyCode, String replyMsg, Map itemMap) {
		Map jsonMap = new HashMap();
		jsonMap.put("replyCode", replyCode);
		jsonMap.put("replyMsg", replyMsg);
		if (itemMap != null && itemMap.size() > 0) {
			jsonMap.put("ResultData", itemMap);
		}
		return JSONObject.fromObject(jsonMap).toString();
	}

	/**
	 * 生成返回的json
	 * 
	 * @param replyCode
	 * @param replyMsg
	 * @param keyAndValue
	 *            keyAndValue以-隔开
	 * @return
	 */
	public String getRetJsonData(String replyCode, String replyMsg,
			String keyAndValue) {
		Map jsonMap = new HashMap();
		jsonMap.put("replyCode", replyCode);
		jsonMap.put("replyMsg", replyMsg);
		if (keyAndValue != null && keyAndValue.indexOf("-") > -1) {
			jsonMap.put(keyAndValue.split("-")[0], keyAndValue.split("-")[1]);
		}
		return JSONObject.fromObject(jsonMap).toString();
	}

	/**
	 * 把map对象转换成VO
	 * 
	 * @param serializable
	 * @param map
	 * @return
	 * @throws Exception
	 */
	protected Serializable copyDataMap2JavaBean(Serializable serializable,
			Map map) throws Exception {
		Serializable obj = serializable;
		if (map == null || map.size() == 0)
			return obj;
		try {
			Map jsonMap = map;
			if (jsonMap != null && jsonMap.size() > 0) {
				Method[] methods = obj.getClass().getDeclaredMethods();
				for (int ii = 0; ii < methods.length; ii++) { // 通过java反射对数据进行一一赋值
					if (methods[ii].getName().startsWith("set")
							&& methods[ii].getParameterTypes().length == 1) {
						String paramName = methods[ii].getName().replaceFirst(
								"set", "");
						paramName = paramName.substring(0, 1).toLowerCase()
								+ paramName.substring(1);
						if (jsonMap.get(paramName) != null
								&& !"".equals(jsonMap.get(paramName).toString())) {
							methods[ii].invoke(obj,
									new Object[] { jsonMap.get(paramName) });
						}
					}
				}
			}
		} catch (Exception ex) {
			logger.error("把MAP数据转化java对象成出现异常!", ex);
			throw new Exception("把MAP数据转化成java对象出现异常!");
		}
		return obj;
	}

	/**
	 * 工具方法，将集合格式的的json串转化成list集合
	 * 
	 * @param json
	 * @param clazz
	 * @return 转化好的对象集合
	 * @throws Exception
	 */
	protected List<?> translateJsonToList(Object json, Class<?> clazz)
			throws Exception {
		try {
			JSONArray jsonArray = JSONArray.fromObject(json);
			JSONUtils.getMorpherRegistry()
					.registerMorpher(
							new TimestampMorpher(
									new String[] { "yyyy-MM-dd HH:mm:ss" }));
			List<?> list = JSONArray.toList(jsonArray, clazz);
			return list;
		} catch (Exception e) {
			logger.error("把json数据转化成java对象集合出现异常!", e);
			throw new Exception("把json数据转化成java对象集合出现异常!");
		}
	}

	public String getRequestJsonStr() {
		return requestJsonStr;
	}

	public void setRequestJsonStr(String requestJsonStr) {
		this.requestJsonStr = requestJsonStr;
	}

	public Map getReqHeadUserMap() {
		return reqHeadUserMap;
	}

	public void setReqHeadUserMap(Map reqHeadUserMap) {
		this.reqHeadUserMap = reqHeadUserMap;
	}

	public Map getReqBodyMap() {
		return reqBodyMap;
	}

	public void setReqBodyMap(Map reqBodyMap) {
		this.reqBodyMap = reqBodyMap;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
}
