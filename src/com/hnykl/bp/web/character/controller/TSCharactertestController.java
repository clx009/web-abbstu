package com.hnykl.bp.web.character.controller;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hnykl.bp.android.common.AndroidResult;
import com.hnykl.bp.base.configuration.BaseConfigMgr;
import com.hnykl.bp.base.tool.character.StringUtils;
import com.hnykl.bp.web.AbstractAndroidController;
import com.hnykl.bp.web.character.entity.TSCharactertestEntity;
import com.hnykl.bp.web.character.service.TSCharactertestServiceI;
import com.hnykl.bp.web.system.service.SystemService;

/**   
 * @Title: Controller
 * @Description: t_s_charactertest
 * @author onlineGenerator
 * @date 2016-10-05 16:46:58
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/tSCharactertestController")
public class TSCharactertestController extends AbstractAndroidController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(TSCharactertestController.class);

	@Autowired
	private TSCharactertestServiceI tSCharactertestService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	@RequestMapping(params = "findCharacters")
	@ResponseBody
	public String findCharacters() {
		String userId = StringUtils.getMapDataStr(reqBodyMap, "userId");
		try {
			List<TSCharactertestEntity> characters = tSCharactertestService.findCharacters(userId);
			for(TSCharactertestEntity e:characters){
				e.setPath(BaseConfigMgr.getWebVisitUrl()+"/"+e.getPath());
			}
			Map<String, Object> resultMap = new HashMap<String, Object>();
			resultMap.put("characters", characters);
			return new AndroidResult(AndroidResult.SUCCESS_CODE,
					AndroidResult.SUCCESS_MASSEGE, resultMap)
					.toAndroidResultString();
		} catch (Exception e) {
			e.printStackTrace();
			return AndroidResult.defaultFailure(e.getMessage())
					.toAndroidResultString();
		}
	}
}
