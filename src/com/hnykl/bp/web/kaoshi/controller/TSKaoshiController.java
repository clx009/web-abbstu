package com.hnykl.bp.web.kaoshi.controller;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.hnykl.bp.android.common.AndroidResult;
import com.hnykl.bp.base.core.common.controller.BaseController;
import com.hnykl.bp.base.core.common.exception.BusinessException;
import com.hnykl.bp.base.core.common.hibernate.qbc.CriteriaQuery;
import com.hnykl.bp.base.core.common.model.json.AjaxJson;
import com.hnykl.bp.base.core.common.model.json.DataGrid;
import com.hnykl.bp.base.core.constant.Globals;
import com.hnykl.bp.base.core.util.MyBeanUtils;
import com.hnykl.bp.base.core.util.StringUtil;
import com.hnykl.bp.base.tool.character.StringUtils;
import com.hnykl.bp.tag.core.easyui.TagUtil;
import com.hnykl.bp.web.AbstractAndroidController;
import com.hnykl.bp.web.character.entity.TSCharactertestEntity;
import com.hnykl.bp.web.kaoshi.entity.TSKaoshiEntity;
import com.hnykl.bp.web.kaoshi.service.TSKaoshiServiceI;
import com.hnykl.bp.web.system.service.SystemService;

/**   
 * @Title: Controller
 * @Description: t_s_kaoshi
 * @author onlineGenerator
 * @date 2016-10-05 16:47:17
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/tSKaoshiController")
public class TSKaoshiController extends AbstractAndroidController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(TSKaoshiController.class);

	@Autowired
	private TSKaoshiServiceI tSKaoshiService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	

	@RequestMapping(params = "findTestResults")
	@ResponseBody
	public String findTestResults() {
		String userId = StringUtils.getMapDataStr(reqBodyMap, "userId");
		try {
			List<TSKaoshiEntity> characters = tSKaoshiService.findTestResults(userId);
			Map<String, Object> resultMap = new HashMap<String, Object>();
			resultMap.put("characters", characters);
			return new AndroidResult(AndroidResult.SUCCESS_CODE,
					AndroidResult.SUCCESS_MASSEGE, resultMap)
					.toAndroidResultString();
		} catch (Exception e) {
			e.printStackTrace();
			return AndroidResult.defaultFailure(e.getMessage())
					.toAndroidResultString();
		}
	}

}
