package com.hnykl.bp.interceptors;

import java.lang.reflect.Method;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.hnykl.bp.base.core.annotation.ReqAnalyze;
import com.hnykl.bp.web.AbstractAndroidController;

public class ReqAnalyzeApiInterceptor extends HandlerInterceptorAdapter {

    public boolean preHandle(HttpServletRequest request,
            HttpServletResponse response, Object handler) throws Exception {
    	AbstractAndroidController handlerMethod = (AbstractAndroidController)handler;
    	String requestJsonStr = request.getParameter("requestJsonStr");
    	handlerMethod.setRequestJsonStr(requestJsonStr);
    	handlerMethod.init();
       /* Method method = handlerMethod.getMethod();
        AbstractAndroidController abstractAndroidController = (AbstractAndroidController)handlerMethod.getBean();
        ReqAnalyze annotation = method.getAnnotation(ReqAnalyze.class);
        if (annotation != null) {
        	handlerMethod.init();
        }*/
        // 没有注解通过拦截
        return true;
    }
}